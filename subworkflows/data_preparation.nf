include { BUILD_DATABASE; REPEAT_MODELER; REPEAT_MASKER } from '../modules/data_preparation/repeat_masking.nf'
include { FORMAT_GENOME; PERCENT_MASKED; METRICS; GENOME_LOG } from '../modules/data_preparation/genome.nf'
include { CHECK_SRA; FETCH_RNA; FASTP; QC_REMOVE_SAMPLES; QC_LOG } from '../modules/data_preparation/rna_reads.nf'
include { MINIPROTHINT_ORTHODB_ALIGN; ORTHODB_ALIGN; HISAT2_INDEX; HISAT2_ALIGN; ALIGNMENT_REMOVE_SAMPLES; MAPPING_RATE; ALIGNMENT_LOG } from '../modules/data_preparation/alignments.nf'
include { REFERENCE; REFSEQ; ENTAP; ORTHODB; PFAM } from '../modules/data_preparation/databases.nf'


workflow DATA_INPUT {

    take:
    general_max_intron
    hisat2_max_intron

    main:
    merge = Channel.empty()

    if(params.entap_db == null && params.eggnog_dir == null && params.eggnog_db == null){
        ENTAP               ( [], [], [] )
        eggnog_db = ENTAP.out.eggnog_db
        entap_db = ENTAP.out.entap_db
        eggnog_dir = ENTAP.out.eggnog_dir

    }
    else if(params.entap_db != null && params.eggnog_dir == null && params.eggnog_db == null){
        ENTAP               ( params.entap_db, [], [] )
        eggnog_db = ENTAP.out.eggnog_db
        entap_db = Channel.fromPath(params.entap_db, checkIfExists: true)
        eggnog_dir = ENTAP.out.eggnog_dir
    }
    else if(params.entap_db == null && params.eggnog_dir != null && params.eggnog_db == null){
        ENTAP               ( [], params.eggnog_dir, [] )
        eggnog_db = ENTAP.out.eggnog_db
        entap_db = ENTAP.out.entap_db
        eggnog_dir = Channel.fromPath(params.eggnog_dir, checkIfExists: true)
    }
    else if(params.entap_db == null && params.eggnog_dir == null && params.eggnog_db != null){
        ENTAP               ( [], [], params.eggnog_db )
        eggnog_db = Channel.fromPath(params.eggnog_db, checkIfExists: true)
        entap_db = ENTAP.out.entap_db
        eggnog_dir = ENTAP.out.eggnog_dir
    }
    else if(params.entap_db != null && params.eggnog_dir != null && params.eggnog_db == null){
        ENTAP               ( params.entap_db, params.eggnog_dir, [])
        eggnog_db = ENTAP.out.eggnog_db
        entap_db = Channel.fromPath(params.entap_db, checkIfExists: true)
        eggnog_dir = Channel.fromPath(params.eggnog_dir, checkIfExists: true)
    }
    else if(params.entap_db != null && params.eggnog_dir == null && params.eggnog_db != null){
        ENTAP               ( params.entap_db, [], params.eggnog_db)
        eggnog_db = Channel.fromPath(params.eggnog_db, checkIfExists: true)
        entap_db = Channel.fromPath(params.entap_db, checkIfExists: true)
        eggnog_dir = ENTAP.out.eggnog_dir
    }
    else if(params.entap_db == null && params.eggnog_dir != null && params.eggnog_db != null){
        ENTAP               ( [], params.eggnog_dir, params.eggnog_db)
        eggnog_db = Channel.fromPath(params.eggnog_db, checkIfExists: true)
        entap_db = ENTAP.out.entap_db
        eggnog_dir = Channel.fromPath(params.eggnog_dir, checkIfExists: true)
    }
    else if(params.entap_db != null && params.eggnog_dir != null && params.eggnog_db != null){
        eggnog_db = Channel.fromPath(params.eggnog_db, checkIfExists: true)
        entap_db = Channel.fromPath(params.entap_db, checkIfExists: true)
        eggnog_dir = Channel.fromPath(params.eggnog_dir, checkIfExists: true)
    }

    if(params.reference_db == null ){
        REFSEQ          ()
        reference_db = REFSEQ.out.entap_reference
    }
    else if(params.reference_db != null ){
        reference_db = Channel.fromPath(params.reference_db, checkIfExists: true)
    }
    
    //fasta = Channel.fromPath(params.genome).flatten().map { file -> tuple(file.baseName, file) }
    FORMAT_GENOME    ( params.genome )
    unzip_fasta = FORMAT_GENOME.out.unzipped

    if(params.repeat_masking == true){
        BUILD_DATABASE  ( unzip_fasta )
        REPEAT_MODELER  ( BUILD_DATABASE.out.db, params.repeat_modeler_args )
        REPEAT_MASKER   ( REPEAT_MODELER.out.rm, unzip_fasta, params.repeat_masker_args )
        genome = REPEAT_MASKER.out.masked
    }
    else if(params.repeat_masking == false){
            genome = unzip_fasta
    }

    if(params.reference != null ){
        ch_reference = Channel.fromPath( params.reference, checkIfExists: true)
        REFERENCE     ( ch_reference, params.prefix )
        reference = REFERENCE.out.gtf
    }
    else if(params.reference == null ){
        reference = Channel.empty()
    }

    if(params.user_protein != null){
        ORTHODB             ( params.order ?:'', params.taxid ?:'', params.user_protein )
    }
    else if(params.user_protein == null){
        ORTHODB             ( params.order, params.taxid ?:'', [] )
    }

    if (params.pfam == true && params.pfam_db == null){
    PFAM ()
    pfam = PFAM.out.db    
    }

    else if (params.pfam == true && params.pfam_db != null){
    pfam = Channel.fromPath(params.pfam_db, checkIfExists: true)   
    }

    else if (params.pfam == false ){
    pfam = Channel.empty()   
    }

    if (params.miniprothint == true){
    MINIPROTHINT_ORTHODB_ALIGN  ( genome, ORTHODB.out.protein, params.miniprot_args, general_max_intron )
    miniprot_alignment = MINIPROTHINT_ORTHODB_ALIGN.out.orthodb_alignment
    }
    else if (params.miniprothint == false){
    ORTHODB_ALIGN       ( genome, ORTHODB.out.protein, params.miniprot_args, general_max_intron )
    miniprot_alignment = ORTHODB_ALIGN.out.orthodb_alignment
    }
    PERCENT_MASKED      ( genome, params.bins )

    if ( params.busco_genome == true ){
    METRICS             ( genome, params.busco_lineage, PERCENT_MASKED.out.log_genome ) 
    genome_log = METRICS.out.log
    }
    else if ( params.busco_genome == false ){
    GENOME_LOG          ( PERCENT_MASKED.out.log_genome )
    genome_log = GENOME_LOG.out.log
    }


    if ( params.long_intron == true ){
        max_intron = general_max_intron
    }
    else if (params.long_intron == false ){
        max_intron = hisat2_max_intron
    }

    if(params.resume_filtering == false){     
        if(params.bam == null && params.bam_long == null ){
            if(params.sra != null && params.user_reads != null){
                sra_ch = Channel.fromPath(params.sra, checkIfExists: true).splitCsv().flatten()
                CHECK_SRA	    ( sra_ch )
		        FETCH_RNA           ( CHECK_SRA.out.sra )
                fetch_rna = FETCH_RNA.out.sra
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                fetch_rna
                    .concat( read_pairs_ch )
                    .view()
                    .set { rna }
            }
            else if(params.sra != null && params.user_reads == null){
                sra_ch = Channel.fromPath(params.sra, checkIfExists: true).splitCsv().flatten()
                CHECK_SRA           ( sra_ch )
		        FETCH_RNA           ( CHECK_SRA.out.sra )
                fetch_rna = FETCH_RNA.out.sra
                fetch_rna
                    .set { rna }
            }
            else if(params.sra == null && params.user_reads != null){
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                read_pairs_ch
                    .set { rna }
            }
            FASTP               ( rna, params.fastp_args )
            QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
            QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
            qc_log = QC_LOG.out.log_qc
            HISAT2_INDEX        ( genome )
            HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, max_intron, params.hisat2_args )
            hisat2 = HISAT2_ALIGN.out.sam_tuple
            ALIGNMENT_REMOVE_SAMPLES    ( hisat2, params.rate, params.total_reads )
            pass_bam_tuple = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
            pass_bam = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
        }

        if(params.bam != null || params.bam_long !=null){
            if(params.bam != null && params.bam_long == null){
            ch_bam = Channel.fromPath(params.bam, checkIfExists: true).flatten().map { file -> tuple(file.baseName, file) }
            }
            else if(params.bam == null && params.bam_long != null){
            ch_bam = Channel.fromPath(params.bam_long, checkIfExists: true).flatten().map { file -> tuple(file.baseName, file) }
            }
            else if(params.bam !=null && params.bam_long != null){
            ch_short = Channel.fromPath(params.bam, checkIfExists: true)
            ch_long = Channel.fromPath(params.bam_long, checkIfExists: true)
            ch_bam = ch_short.concat(ch_long).flatten().map { file -> tuple(file.baseName, file) }
            }
            MAPPING_RATE                ( ch_bam )
            bam_map = MAPPING_RATE.out.bam_user
        
            if(params.sra == null && params.user_reads == null){
                ALIGNMENT_REMOVE_SAMPLES    ( bam_map, params.rate, params.total_reads )
                pass_bam_tuple = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
                pass_bam = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
                QC_LOG                      ( [], params.total_reads, params.mean_length )
                qc_log = QC_LOG.out.log_qc
            }
            else if(params.sra != null && params.user_reads == null){
                sra_ch = Channel.fromPath(params.sra, checkIfExists: true).splitCsv().flatten()
                CHECK_SRA           ( sra_ch )
                FETCH_RNA           ( CHECK_SRA.out.sra )
                fetch_rna = FETCH_RNA.out.sra
                fetch_rna
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                qc_log = QC_LOG.out.log_qc
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, max_intron, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate, params.total_reads )
                pass_bam_tuple = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
                pass_bam = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
            }
            else if(params.sra == null && params.user_reads != null){
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                read_pairs_ch
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                qc_log = QC_LOG.out.log_qc
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, max_intron, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate, params.total_reads )
                pass_bam_tuple = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
                pass_bam = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
            }
            else if(params.sra != null && params.user_reads != null){
                sra_ch = Channel.fromPath(params.sra, checkIfExists: true).splitCsv().flatten()
                CHECK_SRA           ( sra_ch )
                FETCH_RNA           ( CHECK_SRA.out.sra )
		fetch_rna = FETCH_RNA.out.sra
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                fetch_rna
                    .concat( read_pairs_ch )
                    .view()
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                qc_log = QC_LOG.out.log_qc
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, max_intron, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .transpose()
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate, params.total_reads )
                pass_bam_tuple = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
                pass_bam = ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
            }
        }
    ALIGNMENT_LOG              ( ALIGNMENT_REMOVE_SAMPLES.out.mr.collect(), params.rate )
    alignment_log = ALIGNMENT_LOG.out.log_align
    }
    if(params.resume_filtering == true){
        pass_bam_tuple = Channel.fromPath("${launchDir}/${params.outdir}/03_alignments/bam/*.bam", checkIfExists: true).flatten().map { file -> tuple(file.baseName, file) }
        pass_bam = Channel.fromPath("${launchDir}/${params.outdir}/03_alignments/bam/*.bam", checkIfExists: true)
       // MAPPING_RATE                ( ch_bam )
       // bam_map = MAPPING_RATE.out.bam_user
       // ALIGNMENT_REMOVE_SAMPLES    ( bam_map, params.rate, params.total_reads )
       // QC_LOG                      ( [], params.total_reads, params.mean_length )
        qc_log = Channel.fromPath("${launchDir}/${params.outdir}/log/log_qc.txt", checkIfExists: true)
        alignment_log = Channel.fromPath("${launchDir}/${params.outdir}/log/log_align.txt", checkIfExists: true)
    }

    

    emit:
    pass_bam_tuple
    pass_bam
    PERCENT_MASKED.out.chromosomes
    genome_log
    qc_log
    alignment_log
    eggnog_db.collect()
    entap_db.collect()
    eggnog_dir.collect()
    miniprot_alignment
    genome
    reference
    reference_db
    pfam
    ORTHODB.out.protein
}
