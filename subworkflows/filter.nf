include { UNFILTERED_GTF } from '../modules/feature_filtering/gff_to_gtf.nf'
include { FEATURE_COUNTS; GENE_EXPRESSION } from '../modules/feature_filtering/gene_expression.nf'
include { ORTHODB_BLAST; ORTHODB_FEATURE } from '../modules/feature_filtering/orthodb.nf'
include { SUPPORT } from '../modules/feature_filtering/support.nf'
include { MONO_MULTI } from '../modules/feature_filtering/multi_mono.nf' 
include { PROTEIN; NUCLEOTIDE; EGGNOG; EGGNOG_FEATURE } from '../modules/feature_filtering/gene_family.nf'  
include { HMMSCAN } from '../modules/feature_filtering/pfam.nf'
include { COUNT_ATG } from '../modules/feature_filtering/count_atg.nf'
include { CDS_EXTRACT; CDS_REPEAT_CONTENT } from '../modules/feature_filtering/repeat_cds.nf' 
include { KOZAK_SEQUENCE } from '../modules/feature_filtering/kozak.nf'
include { MAP } from '../modules/feature_filtering/map.nf' 
include { TRANSCRIPT_EXTRACT; MOLECULAR_WEIGHT; MERGE_TRANSCRIPT; GC; GC_PYTHON } from '../modules/feature_filtering/transcript.nf'
include { TRUE_START_SITE; PAD; SPLIT_FASTA; RNA_FOLD; MERGE; MATRIX } from '../modules/feature_filtering/folding.nf' 
include { FILTER_TRAINING_SET; REGRESSOR; CLASSIFIER; FIND_DUPS; RF_FILTER; CLEAN_GTF } from '../modules/feature_filtering/random_forest.nf'

workflow FILTERING {

    take:
    genome
    eggnog_db
    bam
    gff
    list
    pfam
    othodb_protein

    main:
    UNFILTERED_GTF          ( gff, params.prefix )
    PROTEIN                 ( genome, UNFILTERED_GTF.out.unfiltered_gtf, params.prefix )
    protein = PROTEIN.out.protein
    NUCLEOTIDE              ( genome, UNFILTERED_GTF.out.unfiltered_gtf, params.prefix )
    cds = NUCLEOTIDE.out.cds
    FEATURE_COUNTS          ( bam, UNFILTERED_GTF.out.unfiltered_gtf )
    SUPPORT                 ( gff, list )
    ORTHODB_BLAST           ( othodb_protein, PROTEIN.out.protein, params.orthodb_qcoverage, params.orthodb_tcoverage )
    ORTHODB_FEATURE         ( SUPPORT.out.support, ORTHODB_BLAST.out.hits )
    GENE_EXPRESSION         ( FEATURE_COUNTS.out.counts.collect(), SUPPORT.out.support)
    MONO_MULTI              ( gff, SUPPORT.out.support )
    COUNT_ATG               ( cds, SUPPORT.out.support )

    if (params.pfam == true ){
    HMMSCAN                    ( protein, pfam )
    hmmscan = HMMSCAN.out.transcripts
    }

    EGGNOG                  ( PROTEIN.out.protein, eggnog_db )
    EGGNOG_FEATURE          ( SUPPORT.out.support, EGGNOG.out.hits )
    CDS_EXTRACT             ( genome, UNFILTERED_GTF.out.unfiltered_gtf, SUPPORT.out.support )
    CDS_REPEAT_CONTENT      ( CDS_EXTRACT.out.cds, SUPPORT.out.support )
    KOZAK_SEQUENCE          ( genome, gff, SUPPORT.out.support )
    TRANSCRIPT_EXTRACT      ( genome, UNFILTERED_GTF.out.unfiltered_gtf, SUPPORT.out.support, params.parts )
    transcript_ch = TRANSCRIPT_EXTRACT.out.transcript_part.flatten().map { file -> tuple(file.baseName, file) }
    //LENGTH                ( transcript_ch ) removed because of high correlation with molecular weight
    MOLECULAR_WEIGHT        ( transcript_ch )
    MERGE_TRANSCRIPT        ( MOLECULAR_WEIGHT.out.weight_txt.collect(), SUPPORT.out.support)
    GC                      ( TRANSCRIPT_EXTRACT.out.transcript )
    GC_PYTHON               ( SUPPORT.out.support, GC.out.gc )
    TRUE_START_SITE         ( gff )
    PAD                     ( params.window, TRUE_START_SITE.out.text, genome )
    SPLIT_FASTA             ( PAD.out.fasta, params.parts )
    fasta_ch = SPLIT_FASTA.out.split.flatten().map { file -> tuple(file.baseName, file) }
    RNA_FOLD                ( fasta_ch )
    MERGE                   ( RNA_FOLD.out.rna_fold.collect() )
    MATRIX                  ( params.window, SUPPORT.out.support, MERGE.out.merge_rna )
    MAP                     ( MATRIX.out.gc_ratio.collect(), MATRIX.out.free_energy.collect(), GC_PYTHON.out.gc_track.collect(), MERGE_TRANSCRIPT.out.weight.collect(), SUPPORT.out.support.collect(), SUPPORT.out.binary.collect(),MONO_MULTI.out.exonic.collect(), EGGNOG_FEATURE.out.bitscore.collect(), EGGNOG_FEATURE.out.similarity.collect(), EGGNOG_FEATURE.out.evalue.collect(), EGGNOG_FEATURE.out.qstart.collect(), EGGNOG_FEATURE.out.qend.collect(), EGGNOG_FEATURE.out.sstart.collect(), EGGNOG_FEATURE.out.send.collect(), EGGNOG_FEATURE.out.qcoverage.collect(), EGGNOG_FEATURE.out.scoverage.collect(), EGGNOG_FEATURE.out.support.collect(), CDS_REPEAT_CONTENT.out.cds_repeat.collect(), CDS_REPEAT_CONTENT.out.cds_length.collect(), ORTHODB_FEATURE.out.bitscore.collect(), ORTHODB_FEATURE.out.similarity.collect(), ORTHODB_FEATURE.out.evalue.collect(), ORTHODB_FEATURE.out.length.collect(), ORTHODB_FEATURE.out.mismatch.collect(), ORTHODB_FEATURE.out.querystart.collect(), ORTHODB_FEATURE.out.queryend.collect(), ORTHODB_FEATURE.out.sstart.collect(), ORTHODB_FEATURE.out.send.collect(), ORTHODB_FEATURE.out.support.collect(), GENE_EXPRESSION.out.expression.collect(), KOZAK_SEQUENCE.out.neg3.collect(), KOZAK_SEQUENCE.out.pos4.collect(), COUNT_ATG.out.counts.collect())
    
    if(params.build_training_set == false){

        if(params.remove_dups == true){
        FIND_DUPS               ( MAP.out.features, gff )
        }
        if(params.training_set == "plant"){
            if (params.keep_taxa == null){
                REGRESSOR               ( MAP.out.features, params.plant, params.stringency )
                CLASSIFIER              ( MAP.out.features, params.plant, params.stringency )
            }
            else if (params.keep_taxa != null){
                FILTER_TRAINING_SET     ( params.plant, params.keep_taxa )
                REGRESSOR               ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )
                CLASSIFIER              ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )                
            }
        }
        else if(params.training_set == "vertebrate"){
            if (params.keep_taxa == null){
                REGRESSOR               ( MAP.out.features, params.vertebrate, params.stringency )
                CLASSIFIER              ( MAP.out.features, params.vertebrate, params.stringency )
            }
            else if (params.keep_taxa != null){
                FILTER_TRAINING_SET     ( params.vertebrate, params.keep_taxa )
                REGRESSOR               ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )
                CLASSIFIER              ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )                
            }
        }
        else if(params.training_set == "invertebrate"){
            if (params.keep_taxa == null){
                REGRESSOR               ( MAP.out.features, params.invertebrate, params.stringency )
                CLASSIFIER              ( MAP.out.features, params.invertebrate, params.stringency )
            }
            else if (params.keep_taxa != null){
                FILTER_TRAINING_SET     ( params.invertebrate, params.keep_taxa )
                REGRESSOR               ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )
                CLASSIFIER              ( MAP.out.features, FILTER_TRAINING_SET.out.track, params.stringency )                
            }
        }
        else if(params.training_set == "user"){
            training_set_ch = Channel.fromPath(params.training_set_path, checkIfExists: true)
            REGRESSOR               ( MAP.out.features, training_set_ch, params.stringency )
            CLASSIFIER              ( MAP.out.features, training_set_ch, params.stringency )
        }
        if (params.pfam == false && params.remove_dups == true){
            RF_FILTER               ( CLASSIFIER.out.c_rf, REGRESSOR.out.r_rf, FIND_DUPS.out.drop, gff, params.regressor, [] )
        }
        if (params.pfam == false && params.remove_dups == false){
            RF_FILTER               ( CLASSIFIER.out.c_rf, REGRESSOR.out.r_rf, [], gff, params.regressor, [] )
        }
        if (params.pfam == true && params.remove_dups == false){
            RF_FILTER               ( CLASSIFIER.out.c_rf, REGRESSOR.out.r_rf, [], gff, params.regressor, hmmscan )
        }
        else if (params.pfam == true && params.remove_dups == true ){
            RF_FILTER               ( CLASSIFIER.out.c_rf, REGRESSOR.out.r_rf, FIND_DUPS.out.drop, gff, params.regressor, hmmscan )
        }
        CLEAN_GTF               ( RF_FILTER.out.filtered_prediction, params.prefix )
        gtf_ch = CLEAN_GTF.out
        regressor_ch = REGRESSOR.out
    }
    if (params.build_training_set == true){
        gtf_ch = Channel.empty()
        regressor_ch = Channel.empty()
    }

    emit:
    gtf_ch
    regressor_ch
    protein
    MAP.out.features
}

