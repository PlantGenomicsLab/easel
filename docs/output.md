# EASEL Output

### 01_reads

**Required param(s):** `sra`

<details markdown="1">
<summary>raw_reads</summary>

- `*{1,2}.fastq.gz`: SRA paired-end, short RNA FASTQ files 

</details>

**Required param(s):** `sra` and `user_reads`

<details markdown="1">
<summary>quality_control</summary>

- `*.html`: fastp HTML report
- `*.json`: fastp JSON report
</details>
<details markdown="1">
<summary>trimmed_reads</summary>

- `*trimmed_{1,2}.fastq.gz`: fastp trimmed paired-end, short RNA FASTQ files
</details>

* Tool(s):
  * [fastp](#fastp)  

### 02_index

**Required param(s):** `sra` and `user_reads`

<details markdown="1">
<summary>hisat2</summary>

- `hisat2.index/*.ht2`: HISAT2 genome index 
</details>
<details markdown="1">
<summary>gmap</summary>

- `gmap/gmap*`: GMAP genome index  
</details>

* Tool(s):
  * [HISAT2](#hisat2)
  * [GMAP](#gmap)  

### 03_alignments

<details markdown="1">
<summary>bam</summary>

- `*.bam`: HISAT2 RNA alignments 
</details>

<details markdown="1">
<summary>mapping_rates</summary>

- `*_mapping_rate.txt`: HISAT2 mapping rates [`sra`, `user_reads`]
- `*.txt`: Samtools mapping rates [`bam`] 
</details>

<details markdown="1">
<summary>est</summary>

- `psiclass.psl`: PsiCLASS TransDecoder CDS aligned to genome with GMAP 
- `stringtie2`: StringTie2 TransDecoder CDS aligned to genome with GMAP 
</details>

<details markdown="1">
<summary>protein</summary>

- `orthodb.gtf`: OrthoDB [`order`] proteins aligned to genome with miniprot
- `psiclass.gtf`: PsiCLASS TransDecoder PEP aligned to genome with miniprot 
- `stringtie2.gtf`: StringTie2 TransDecoder PEP aligned to genome with miniprot
</details>

* Tool(s):
  * [HISAT2](#hisat2)
  * [GMAP](#gmap)
  * [miniprot](#miniprot)
  * [samtools](#samtools)  

### 04_assembly

<details markdown="1">
<summary>stringtie2</summary>

- `clustering/centroids.cds`:
- `clustering/centroids.uc`:
- `clustering/id.txt`:
- `clustering/stringtie2.completeORF.cds`:
- `stringtie2_transcripts.fa.transdecoder_dir/*`:
- `frameselection/stringtie2_eggnog.blastp.emapper.hits`:
- `frameselection/stringtie2_transcripts.fa.transdecoder.bed`:
- `frameselection/stringtie2_transcripts.fa.transdecoder.cds`:
- `frameselection/stringtie2_transcripts.fa.transdecoder.gff3`:
- `frameselection/stringtie2_transcripts.fa.transdecoder.pep`:
- `gtf/*.gtf`:
- `model.stringtie2.gff3`:
- `stringtie2.fasta.transdecoder.genome.gff3`:
- `stringtie2_transcripts.fa`: 
</details>

<details markdown="1">
<summary>psiclass</summary>

- `clustering/centroids.cds`:
- `clustering/centroids.uc`:
- `clustering/id.txt`:
- `clustering/psiclass.completeORF.cds`:
- `psiclass_transcripts.fa.transdecoder_dir/*`:
- `frameselection/psiclass_eggnog.blastp.emapper.hits`:
- `frameselection/psiclass_transcripts.fa.transdecoder.bed`:
- `frameselection/psiclass_transcripts.fa.transdecoder.cds`:
- `frameselection/psiclass_transcripts.fa.transdecoder.gff3`:
- `frameselection/psiclass_transcripts.fa.transdecoder.pep`:
- `gtf/*.gtf`:
- `model.psiclass.gff3`:
- `psiclass.fasta.transdecoder.genome.gff3`:
- `psiclass_transcripts.fa`: 
</details>

* Tool(s):
  * [PsiCLASS](#psiclass)
  * [StringTie2](#stringtie2)
  * [VSEARCH](#vsearch)
  * [TransDecoder](#transdecoder) 

### 05_hints

<details markdown="1">
<summary>orthodb</summary>

- `orthodb.protHints.gff`:
</details>

<details markdown="1">
<summary>psiclass</summary>

- `psiclass.protHints.gff`:
- `psiclass.estHints.gff`:
</details>

<details markdown="1">
<summary>stringtie2</summary>

- `stringtie2.protHints.gff`:
- `stringtie2.estHints.gff`:
</details>

### 06_predictions

<details markdown="1">
<summary>psiclass</summary>

- `est_psiclass.gff`:
- `protein_psiclass.gff`:
- `orthodb_psiclass.gff`:
</details>

<details markdown="1">
<summary>stringtie2</summary>

- `est_stringtie2.gff`:
- `protein_stringtie2.gff`:
- `orthodb_stringtie2.gff`:
</details>

### 07_filtering

<details markdown="1">
<summary>psiclass</summary>

- `est_psiclass.gff`:
- `protein_psiclass.gff`:
- `orthodb_psiclass.gff`:
</details>


[Nextflow](https://www.nextflow.io/docs/latest/tracing.html) provides excellent functionality for generating various reports relevant to the running and execution of the pipeline. This will allow you to troubleshoot errors with the running of the pipeline, and also provide you with other information such as launch commands, run times and resource usage.