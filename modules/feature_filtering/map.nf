process MAP {
    publishDir "$params.outdir/07_filtering",  mode: 'copy'
    label 'process_single'
     
    input:
    path(gc_ratio)
    path(rna_fold)
    path(gc_content)
    path(weight)
    path(support)
    path(support_binary)
    path(multi_mono)
    path(gene_family_bitscore)
    path(gene_family_similarity)
    path(gene_family_evalue)
    path(gene_family_qstart)
    path(gene_family_qend)
    path(gene_family_sstart)
    path(gene_family_send)
    path(gene_family_qcoverage)
    path(gene_family_scoverage)
    path(gene_family_support)
    path(cds_repeat)
    path(cds_length)
    path(orthodb_bitscore)
    path(orthodb_similarity)
    path(orthodb_evalue)
    path(orthodb_length)
    path(orthodb_mismatch)
    path(orthodb_querystart)
    path(orthodb_queryend)
    path(orthodb_sstart)
    path(orthodb_send)
    path(orthodb_support)
    path(expression)
    path(koz3)
    path(koz4)
    path(atg_count)
     
    output: 
    path("features.tracking"), emit: features
      
    """
    awk '{print \$8}' ${orthodb_bitscore} > orthodb_bitscore.txt
    awk '{print \$8}' ${orthodb_similarity} > orthodb_similarity.txt
    awk '{print \$8}' ${orthodb_evalue} > orthodb_evalue.txt
    awk '{print \$8}' ${orthodb_length} > orthodb_length.txt
    awk '{print \$8}' ${orthodb_mismatch} > orthodb_mismatch.txt
    awk '{print \$8}' ${orthodb_querystart} > orthodb_querystart.txt
    awk '{print \$8}' ${orthodb_queryend} > orthodb_queryend.txt
    awk '{print \$8}' ${orthodb_sstart} > orthodb_sstart.txt
    awk '{print \$8}' ${orthodb_send} > orthodb_send.txt
    awk '{print \$8}' ${orthodb_support} > orthodb_support.txt
    awk '{print \$8}' ${expression} > expression.txt
    awk '{print \$8}' ${gc_ratio} > gc_ratio.txt
    awk '{print \$8}' ${gc_content} > gc_content.txt
    awk '{print \$8}' ${rna_fold} > rna_fold.txt
    awk '{print \$8}' ${weight} > weight.txt
    awk '{print \$8}' ${multi_mono} > multi_mono.txt
    awk '{print \$8}' ${gene_family_bitscore} > gene_family_bitscore.txt
    awk '{print \$8}' ${gene_family_similarity} > gene_family_similarity.txt
    awk '{print \$8}' ${gene_family_evalue} > gene_family_evalue.txt
    awk '{print \$8}' ${gene_family_qstart} > gene_family_qstart.txt
    awk '{print \$8}' ${gene_family_qend} > gene_family_qend.txt
    awk '{print \$8}' ${gene_family_sstart} > gene_family_sstart.txt
    awk '{print \$8}' ${gene_family_send} > gene_family_send.txt
    awk '{print \$8}' ${gene_family_qcoverage} > gene_family_qcoverage.txt
    awk '{print \$8}' ${gene_family_scoverage} > gene_family_scoverage.txt
    awk '{print \$8}' ${gene_family_support} > gene_family_support.txt
    awk '{print \$8}' ${cds_repeat} > cds_repeat.txt
    awk '{print \$8}' ${cds_length} > cds_length.txt
    awk '{print \$8}' ${koz3} > koz3.txt
    awk '{print \$8}' ${koz4} > koz4.txt
    awk '{print \$8}' ${atg_count} > atg_count.txt
    awk '{print \$3}' ${support_binary} > support_binary.txt

    paste ${support} support_binary.txt multi_mono.txt gene_family_support.txt gene_family_bitscore.txt gene_family_similarity.txt gene_family_evalue.txt gene_family_qstart.txt gene_family_qend.txt gene_family_sstart.txt gene_family_send.txt gene_family_qcoverage.txt gene_family_scoverage.txt orthodb_support.txt orthodb_bitscore.txt orthodb_similarity.txt orthodb_evalue.txt orthodb_length.txt orthodb_mismatch.txt orthodb_querystart.txt orthodb_queryend.txt orthodb_sstart.txt orthodb_send.txt expression.txt weight.txt cds_repeat.txt cds_length.txt gc_content.txt rna_fold.txt gc_ratio.txt koz3.txt koz4.txt atg_count.txt | column -s \$'\t' -t > features.tracking

    """
}

