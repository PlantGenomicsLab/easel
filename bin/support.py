#!/usr/bin/env python

########################################################
#                                                      #
# support.py - create matrix of support for unfiltered #        
# transcripts from AUGUSTUS predictions and PsiCLASS/  #
# StringTie2 gene models                               #
#                                                      #
# Developed by Cynthia Webster (2024)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import sys
import gffutils
from collections import defaultdict

def parse_gff(file_path):
    """
    Parses a GFF file using gffutils and returns a dictionary 
    where each key is a transcript ID, and the value is a tuple 
    containing a set of tuples representing the features 
    (chromosome, start, end, strand, frame) and the parent gene ID.
    """
    features = {}

    # Create a database from the GFF file
    db = gffutils.create_db(file_path, ':memory:', merge_strategy='merge', 
                             disable_infer_genes=True)

    # Retrieve all genes and transcripts
    for gene in db.features_of_type('gene'):
        gene_id = gene.id
        for transcript in db.children(gene, featuretype='transcript', order_by='start'):
            transcript_id = transcript.id
            # Fetch all child features (exons and CDS) for the transcript
            child_features = list(db.children(transcript, featuretype=['exon', 'CDS'], 
                                               order_by='start'))

            # Store features along with the parent gene ID
            features[transcript_id] = ({
                (feature.chrom, feature.start, feature.end, feature.strand, feature.frame) 
                for feature in child_features
            }, gene_id)
    
    return features

def compare_transcripts(reference_features, query_features):
    """
    Compares reference transcript features with query features 
    to find exact matches.
    """
    results = {}
    query_sets = set(frozenset(q_set[0]) for q_set in query_features.values())  # Create a set of frozensets for query features

    for ref_transcript, (ref_set, parent_gene_id) in reference_features.items():
        # Check for an identical match in the query features using set membership
        results[ref_transcript] = (1 if frozenset(ref_set) in query_sets else 0, parent_gene_id)

    return results

def main(reference_file, query_file, output_file):
    # Parse GFF files
    reference_features = parse_gff(reference_file)
    query_features = parse_gff(query_file)

    # Compare transcript features and get results
    overlap_results = compare_transcripts(reference_features, query_features)

    # Write results to output file
    with open(output_file, 'w') as out_f:
        for transcript_id, (overlap, parent_gene_id) in overlap_results.items():
            out_f.write(f"{parent_gene_id}\t{transcript_id}\t{overlap}\n")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python support.py <reference_gff> <query_gff> <output_file>")
        sys.exit(1)

    reference_gff = sys.argv[1]
    query_gff = sys.argv[2]
    output_file = sys.argv[3]

    main(reference_gff, query_gff, output_file)

