#!/usr/bin/env python

########################################################
#                                                      #
# check_sra.py - uses ENA API to check if SRA          #
# accession is paired or single-end.                   #
#                                                      #
# Developed by Cynthia Webster (2024)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import requests
import sys

# Function to check if a read is paired
def check_paired_read(run_accession):
    # ENA API URL
    url = "https://www.ebi.ac.uk/ena/portal/api/search"
    
    # Parameters for the API request
    params = {
        "result": "read_run",
        "query": f"run_accession={run_accession}",
        "fields": "fastq_ftp"
    }
    
    # Send the request to the ENA API
    response = requests.get(url, params=params)
    
    if response.status_code == 200:
        # Parse the response text
        data = response.text.strip().split('\n')
        # Skip the header line and get the fastq_ftp field
        if len(data) > 1:
            fastq_ftp = data[1]  # Second line contains the data
            files = fastq_ftp.split(';')  # Split the FTP links
            
            # Determine if the read is paired based on the number of files
            if len(files) == 2:
                return f"paired"
            elif len(files) == 1:
                return f"single-end"
            else:
                return f"The read '{run_accession}' has an unexpected number of FASTQ files: {len(files)}"
        else:
            return f"No data found for run accession '{run_accession}'."
    else:
        return f"Error: Unable to fetch data from ENA API. Status code: {response.status_code}"

run_accession = sys.argv[1]
result = check_paired_read(run_accession)
print(result)
