#!/usr/bin/env python

########################################################
#                                                      #
# map_transcript.py - map features to matrix by 	   #
# transcript ID       								   #
#                                                      #
# Developed by Cynthia Webster (2022)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import pandas as pd         # For working with data frames
import numpy as np         # For numerical operations
import sys         # For command line arguments
import os

# Read command line arguments
matrix = sys.argv[1]
feature = sys.argv[2]
header = sys.argv[3]
output = sys.argv[4]

if not os.path.exists(feature) or os.path.getsize(feature) == 0:
    print(f"Warning: The file '{feature}' is empty or does not exist. Adding column '{header}' with NaN values.")
    df1 = pd.read_csv(matrix, delimiter='\t')
    df1[header] = np.nan
    df1.to_csv(output, sep='\t', encoding='utf-8', index=False)

else:
    # Read the input matrix and feature files into data frames
    df1 = pd.read_csv(matrix, delimiter='\t')
    df2 = pd.read_csv(feature, delimiter='\t')
    # Create a mapping dictionary from the 'Transcript' column to the 'Hit' column in the feature file
    mapping = dict(df2[['Transcript', 'Hit']].values)

    # Map the values from the 'Transcript' column in the matrix file using the mapping dictionary
    df1[header] = df1.Transcript.map(mapping)

# Write the updated matrix data frame to the output file
df1.to_csv(output, sep='\t', encoding='utf-8', index=False)
