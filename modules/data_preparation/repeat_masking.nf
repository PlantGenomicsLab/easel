process BUILD_DATABASE {
    label 'process_high'

    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/repeatmodeler:2.0.5--pl5321hdfd78af_0':
        'biocontainers/repeatmodeler:2.0.5--pl5321hdfd78af_0' }"

    input:
    path(genome)
     
    output:
    path("rm_database*"), emit: db
    
    """
    BuildDatabase -name "rm_database" ${genome}

    """
}

process REPEAT_MODELER {
    label 'process_high'

    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/repeatmodeler:2.0.5--pl5321hdfd78af_0':
        'biocontainers/repeatmodeler:2.0.5--pl5321hdfd78af_0' }"

    input:
    path(db)
    val(args)
     
    output:
    path("RM_*"), emit: rm
    
    """
    RepeatModeler -threads ${task.cpus} -database ${db} ${args}

    """
}

process REPEAT_MASKER {
    publishDir "$params.outdir",  mode: 'copy'
    label 'process_high'

    conda "bioconda::repeatmasker=4.1.2.p1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/repeatmasker:4.1.7p1--pl5321hdfd78af_1':
        'quay.io/biocontainers/repeatmasker:4.1.7p1--pl5321hdfd78af_1' }"

    input:
    path(rm)
    path(genome)
    val(args)
     
    output:
    path("genome/*"), emit: genome
    path("genome/*.masked"), emit: masked
    
    """
    RepeatMasker -dir genome -pa ${task.cpus} -lib ${rm}/consensi.fa.classified -gff -a -noisy -xsmall ${args} ${genome}

    """
}
