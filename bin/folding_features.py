#!/usr/bin/env python

########################################################
#                                                      #
# folding_features.py - calculates the gc ratio from   #
# defined window                                       #
#                                                      #
# Developed by Sumaira Zaman (2022)                    #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import re         # For regular expressions
import numpy as np         # For numerical operations
import linecache         # For reading specific lines from a file
import argparse         # For parsing command line arguments
import os         # For file operations
import glob         # For finding files using pattern matching
import csv         # For working with CSV files
import sys         # For command line arguments

# Create an empty dictionary
dicts = {}

# Get the current working directory and upstream/downstream values from command line arguments
cwd = sys.argv[1]
upstream = int(sys.argv[2])
downstream = int(sys.argv[3])

total_len = upstream + downstream
print(total_len)

# Find files in the specified directory matching the pattern
for filename in glob.glob(cwd):
    print(filename)

    # Count the number of sequences in the file
    numSeqs = open(filename, 'r').read().count(">")
    print(numSeqs)

    # Open the file for reading
    f = open(filename, 'r')

    # Read all lines from the file
    lines = f.readlines()
    sample = 0

    # Process each line
    for line in lines:
        i = lines.index(line)
        seq = lines[i + 1]

        # Check if the line is a sequence header
        if line.startswith(">") == True:
            sample = sample + 1
            print(sample)

        # Check if the line is a sequence header and the sequence length matches the desired length
        if line.startswith(">") == True and len(seq.strip()) == total_len:
            print(seq[upstream:upstream + 3])
            struct = lines[i + 2]
            print(struct[upstream:upstream + 3])

            # Extract the value from the structure line
            if len(struct.split(" ")) > 2:
                val = struct.split(" ")[2].strip()[:-1]
            elif len(struct.split(" ")) == 2:
                val = struct.split(" ")[1].strip()[1:-1]
            print(val)

            # Calculate GC content for upstream and downstream regions
            up_seq = seq[:upstream]
            g = seq[:upstream].upper().count("G")
            c = seq[:upstream].upper().count("C")
            gc_up = (g + c) / float(len(seq))

            g = seq[upstream:].upper().count("G")
            c = seq[upstream:].upper().count("C")
            gc_down = (g + c) / float(len(seq))

            # Calculate GC ratio
            gc_ratio = float(gc_up) / float(gc_down)

            # Create a string combining value and GC ratio
            string = str(val) + '\t' + str(gc_ratio)
            dicts[line.strip()[1:]] = string

            # Check if it is the last sequence
            if numSeqs == sample:
                print('reached last sequence')
                break

# Write the dictionary to a CSV file
a_file = open(sys.argv[4], "w")
writer = csv.writer(a_file, delimiter='\t')
for key, value in dicts.items():
    writer.writerow([key, value])

a_file.close()