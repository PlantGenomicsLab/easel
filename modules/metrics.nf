    process MIKADO {
   label 'process_medium'
   tag {id}
    publishDir "$params.outdir/metrics/mikado",  mode: 'copy'

    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    tuple val(id), path(prediction), path(reference)
    val(mikado)
     
    output: 
    path("${id}*"), emit: prediction

      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}

    """
}

process BUSCO {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy', pattern: "*filtered*/*"

    tag {id}
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.7.0--pyhdfd78af_1' :
        'quay.io/biocontainers/busco:5.7.0--pyhdfd78af_1' }"

    input:
    tuple val(id), path(protein)
    val(odb)

    output:
    tuple val(id), path("*filtered*/*"), emit: busco
    tuple val(id), path("*filtered*/*.txt"), emit: busco_txt
    tuple val(id), path("busco_complete.txt"), emit: busco_complete
      
    """
    busco -i ${protein} -l ${odb} -o ${id} -m Protein -r -c ${task.cpus}
    awk 'NR>3 { print \$3 }' *filtered*/*run_*/full_table.tsv > busco_complete.txt

    if [[ "${id}" == *"longest_isoform"* ]]; then
        id=\$(echo "${id}" | sed -E 's/.*_([^_]+_[^_]+_[^_]+)\$/\\1/')
    else
        id=\$(echo "${id}" | sed -E 's/.*_([^_]+)\$/\\1/')
    fi
    mv ${id} \$id
    """
}

process AGAT {
    publishDir "$params.outdir/metrics/agat",  mode: 'copy'
    label 'process_low'
    tag {id}

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(gff)
    val(species)

    output:
    tuple val(id), path("*.txt"), emit: stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${id}.txt
    
    if [[ "${id}" == *"longest_isoform"* ]]; then
        id=\$(echo "${id}" | sed -E 's/.*_([^_]+_[^_]+_[^_]+)\$/\\1/')
    else
        id=\$(echo "${id}" | sed 's/.*_//')
    fi

    mv ${id}.txt \$id.txt
    """
}

process ENTAP {
    publishDir "$params.outdir/08_functional_annotation",  mode: 'copy', pattern: '*filtered*/*' 

    tag {id}
    label 'process_high'

    container "plantgenomics/entap:2.0.0"
    
    input:
    tuple val(id), path(protein), path(entap_db), path(eggnog_db), path(eggnog_dir)
    path(database)
    val(taxon)
    val(tcoverage)
    val(qcoverage)
    val(contam)

    output:
    tuple val(id), path("*filtered*/*"), emit: entap
    tuple val(id), path("*filtered*/log*"), emit: entap_log
    tuple val(id), path("*filtered*/final_results/entap_results.tsv"), emit: entap_results
    path("filtered/*"), emit: entap_filtered, optional: true
    path("filtered/log*"), emit: entap_log_filtered, optional: true
    path("filtered/final_results/entap_results.tsv"), emit: entap_results_filtered, optional: true
    path("unfiltered/*"), emit: entap_unfiltered, optional: true
    path("unfiltered/log*"), emit: entap_log_unfiltered, optional: true
    path("unfiltered/final_results/entap_results.tsv"), emit: entap_results_unfiltered, optional: true

    """
    entap_db=\$(readlink -f ${entap_db}) 
    eggnog_db=\$(readlink -f ${eggnog_db}) 
    eggnog_dir=\$(readlink -f ${eggnog_dir})
    protein=\$(readlink -f ${protein})

    if [ -n "${contam}" ]; then
        eggcontam="true"
    else
        eggcontam="false"
    fi

    mkdir -p dmnd
    mv ${database} dmnd
    database=\$(readlink -f dmnd/* | paste -sd, -)
         
    if [[ "${id}" == *"longest_isoform"* ]]; then
        id=\$(echo "${id}" | sed -E 's/.*_([^_]+_[^_]+_[^_]+)\$/\\1/')
    else
        id=\$(echo "${id}" | sed -E 's/.*_([^_]+)\$/\\1/')
    fi
    
    if [[ ${task.memory.giga} -lt 40 ]]; then
        dbmem="false"
    else
        dbmem="true"
    fi

    cat <<EOF > entap_run.params
    fpkm=0.5
    align=
    single-end=false
    transdecoder-m=100
    transdecoder-no-refine-starts=false
    out-dir=\$PWD/\$id
    overwrite=false
    resume=false
    input=\$protein
    database=\$database
    no-trim=false
    threads=${task.cpus}
    output-format=1,3,4,7,
    hgt-donor=
    hgt-recipient=
    hgt-gff=
    ontology_source=0,
    eggnog-contaminant=\$eggcontam
    eggnog-dbmem=\$dbmem
    eggnog-sensitivity=more-sensitive
    diamond-sensitivity=very-sensitive
    interproscan-db=
    taxon=${taxon}
    qcoverage=${qcoverage}
    tcoverage=${tcoverage}
    contam=${contam}
    e-value=1e-05
    uninformative=conserved,predicted,unknown,unnamed,hypothetical,putative,unidentified,uncharacterized,uncultured,uninformative,
    EOF

    cat <<EOF > entap_config.ini
    data-generate=false
    data-type=0,
    entap-db-bin=\$entap_db
    entap-db-sql=
    entap-graph=${projectDir}/bin/entap_graphing.py
    rsem-calculate-expression=rsem-calculate-expression
    rsem-sam-validator=rsem-sam-validator
    rsem-prepare-reference=rsem-prepare-reference
    convert-sam-for-rsem=convert-sam-for-rsem
    transdecoder-long-exe=/usr/lib/TransDecoder-v5.7.1/TransDecoder.LongOrfs
    transdecoder-predict-exe=/usr/lib/TransDecoder-v5.7.1/TransDecoder.Predict
    eggnog-map-exe=emapper.py
    eggnog-map-data=\$eggnog_dir
    eggnog-map-dmnd=\$eggnog_db
    interproscan-exe=interproscan.sh
    diamond-exe=diamond
    EOF

    EnTAP --runP --entap-ini entap_config.ini --run-ini entap_run.params 
    """
}
process ENTAP_HGT {
    publishDir "$params.outdir/08_functional_annotation",  mode: 'copy', pattern: 'filtered_longest_isoform/*' 

    tag {id}
    label 'process_high'

    container "plantgenomics/entap:2.0.0"
    
    input:
    tuple val(id), path(protein)
    path(hgt_gff)
    path(entap_db)
    path(eggnog_db)
    path(eggnog_dir)
    path(hgt_donor, stageAs: "hgt_donor.dmnd" )
    path(hgt_recipient, stageAs: "hgt_recipient.dmnd")
    path(database)
    val(taxon)
    val(tcoverage)
    val(qcoverage)
    val(contam)

    output:
    path("filtered_longest_isoform/*"), emit: entap_hgt
    path("filtered_longest_isoform/log*"), emit: entap_log_hgt
    path("filtered_longest_isoform/final_results/entap_results.tsv"), emit: entap_results_hgt

    """
    entap_db=\$(readlink -f ${entap_db}) 
    eggnog_db=\$(readlink -f ${eggnog_db}) 
    eggnog_dir=\$(readlink -f ${eggnog_dir})
    hgt_donor=\$(readlink -f ${hgt_donor})
    hgt_recipient=\$(readlink -f ${hgt_recipient})
    protein=\$(readlink -f ${protein})
    mkdir -p dmnd
    mv ${database} dmnd
    database=\$(readlink -f dmnd/* | paste -sd, -)
    id=\$(echo "${id}" | sed 's/.*_//')


    if [ -n "${contam}" ]; then
        eggcontam="true"
    else
        eggcontam="false"
    fi

    if [[ ${task.memory.giga} -lt 40 ]]; then
        dbmem="false"
    else
        dbmem="true"
    fi

    awk '\$3 == "transcript" {print \$0}' ${hgt_gff} > hgt.gff
    hgt_gff=\$(readlink -f hgt.gff)

    cat <<EOF > entap_run.params
    fpkm=0.5
    align=
    single-end=false
    transdecoder-m=100
    transdecoder-no-refine-starts=false
    out-dir=\$PWD/\$id
    overwrite=false
    resume=false
    input=\$protein
    database=\$database
    no-trim=false
    threads=${task.cpus}
    output-format=1,3,4,7,
    hgt-donor=\$hgt_donor
    hgt-recipient=\$hgt_recipient
    hgt-gff=\$hgt_gff
    ontology_source=0,
    eggnog-contaminant=\$eggcontam
    eggnog-dbmem=\$dbmem
    eggnog-sensitivity=more-sensitive
    diamond-sensitivity=very-sensitive
    interproscan-db=
    taxon=${taxon}
    qcoverage=${qcoverage}
    tcoverage=${tcoverage}
    contam=${contam}
    e-value=1e-05
    uninformative=conserved,predicted,unknown,unnamed,hypothetical,putative,unidentified,uncharacterized,uncultured,uninformative,
    EOF

    cat <<EOF > entap_config.ini
    data-generate=false
    data-type=0,
    entap-db-bin=\$entap_db
    entap-db-sql=
    entap-graph=${projectDir}/bin/entap_graphing.py
    rsem-calculate-expression=rsem-calculate-expression
    rsem-sam-validator=rsem-sam-validator
    rsem-prepare-reference=rsem-prepare-reference
    convert-sam-for-rsem=convert-sam-for-rsem
    transdecoder-long-exe=/usr/lib/TransDecoder-v5.7.1/TransDecoder.LongOrfs
    transdecoder-predict-exe=/usr/lib/TransDecoder-v5.7.1/TransDecoder.Predict
    eggnog-map-exe=emapper.py
    eggnog-map-data=\$eggnog_dir
    eggnog-map-dmnd=\$eggnog_db
    interproscan-exe=interproscan.sh
    diamond-exe=diamond
    EOF

    EnTAP --runP --entap-ini entap_config.ini --run-ini entap_run.params 
    """
}
process ADD_FUNCTION {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'

    label 'process_single'
    tag {id}

    container 'plantgenomics/easel:ncbi-datasets'

    input:
    tuple val(id), path(gff), path(entap)

    output:
    tuple val(id), path("*functional.gff")

    """
    python3 ${projectDir}/bin/ncbi_gff.py ${entap} ${gff} ${projectDir}/bin/pfam_accessions.txt ${id}_functional.gff

    """
}
process LOG {
    label 'process_single'
    tag {id}
    publishDir "$params.outdir/log",  mode: 'copy' 
   
    input:
    tuple val(id), path(busco), path(agat), path(entap)
 
    output:
    path("log*_unfiltered.txt"), emit: unfiltered, optional: true
    path("log*_filtered.txt"), emit: filtered, optional: true
    path("log*_longest_isoform.txt"), emit: longest, optional: true
      
    script:

    """
    grep -m 1 "Number of gene" ${agat} > genes.txt
    grep -m 1 "Number of transcript" ${agat} > transcripts.txt

    if grep -q "No contaminants were selected by user" ${entap}; then
        grep "Total unique sequences with an alignment:" ${entap} > aligned.txt
    else
        grep "Total unique sequences with an alignment:" ${entap} > aligned.txt
        grep "Total alignments flagged as a Similarity Search contaminant:" ${entap} > contam.txt
        grep "contam:" ${entap} > taxon.txt
    fi

    if grep -q "The following Horizontally Transferred Genes were found" ${entap}; then
        grep "The following Horizontally Transferred Genes were found:" ${entap} > hgt.txt
        hgt=\$(cut -d':' -f2 hgt.txt | awk -F',' '{print NF}')
    fi

    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt

    alignments=\$(sed 's/.*: //' aligned.txt | sed 's@^[^0-9]*\\([0-9]\\+\\).*@\\1@')
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    genes=\$(grep -o '[0-9]\\+' genes.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)

    # Check if "Number of single exon gene" exists before creating mono.txt
    if grep -q "Number of single exon gene" ${agat}; then
       grep -m 1 "Number of single exon gene" ${agat} > mono.txt
       mono=\$(grep -o '[0-9]\\+' mono.txt)
       multi=\$((\$genes - \$mono))
       mono_multi=\$(echo "scale=3 ; \$mono / \$multi" | bc)
    else
       mono_multi="0"  
    fi

    entap=\$(echo "scale=3 ; \$alignments / \$transcripts * 100" | bc | xargs printf "%.1f\n") 

    if [[ "${id}" == *_filtered ]]; then
    	echo "##### Filtered #####" > log_${id}.txt
    elif [[ "${id}" == *_unfiltered* ]]; then
    	echo "##### Unfiltered #####" > log_${id}.txt
    elif [[ "${id}" == *_filtered_longest* ]]; then
    	echo "##### Filtered (Longest Isoform)  #####" > log_${id}.txt
    elif [[ "${id}" == *_filtered_primary* ]]; then
    	echo "##### Filtered (Primary Isoform)  #####" > log_${id}.txt
    fi

    echo "Total number of genes: \$genes" >> log_${id}.txt
    if [[ "${id}" == *_filtered ]] || [[ "${id}" == *unfiltered* ]]; then
    echo "Total number of transcripts: \$transcripts" >> log_${id}.txt
    fi
    echo "EnTAP alignment rate: "\$entap"%" >> log_${id}.txt
    if grep -q "Total alignments flagged as a Similarity Search contaminant" ${entap}; then
        contam_alignments=\$(sed 's/.*: //' contam.txt | sed 's@^[^0-9]*\\([0-9]\\+\\).*@\\1@')
        contam=\$(echo "scale=3 ; \$contam_alignments / \$transcripts" * 100 | bc | xargs printf "%.1f\n" )
        taxon=\$(sed 's/contam://g' taxon.txt | sed 's/,\$//' taxon.txt)
        echo "EnTAP alignment contaminant rate (\$taxon): "\$contam"%" >> log_${id}.txt
    fi
    if grep -q "The following Horizontally Transferred Genes were found" ${entap}; then
        echo "Total HGT candidates: \$hgt" >> log_${id}.txt
    fi
    if grep -q "No Horizontally Transferred Genes were found!" ${entap}; then
        echo "No HGT candidates were detected" >> log_${id}.txt
    fi
    echo "Mono-exonic/multi-exonic ratio: \$mono_multi " >> log_${id}.txt
    echo "BUSCO (\$odb): \$busco" >> log_${id}.txt
    """
}
