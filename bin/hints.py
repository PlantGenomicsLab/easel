#!/usr/bin/env python

#########################################################
#                                                       #
# hints.py - format TransDecoder hints file to be used  #
# by AUGUSTUS                                           #
#                                                       #
# Developed by Cynthia Webster (2024)                   #
# Plant Computational Genomics Lab                      #
# University of Connecticut                             #
#                                                       #
# This script is under the MIT License                  #
#                                                       #
#########################################################

import sys

def process_gff(input_file, output_file):
    with open(input_file, 'r') as f_in:
        with open(output_file, 'w') as f_out:
            for line in f_in:
                # Remove mRNA and gene lines
                if "mRNA" in line or "gene" in line:
                    continue
                
                # Replace features as requested
                line = line.replace("five_prime_UTR", "UTR").replace("three_prime_UTR", "UTR").replace("intergenic_region", "irpart").replace("exon", "exonpart").replace("CDS", "CDSpart").replace("start_codon", "start").replace("stop_codon", "stop")
                
                # Check if the line contains the ID attribute
                if 'ID=' in line:
                    # Split the line by tabs
                    fields = line.strip().split('\t')
                    # Split the attribute field by semicolon
                    attributes = fields[8].split(';')
                    # Filter out the ID attribute and reconstruct the attributes field
                    attributes = [attr for attr in attributes if not attr.startswith('ID=')]
                    # Replace Parent with grp
                    attributes = [attr.replace('Parent=', 'grp=') for attr in attributes]
                    fields[8] = ';'.join(attributes)
                    # Add additional attributes
                    if fields[8]:
                        fields[8] += ';'
                    fields[8] += 'pri=4;src=G'
                    # Write the modified line to the output file
                    f_out.write('\t'.join(fields) + '\n')
                else:
                    # Write the unmodified line to the output file
                    f_out.write(line)

# Ensure this is outside of the function definition
if __name__ == "__main__":
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    
    process_gff(input_file, output_file)
