#!/usr/bin/env python

########################################################
#                                                      #
# add_molecular_weight.py - calculates the molecular   #
# weight of each transcript                            #
#                                                      #
# Developed by Cynthia Webster (2022)                  #
# Updated - 2024                                       #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

# Import necessary modules
from Bio import SeqIO
import pandas as pd
import sys
import re
from Bio.SeqUtils import molecular_weight

# Retrieve file path from command-line arguments
filepath = sys.argv[1]

# Define a function to calculate molecular weight with error handling
def calculate_molecular_weight(sequence):
    try:
        weight = ("%0.2f" % molecular_weight(sequence))
    except Exception as e:
        print(f"Error calculating molecular weight: {e}")
        weight = "NaN"
    return weight

# Create a list to store sequence information
data = []

# Process sequences one by one
for record in SeqIO.parse(filepath, 'fasta'):
    seq_id = record.id
    seq_description = record.description
    fields = dict(re.findall(r'(\w+)=(.*?) ', seq_description))
    seq_gene = fields.get('gene', "Unknown")
    sequence = record.seq.upper()  # Convert sequence to uppercase
    sequence = str(sequence)

    # Remove regions that do not contain ACGT
    sequence_cleaned = re.sub(r'[^ACGTacgt]','', sequence)
    
    length = len(sequence_cleaned)
    weight = calculate_molecular_weight(sequence_cleaned)
    
    # Collect sequence information
    data.append({
        'Transcript_ID': seq_id,
        'Gene_ID': seq_gene,
        'Seq_Length': length,
        'Molecular_Weight': weight
    })

# Convert the list to a DataFrame
dataframe = pd.DataFrame(data, columns=['Transcript_ID', 'Gene_ID', 'Seq_Length', 'Molecular_Weight'])

# Save the DataFrame to a tab-delimited text file
dataframe.to_csv('masked.tracking', sep='\t', encoding='utf-8', index=False)

