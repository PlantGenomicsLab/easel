#!/usr/bin/env python

########################################################
#                                                      #
# atg_count.py - count number of in-frame ATGs in      #
#  transcript nucleotide sequence                      #
#                                                      #
# Developed by Cynthia Webster (2024)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import sys
from Bio import SeqIO

# Check if the correct number of arguments is provided
if len(sys.argv) != 3:
    print("Usage: python script.py <input_file.fasta> <output_file.tsv>")
    sys.exit(1)

# Get the input and output file names from the command-line arguments
input_file = sys.argv[1]
output_file = sys.argv[2]

# Function to count the number of ATG codons in the sequence
def count_atg(sequence):
    count = 0
    # Convert the sequence to uppercase to ensure case insensitivity
    sequence = sequence.upper()
    
    # Iterate through the sequence starting from index 0 and count occurrences of 'ATG'
    for i in range(0, len(sequence) - 2, 3):  # Step by 3 to get codons
        codon = sequence[i:i+3]
        if codon == "ATG":
            count += 1
    return count

# Open the FASTA file and process each sequence
with open(input_file) as fasta_file, open(output_file, 'w') as output_tsv:
    # Write the header for the output file
    output_tsv.write("Transcript\tHit\n")

    # Parse the FASTA file and count ATG codons
    for record in SeqIO.parse(fasta_file, "fasta"):
        # Get the transcript ID and sequence
        transcript_id = record.id
        sequence = str(record.seq)  # Convert sequence to string
        
        # Count ATG codons
        atg_count = count_atg(sequence)
        
        # Write the transcript ID and ATG count to the output file
        output_tsv.write(f"{transcript_id}\t{atg_count}\n")

# Notify user of completion
print(f"ATG codon counts have been written to {output_file}")

