process ORTHODB_BLAST {
    label 'process_medium'
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.1.10--h43eeafb_2' :
        'quay.io/biocontainers/diamond:2.1.10--h43eeafb_2' }"

    input:
    path(orthodb)
    path(unfiltered_pep)
    val(qcoverage)
    val(scoverage)

    output:
    path "hits.txt", emit: hits

    script:
    """
diamond makedb --in ${orthodb} -d reference --threads ${task.cpus}
diamond blastp --more-sensitive --query-cover ${qcoverage} --subject-cover ${scoverage} -d reference -q ${unfiltered_pep} -k 1 -o hits.txt --threads ${task.cpus}

"""
}
process ORTHODB_FEATURE {
    label 'process_low'
    container 'plantgenomics/easelv2.1:python'

    input:
    path(matrix)
    path(orthodb)

    output:
    path "orthodb_bitscore.tracking", emit: bitscore
    path "orthodb_similarity.tracking", emit: similarity
    path "orthodb_evalue.tracking", emit: evalue
    path "orthodb_length.tracking", emit: length
    path "orthodb_mismatch.tracking", emit: mismatch
    path "orthodb_querystart.tracking", emit: querystart
    path "orthodb_queryend.tracking", emit: queryend
    path "orthodb_sstart.tracking", emit: sstart
    path "orthodb_send.tracking", emit: send
    path "orthodb_support.tracking", emit: support

    script:
    """

awk '!/^#/ {print \$1 "\t" \$12}' ${orthodb} > bitscore.txt
sed -i \$'1 i\\\nTranscript\tHit' bitscore.txt
python ${projectDir}/bin/map_transcript.py ${matrix} bitscore.txt OrthoDB_Bitscore feature1.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature1.tracking > orthodb_bitscore.tracking

awk '!/^#/ {print \$1 "\t" \$3}' ${orthodb} > similarity.txt
sed -i \$'1 i\\\nTranscript\tHit' similarity.txt
python ${projectDir}/bin/map_transcript.py ${matrix} similarity.txt OrthoDB_SimilarityScore feature2.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature2.tracking > orthodb_similarity.tracking

awk '!/^#/ {print \$1 "\t" \$11}' ${orthodb} > evalue.txt
sed -i \$'1 i\\\nTranscript\tHit' evalue.txt
python ${projectDir}/bin/map_transcript.py ${matrix} evalue.txt OrthoDB_Evalue feature3.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature3.tracking > orthodb_evalue.tracking

awk '!/^#/ {print \$1 "\t" \$4}' ${orthodb} > length.txt
sed -i \$'1 i\\\nTranscript\tHit' length.txt
python ${projectDir}/bin/map_transcript.py ${matrix} length.txt OrthoDB_Length feature4.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature4.tracking > orthodb_length.tracking

awk '!/^#/ {print \$1 "\t" \$5}' ${orthodb} > mismatch.txt
sed -i \$'1 i\\\nTranscript\tHit' mismatch.txt
python ${projectDir}/bin/map_transcript.py ${matrix} mismatch.txt OrthoDB_Mismatch feature5.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature5.tracking > orthodb_mismatch.tracking

awk '!/^#/ {print \$1 "\t" \$7}' ${orthodb} > qstart.txt
sed -i \$'1 i\\\nTranscript\tHit' qstart.txt
python ${projectDir}/bin/map_transcript.py ${matrix} qstart.txt OrthoDB_QueryStart feature6.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature6.tracking > orthodb_querystart.tracking

awk '!/^#/ {print \$1 "\t" \$8}' ${orthodb} > qend.txt
sed -i \$'1 i\\\nTranscript\tHit' qend.txt
python ${projectDir}/bin/map_transcript.py ${matrix} qend.txt OrthoDB_QueryEnd feature7.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature7.tracking > orthodb_queryend.tracking

awk '!/^#/ {print \$1 "\t" \$9}' ${orthodb} > sstart.txt
sed -i \$'1 i\\\nTranscript\tHit' sstart.txt
python ${projectDir}/bin/map_transcript.py ${matrix} sstart.txt OrthoDB_SubjectStart feature8.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature8.tracking > orthodb_sstart.tracking

awk '!/^#/ {print \$1 "\t" \$10}' ${orthodb} > send.txt
sed -i \$'1 i\\\nTranscript\tHit' send.txt
python ${projectDir}/bin/map_transcript.py ${matrix} send.txt OrthoDB_SubjectEnd feature9.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature9.tracking > orthodb_send.tracking

awk '!/^#/ {print \$1}' ${orthodb} > ids.txt
awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' ids.txt > support.txt
sed -i \$'1 i\\\nTranscript\tHit' support.txt
python ${projectDir}/bin/map_transcript.py ${matrix} support.txt OrthoDB_Support feature10.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = 0 
    }
else if (\$8 == 1.0){
    \$8 = 1
    } 
}   1' feature10.tracking > orthodb_support.tracking

"""
}
