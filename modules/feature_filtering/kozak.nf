process KOZAK_SEQUENCE {
    label 'process_medium'
    container 'plantgenomics/easelv2.1:python'
     
    input:
    path(genome)
    path(gff)
    path(matrix)
     
    output: 
    path("kozak_neg3.tracking"), emit: neg3
    path("kozak_pos4.tracking"), emit: pos4 
    
    """
    python ${projectDir}/bin/kozak_sequence.py ${genome} ${gff} kozak.txt
    awk -F'\t' -v OFS='\t' 'NR > 1 {print \$1, \$3}' kozak.txt | sed \$'1 i\\\nTranscript\tHit' > three.txt
    
    awk 'BEGIN {OFS="\t"} NR==1 {print \$0, "Hit"} NR>1 {
    if (\$2 == "A" || \$2 == "G") {
        print \$0, 1
    } else {
        print \$0, 0
    }
    }' three.txt | cut -f1,3 > three_purine.txt
   
    awk -F'\t' -v OFS='\t' 'NR > 1 {print \$1, \$4}' kozak.txt | sed \$'1 i\\\nTranscript\tHit' > four.txt
    python ${projectDir}/bin/map_transcript.py ${matrix} three_purine.txt Kozak_Neg_3_Purine kozak_neg3.tracking
    python ${projectDir}/bin/map_transcript.py ${matrix} four.txt Kozak_Pos_4 kozak_pos4.tracking

    """
}
