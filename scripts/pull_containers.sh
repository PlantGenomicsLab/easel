#!/bin/bash
#SBATCH --job-name=script
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

#module load singularity
export SINGULARITY_TMPDIR=$PWD/tmp
mkdir -p singularity
# Define the container names and versions
containers=("agat:1.4.0--pl5321hdfd78af_0"
            "viennarna:2.6.4--py310pl5321h6cc9453_1"
            "subread:2.0.6--he4a0461_0"
            "seqkit:2.3.1--h9ee0642_0"
            "eggnog-mapper:2.1.12--pyhdfd78af_0"
            "mikado:2.3.4--py39h919a90d_0"
            "busco:5.7.0--pyhdfd78af_1"
            "seqtk:1.3--h7132678_4"
            "sra-tools:3.1.1--h4304569_2"
            "fastp:0.23.2--hb7a2d85_2"
            "miniprot:0.13--he4a0461_0"
            "hisat2:2.2.1--py38he1b5a44_0"
            "samtools:1.20--h50ea8bc_0"
            "stringtie:2.2.3--h43eeafb_0"
            "transdecoder:5.7.1--pl5321hdfd78af_0"
            "vsearch:2.28.1--h6a68c12_0"
            "gffread:0.12.7--hd03093a_1"
            "repeatmodeler:2.0.5--pl5321hdfd78af_0"
            "repeatmasker:4.1.7p1--pl5321hdfd78af_1"
            "diamond:2.1.10--h43eeafb_2"
            "hmmer:3.4--hdbdd923_1"
            "gffcompare:0.12.6--h4ac6f70_2")

# Loop through the containers and build Singularity images
for container in "${containers[@]}"; do
    image_name="$(echo "${container}" | tr ':' '-' )"
    singularity pull singularity/depot.galaxyproject.org-singularity-${image_name}.img https://depot.galaxyproject.org/singularity/${container}
done

containers=("plantgenomics/psiclass:1.0.3-fork" 
            "plantgenomics/easelv2.1:python"
            "plantgenomics/easel:perl"
            "plantgenomics/entap:2.0.0"
            "plantgenomics/easel:augustus")

for container in "${containers[@]}"; do
    image_name="$(echo "${container}" | tr '/' '-' | tr ':' '-')"
    singularity pull singularity/${image_name}.img "docker://${container}"
done

path=$(realpath singularity)
echo "Use the following parameter: --singularity_cache_dir '$path'"
