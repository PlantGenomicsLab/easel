#!/bin/bash
#SBATCH --job-name=EASELv_2.1.0-beta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

#If Nextflow and Singularity are not available as modules, be sure to follow dependency instructions
module load nextflow
module load singularity

#Singularity may default to a scratch directory, I choose to redirect this into a new location with more storage
mkdir -p tmp
SINGULARITY_TMPDIR=$PWD/tmp
export SINGULARITY_TMPDIR

#The `xanadu` profile is specific to the UConn HPC, other available profiles can be found here: https://github.com/nf-core/configs. Otherwise specify singularity/docker/etc. and put desired resources in header (adjust max memory/cpu as needed)
nextflow run -hub gitlab PlantGenomicsLab/easel -profile xanadu -params-file params.yaml
