process FIND_DUPS {
    label 'process_low'
    publishDir "$params.outdir/07_filtering",  mode: 'copy', pattern: '*.tsv'
    container 'plantgenomics/easelv2.1:python'
    
    input:
    path(features)
    path(gff)
     
    output: 
    path("coding_dups.tsv"), emit: dups
    path("drop.txt"), emit: drop
    
    """

python ${projectDir}/bin/find_duplicates.py ${features}
python ${projectDir}/bin/pick_duplicate.py coding_dups.tsv ${gff} best_pick.txt

if [ ! -s coding_dups.tsv ]; then
    echo "coding_dups.tsv is empty. Skipping the parsing step."
    touch drop.txt
else
    echo "Parsing coding_dups.tsv..."
    cut -f2 coding_dups.tsv | tr ',' '\n' | grep -v -F -f best_pick.txt > drop.txt
fi

    """
}
process FILTER_TRAINING_SET {
    label 'process_low'
    
    input:
    path(train)
    val(taxa)
     
    output: 
    path("filtered_*"), emit: track

    script:

    if (params.training_set == "plant")
    """
    basename=\$(basename ${train})
    echo "${taxa}" | grep -E 'a_thaliana|p_patens|p_trichocarpa' > /dev/null
    if [ \$? -eq 0 ]; then
        echo "String contains one of the species."
        taxa=\$(echo "${taxa}" | tr ',' '|')
        (head -n 1 && grep -E "\$taxa") < ${train} > filtered_\$basename
    else
        echo "String does not contain any of the species."
        exit 1
    fi
    
    """
    else if (params.training_set == "invertebrate")
    """
    basename=\$(basename ${train})
    echo "${taxa}" | grep -E 'a_aegypti|d_melanogaster|c_elegans' > /dev/null
    if [ \$? -eq 0 ]; then
        echo "String contains one of the species."
        taxa=\$(echo "${taxa}" | tr ',' '|')
        (head -n 1 && grep -E "\$taxa") < ${train} > filtered_\$basename
    else
        echo "String does not contain any of the species."
        exit 1
    fi
    
    """
    else if (params.training_set == "vertebrate")
    """
    basename=\$(basename ${train})
    echo "${taxa}" | grep -E 'd_rerio|m_musculus|x_laevis' > /dev/null
    if [ \$? -eq 0 ]; then
        echo "String contains one of the species."
        taxa=\$(echo "${taxa}" | tr ',' '|')
        (head -n 1 && grep -E "\$taxa") < ${train} > filtered_\$basename
    else
        echo "String does not contain any of the species."
        exit 1
    fi
    
    """
}
process REGRESSOR {
    label 'process_medium'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    container 'plantgenomics/easelv2.1:python'
    
    input:
    path(test)
    path(train)
    val(stringency)
     
    output: 
    path("regressor_prediction.csv"), emit: r_rf

    """
    python ${projectDir}/bin/random_forest_regressor_predict.py ${train} ${test} regressor_prediction.csv ${stringency}

    """
}
process CLASSIFIER {
    label 'process_medium'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    container 'plantgenomics/easelv2.1:python'
    
    input:
    path(test)
    path(train)
    val(stringency)
     
    output: 
    path("classifier_prediction.csv"), emit: c_rf
      
    """
    python ${projectDir}/bin/random_forest_classifier_predict.py ${train} ${test} classifier_prediction.csv ${stringency}

    """
}

process RF_FILTER {
    label 'process_low'

    input:
    path(classifier)
    path(regressor)
    path(dups)
    path(unfiltered_gff)
    val(reg_val)
    path(pfam)
     
    output: 
    path("rf.gff"), emit: filtered_prediction
    
    script:
    if (params.pfam == false && params.remove_dups == false)
    """
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gff} > rf.gff
sed -i 's/\ttranscript\t/\tmRNA\t/' rf.gff 

    """
    else if (params.pfam == true && params.remove_dups == true)
    """
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gff} > tmp.gff

if [ ! -s ${dups} ]; then
	echo "drop.txt is empty"
        mv tmp.gff tmp2.gff
else
	awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (!(m[1] in transcripts)) print }' ${dups} tmp.gff > tmp2.gff
fi

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' ${pfam} tmp2.gff > rf.gff
sed -i 's/\ttranscript\t/\tmRNA\t/' rf.gff
   """
    else if (params.pfam == true && params.remove_dups == false)
    """
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gff} > tmp.gff
awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' ${pfam} tmp.gff > rf.gff
sed -i 's/\ttranscript\t/\tmRNA\t/' rf.gff
   """
    else if (params.pfam == false && params.remove_dups == true)
    """
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gff} > tmp.gff

if [ ! -s ${dups} ]; then
	echo "drop.txt is empty."
        mv tmp.gff rf.gff
else
	awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /Parent=([^;]+)/, m); if (!(m[1] in transcripts)) print }' ${dups} tmp.gff > rf.gff
fi

sed -i 's/\ttranscript\t/\tmRNA\t/' rf.gff
   """
}

process CLEAN_GTF {
    label 'process_medium'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(prefix)

    output: 
    path("*filtered.gtf"), emit: filtered_gtf
      
    """
agat_sp_fix_overlaping_genes.pl -f ${gff} --out tmp.gff
sed -i 's/agat-/${prefix}_/g' tmp.gff
agat_convert_sp_gff2gtf.pl --gff tmp.gff -o tmp.gtf
grep -v '^#' tmp.gtf | awk -F'\\t' '{OFS="\\t"; \$2="EASEL"; print}' | sed 's/\tmRNA\t/\ttranscript\t/' > ${prefix}_filtered.gtf
    """
}
