import nextflow.Nextflow

class WorkflowEasel {

    //
    // Check and validate parameters
    //
    public static void initialise(params, log, valid_params) {
        if (!params.genome) {
            Nextflow.error("ERROR: Please provide an input FASTA file of softmasked sequences to the pipeline e.g. '--genome seqs.faa'")
        }
        
        def allowed_training_sets = ["plant", "invertebrate", "vertebrate"]
        def taxa_groups = [
            "plant": ["a_thaliana", "p_patens", "p_trichocarpa"],
            "invertebrate": ["a_aegypti", "d_melanogaster", "c_elegans"],
            "vertebrate": ["d_rerio", "m_musculus", "x_laevis"]
        ]

        if (params.keep_taxa) {
            // If the training set is not "user", validate taxa
            if (params.training_set == "user") {
                Nextflow.error("ERROR: '--keep_taxa' cannot be used with 'user' training set.")
            }
            else if (params.training_set != "user") {
                def allowed_taxa = taxa_groups.get(params.training_set, [])
                def input_taxa = params.keep_taxa.tokenize(",").collect { it.trim() }

                if (allowed_taxa) {
                    def invalid_taxa = input_taxa.findAll { it !in allowed_taxa }
                    if (invalid_taxa) {
                        Nextflow.error("ERROR: Invalid taxa found in '--keep_taxa': ${invalid_taxa.join(', ')}. Allowed taxa for '${params.training_set}' are: ${allowed_taxa.join(', ')}")
                    }
                } else {
                    Nextflow.error("ERROR: '--keep_taxa' is not supported for training set '${params.training_set}'")
                }
            }
        }
    
        if (!params.reference_db) {
            log.info("WARN: A reference database (--reference_db) for functional annotation via EnTAP was not provided. Pulling complete RefSeq database!")
        }
        if (params.long_intron == true) {
            log.info("WARN: Long intron (--long_intron) is turned on (default: 3000000). If desired, change threshold with --max_intron")
        }
        if (!params.sra && !params.user_reads && !params.bam && !params.bam_long) {
            Nextflow.error("ERROR: Please provide '--sra', '--user_reads', '--bam' and/or '--bam_long' to the pipeline.")
        }
        if (params.build_training_set && !params.reference) {
            Nextflow.error("ERROR: Please provide '--reference' when `--build_training_set true`")
        }
        if (!params.sra && !params.user_reads && !params.bam && params.bam_long) {
            log.info ("WARN: Only '--bam_long' was provided, consider including short reads for increased accuracy.")
        }
        if (!params.busco_lineage) {
            Nextflow.error("ERROR: Please provide a relevant BUSCO protein database to the pipeline from bin/busco_lineage.txt e.g. '--busco_lineage embryophyta'")
        } else if (!valid_params['busco'].contains(params.busco_lineage)) {
            Nextflow.error("ERROR: Invalid option: '${params.busco_lineage}'. Valid options for '--busco_lineage': ${valid_params['busco'].join(', ')}.")
        }
        if (!params.order && !params.user_protein) {
            Nextflow.error("ERROR: Please provide a relevant taxonomic rank to filter OrthoDB proteins from bin/orthodb.txt e.g. '--order Brassicales' or provide user proteins e.g. '--user_protein /path/to/Brassicales.fasta")
        } 
        if (params.order && params.user_protein){
            Nextflow.error("ERROR: EASEL cannot accept `--order` and `--user_proteins` at the same time. Please either provide a relevant taxonomic rank to filter OrthoDB proteins from bin/orthodb.txt e.g. '--order Brassicales' or provide user proteins e.g. '--user_protein /path/to/Brassicales.fasta")
        }
        if (params.order && !valid_params['orthodb'].contains(params.order) && !params.user_protein) {
            Nextflow.error("ERROR: Invalid option: '${params.order}'. Valid options for '--order': ${valid_params['orthodb'].join(', ')}.")
        }
        if (params.resume_filtering == true) {
            log.info ("WARN: Filtering has been resumed! Previous log files will be pulled into 'easel_summary.txt' and BAM alignments from '03_alignments/bam' will be used to calculate transcript expression.")
        }
        if (params.user_protein && !params.order) {
            log.info ("WARN: User proteins (--user_protein) will be used for gene prediction with AUGUSTUS and feature filtering. If you would prefer to use OrthoDB protein evidence, provide a valid '--order' instead.")
        }
        if (!params.taxon) {
           log.info ("WARN: Taxon (--taxon) was not provided. This parameter will improve EnTAP runtime and provide more relevant functional annotations")
        }
        if (params.primary_isoform == false) {
           log.info ("WARN: The primary isoform (--primary_isoform) will not be provided in the final output.")
        }
        if (params.primary_isoform == false && params.hgt_donor != null && params.hgt_recipient != null) {
           Nextflow.error("ERROR: The primary_isoform (--primary_isoform) must be set to true when identifying HGT events with EnTAP.")
        }
        if (params.primary_isoform == true && params.hgt_donor != null && params.hgt_recipient == null) {
           Nextflow.error("ERROR: Path found for HGT donor database (--hgt_donor), please set (--hgt_recipient) to detect HGT events. If you do not wish to invoke this command, remove path for (--hgt_donor).")
        }
        if (params.primary_isoform == true && params.hgt_donor == null && params.hgt_recipient != null) {
           Nextflow.error("ERROR: Path found for HGT recipient database (--hgt_recipient), please set (--hgt_donor) to detect HGT events. If you do not wish to invoke this command, remove path for (--hgt_recipient).")
        }
        if (!(params.stringency in [0, 1, 2, 3])) {
            Nextflow.error("ERROR: Please provide a valid value (0, 1, 2, 3) for --stringency")
        }
        if (params.training_set == "user" && !params.training_set_path) {
           Nextflow.error("ERROR: Please provide a training set path with '--training_set_path' when '--training_set' is set to user")
        }
        if (params.training_set != "user" && params.training_set_path != null) {
           log.info("WARN:'--training_set ${params.training_set}' set, --training_set_path' will be ignored. If this is a mistake, set `--training_set user` and rerun.")
        }
        if (!params.training_set && params.build_training_set == false) {
            Nextflow.error("ERROR: Please provide a training set '--training_set plant'")
        } else if (!valid_params['training'].contains(params.training_set) && params.build_training_set == false) {
            Nextflow.error("ERROR: Invalid option: '${params.training_set}'. Valid options for '--training_set': ${valid_params['training'].join(', ')}.")
        }
    }
}
