include { PSICLASS; STRINGTIE2; MERGE_STRINGTIE2; FASTA; ORF; EGGNOG; PREDICT; GENEMODEL; CLUSTER; GET_COMPLETE; BUSCO; AGAT; LOG } from '../modules/gene_prediction/gene_model.nf'
include { TRANSDECODER_HINTS; FORMAT_HINTS; MINIPROTHINT_ORTHODB_HINTS; ORTHODB_HINTS; TRAINING_SET; GFFREAD; AUGUSTUS_TRANSDECODER; AUGUSTUS_ORTHODB; COMBINE_TRANSDECODER; COMBINE_ORTHODB; COMBINE_RNA; GRAB_TRANSCRIPTS; COMPARE_RNA; PREDICTIONS; FIX_EXONS; RENAME_ATTRIBUTES; COMBINE_ALL; GENE_OVERLAP; COMPLETE_GENEMODEL; FORMAT_GFF; LIST } from '../modules/gene_prediction/prediction.nf'

workflow ANNOTATION {
    take:
    bam
	hisat_alignments
	genome
	split
	configTD
	configProtein
	eggnog_db
	orthodb

    main:
    /////////////////////////////////////////////////////
    //gene_model.nf - assemble and filter gene model   // 
	/////////////////////////////////////////////////////

	if(params.resume_filtering == false){ 

		bam_ch = bam.toList()
    	PSICLASS 					( bam_ch, params.psiclass_args )
    	psiclass_gtf = PSICLASS.out.gtf
		STRINGTIE2	                ( hisat_alignments, params.gap_tolerance, params.stringtie2_args )
		merge_ch = STRINGTIE2.out.stringtie2_gtf.collect()
		MERGE_STRINGTIE2            ( merge_ch, params.stringtie2_merge_args )
		stringtie2_gtf = MERGE_STRINGTIE2.out.merged_gtf

		merge = Channel.empty()
		merge
			.concat(stringtie2_gtf).concat(psiclass_gtf)
			.flatten()
			.map { file -> tuple(file.baseName, file) }
			.view()
			.set { assemblies }

		FASTA			            ( genome, assemblies )
		fasta = FASTA.out.fasta
		gff3 = FASTA.out.gff3

    	ORF          	   			( FASTA.out.fasta )
    	directory = ORF.out.LongOrfsDirFiles
    	EGGNOG                      ( ORF.out.longOrfsPepFiles, eggnog_db )
    	blast = EGGNOG.out.eggnogBlastp

		fasta
        	.join(blast, by:0).join(directory)
			.view()
        	.set { transdecoder }

    	PREDICT 	                ( transdecoder )
    	orf = PREDICT.out.transdecoderGFF3
    	protein = PREDICT.out.transdecoderPEP
   
		orf
        	.join(gff3, by:0).join(fasta)
			.view()
        	.set { model }

		GENEMODEL                   ( model )
		CLUSTER 	                ( PREDICT.out.transdecoderCDS, params.cluster_id )
		centroids = CLUSTER.out.centroids
		gff3_model = GENEMODEL.out.genomeGFF3


		centroids
        	.join(gff3_model, by:0)
			.view()
        	.set { complete }

    	GET_COMPLETE 	            ( complete )
    	gff3_complete = GET_COMPLETE.out.GeneModel
    	BUSCO 						( protein, params.busco_lineage )
    	AGAT 						( gff3_model, params.prefix )

    	busco_txt = BUSCO.out.busco_txt
    	agat_stats = AGAT.out.stats


    	busco_txt
        	.join(agat_stats, by:0)
        	.view()
        	.set { log_files }

    	LOG                     ( log_files )
    	psiclass_log = LOG.out.psiclass
    	stringtie2_log = LOG.out.stringtie2

    //////////////////////////////////////////////////
    //prediction.nf - hint generation and augustus // 
	/////////////////////////////////////////////////
	
		TRANSDECODER_HINTS			( gff3_model, genome )
		FORMAT_HINTS				( TRANSDECODER_HINTS.out.hint )
		
		GFFREAD					    ( GET_COMPLETE.out.GeneModel )
		gtf = GFFREAD.out.gtf

		gtf
        	.join(gff3_complete, by:0)
			.view()
        	.set { train }
        
		TRAINING_SET		            ( train, genome, params.prefix, params.test_count, params.train_count, params.kfold, params.rounds, params.augustus )
		training_set = TRAINING_SET.out.output
		td = FORMAT_HINTS.out.format.join(training_set, by: 0)

		if (params.miniprothint == true){
		MINIPROTHINT_ORTHODB_HINTS         	    ( orthodb )
		orthodb_protein_hints = MINIPROTHINT_ORTHODB_HINTS.out.protein
		}
		else if (params.miniprothint == false){
		ORTHODB_HINTS 				    ( orthodb, genome )
		orthodb_protein_hints = ORTHODB_HINTS.out.protein
		}

		orthodb_protein_hints
        	.map { file -> tuple('stringtie2', file) }
        	.join(training_set, by: 0)
        	.view()
        	.set { stringtie_orthodb }

    	orthodb_protein_hints
        	.map { file -> tuple('psiclass', file) }
        	.join(training_set, by: 0)
        	.view()
        	.set { psiclass_orthodb }

		genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
			
		genome_ch
    		.combine(stringtie_orthodb)
    		.view()
        	.set { st_orthodb_hints }
        	
        genome_ch
    		.combine(psiclass_orthodb)
    		.view()
        	.set { pc_orthodb_hints }

        merge
			.concat( st_orthodb_hints, pc_orthodb_hints )
		//	.transpose()
		//	.flatten()
			.view()
			.set { orthodb_hints }

		AUGUSTUS_ORTHODB 			( configProtein, orthodb_hints, params.prefix, params.augustus )

    	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
    	genome_ch
    		.combine(td)
    		.view()
        	.set { td_hints }

		AUGUSTUS_TRANSDECODER 	    ( configTD, td_hints, params.prefix, params.augustus )
		td_ch = AUGUSTUS_TRANSDECODER.out.trans.collect()
		COMBINE_TRANSDECODER            ( td_ch )

		merge
			.concat( GENEMODEL.out.stringtie2_model).concat(GENEMODEL.out.psiclass_model )
			.flatten()
			.collect()
			.view()
			.set { rna_gff }

		COMBINE_RNA					( rna_gff )
		COMPARE_RNA					( COMBINE_RNA.out.transdecoder )
		GRAB_TRANSCRIPTS			( COMBINE_RNA.out.transdecoder, COMPARE_RNA.out.gffcompare )
		orthodb_ch = AUGUSTUS_ORTHODB.out.orthodb_proteinHints.collect()
		COMBINE_ORTHODB				( orthodb_ch )

		merge
			.concat( GRAB_TRANSCRIPTS.out.rna, COMBINE_TRANSDECODER.out.td_stringtie2, COMBINE_TRANSDECODER.out.td_psiclass, COMBINE_ORTHODB.out.orthodb_psiclass, COMBINE_ORTHODB.out.orthodb_stringtie2 )
			.flatten()
			.map { file -> tuple(file.baseName, file) }
			.view()
			.set { gff }
		merge
			.concat( COMBINE_TRANSDECODER.out.td_stringtie2, COMBINE_TRANSDECODER.out.td_psiclass, COMBINE_ORTHODB.out.orthodb_psiclass, COMBINE_ORTHODB.out.orthodb_stringtie2, GENEMODEL.out.stringtie2_model, GENEMODEL.out.psiclass_model )
			.flatten()
			.collect()
			.view()
			.set { list }
	/*/	else if(params.external_protein == false){
			merge
				.concat( GRAB_TRANSCRIPTS.out.rna, COMBINE_TRANSDECODER.out.td_stringtie2, COMBINE_TRANSDECODER.out.td_psiclass )
				.flatten()
				.map { file -> tuple(file.baseName, file) }
				.view()
				.set { gff }
			merge
				.concat( COMBINE_TRANSDECODER.out.td_stringtie2, COMBINE_TRANSDECODER.out.td_psiclass, GENEMODEL.out.stringtie2_model, GENEMODEL.out.psiclass_model )
				.flatten()
				.collect()
				.view()
				.set { list }
		}
	/*/
		PREDICTIONS					( list )		
		FIX_EXONS                   ( gff, genome )
		RENAME_ATTRIBUTES           ( FIX_EXONS.out.fixed )
		COMBINE_ALL                 ( RENAME_ATTRIBUTES.out.rename.collect() )
		GENE_OVERLAP				( COMBINE_ALL.out.combined )
        COMPLETE_GENEMODEL                      ( GENE_OVERLAP.out.overlap_unfiltered, genome )       
		FORMAT_GFF					( COMPLETE_GENEMODEL.out.start, genome, params.prefix )
		format_gff = FORMAT_GFF.out.unfiltered_fixed
		//I merged RNA together so support is both together
		LIST  						( RENAME_ATTRIBUTES.out.rename.collect() )
	}
	else if(params.resume_filtering == true){
		if(params.augustus_list == null){
			augustus_ch = Channel.fromPath("${launchDir}/${params.outdir}/06_predictions/augustus.txt", checkIfExists: true)
		}
		else if(params.augustus_list != null){
			augustus_ch = Channel.fromPath(params.augustus_list)
		}
		
		augustus_ch.splitCsv().flatten()
			.map { gff -> file(gff) }
			.view()
			.set { augustus_gff }

		if(params.augustus_list == null){
			rna_ch = Channel.fromPath("${launchDir}/${params.outdir}/06_predictions/rna.txt", checkIfExists: true)
		}
		else if(params.augustus_list != null){
			rna_ch = Channel.fromPath(params.rna_list)
		}

		rna_ch.splitCsv().flatten()
			.map { gff -> file(gff) }
			.collect()
			.view()
			.set { rna_gff }

		if (params.miniprothint == true){
			MINIPROTHINT_ORTHODB_HINTS         	    ( orthodb )
			orthodb_protein_hints = MINIPROTHINT_ORTHODB_HINTS.out.protein
		}
		else if (params.miniprothint == false){
			ORTHODB_HINTS 				    ( orthodb, genome )
			orthodb_protein_hints = ORTHODB_HINTS.out.protein
		}
		COMBINE_RNA					( rna_gff )
		COMPARE_RNA					( COMBINE_RNA.out.transdecoder )
		GRAB_TRANSCRIPTS			( COMBINE_RNA.out.transdecoder, COMPARE_RNA.out.gffcompare )
		compare_rna = GRAB_TRANSCRIPTS.out.rna

		merge = Channel.empty()
		merge
			.concat( augustus_gff, compare_rna )
			.flatten()
			.map{ file -> tuple(file.baseName, file)}
			.view()
			.set { gff }

		psiclass_log = Channel.fromPath("${launchDir}/${params.outdir}/log/log_psiclass.txt", checkIfExists: true)
        stringtie2_log = Channel.fromPath("${launchDir}/${params.outdir}/log/log_stringtie2.txt", checkIfExists: true)

		FIX_EXONS                   ( gff, genome )
		RENAME_ATTRIBUTES           ( FIX_EXONS.out.fixed )
		COMBINE_ALL                 ( RENAME_ATTRIBUTES.out.rename.collect() )
		GENE_OVERLAP				( COMBINE_ALL.out.combined )
        COMPLETE_GENEMODEL                      ( GENE_OVERLAP.out.overlap_unfiltered, genome )       
        FORMAT_GFF                                      ( COMPLETE_GENEMODEL.out.start, genome, params.prefix )		
		LIST  						( RENAME_ATTRIBUTES.out.rename.collect() )
	}
    emit:
	FORMAT_GFF.out
	LIST.out
	psiclass_log
	stringtie2_log
}
