process LONGEST_ISOFORM {
    label 'process_medium'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(prefix)

    output: 
    path("*_filtered_longest_isoform.gff"), emit: filtered_gff
    path("*_filtered_longest_isoform.gtf"), emit: filtered_gtf
      
    """

    agat_sp_keep_longest_isoform.pl --gff ${gff} -o ${prefix}_filtered_longest_isoform.gff
    agat_convert_sp_gff2gtf.pl --gff ${prefix}_filtered_longest_isoform.gff -o ${prefix}_filtered_longest_isoform.gtf

    """
}
process F1_ISOFORM {
    label 'process_low'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    input:
    path(gtf)
    path(regressor)
    val(prefix)

    output: 
    path("*_filtered_primary_isoform.gtf"), emit: filtered_gtf
      
    """
    awk -F'[\t";]+' '\$3 == "transcript" {print \$10 "," \$14}' ${gtf} > gene_transcript.txt
    awk 'BEGIN {FS=","; OFS=","} NR==FNR {mapping[\$2]=\$1; next} FNR>1 {if (\$1 in mapping) print mapping[\$1], \$1, \$2}' gene_transcript.txt ${regressor} > mapped_f1.csv
    
    awk -F',' '{ key = \$1; if (\$3 > max[key]) { max[key] = \$3; value[key] = \$2; } }
    END { for (k in value) { print k "," value[k] "," max[k]; } }' mapped_f1.csv | awk -F',' '{ print \$2 }' > primary_transcript.txt

    awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /transcript_id "([^"]+)";/, m); if (m[1] in transcripts) print }' primary_transcript.txt ${gtf} > ${prefix}_filtered_primary_isoform.gtf

    """
}