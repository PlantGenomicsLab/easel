include { EASEL } from '../modules/log.nf'

workflow OUTPUT {

    take:
    genome
    rna_reads
    alignments
    psiclass
    stringtie2
    unfiltered
    filtered
    longest
	
    main:

    if (params.build_training_set == false){
        if (params.primary_isoform == true){
            EASEL    ( genome, rna_reads, alignments, psiclass, stringtie2, unfiltered, filtered, longest )  
        }
        else if (params.primary_isoform == false){
            EASEL    ( genome, rna_reads, alignments, psiclass, stringtie2, unfiltered, filtered, [] )

        }
    }
    else if (params.build_training_set == true){
        EASEL    ( genome, rna_reads, alignments, psiclass, stringtie2, unfiltered, [], [] )
    }
}