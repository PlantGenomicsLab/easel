process FORMAT_GENOME {
    label 'process_low'

    conda "bioconda::seqtk"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/seqtk:1.3--h7132678_4' :
        'quay.io/biocontainers/seqtk:1.3--h7132678_4' }"

    input:
    path(genome)

    output:
    path("*folded.fasta"), emit: unzipped
      
    """
    gname=\$(basename "${genome}" | cut -d. -f1)
    if [[ ${genome} == *.gz ]]; then
        real_zip=\$(readlink -f ${genome}) 
        gzip -d \$real_zip -c | seqtk seq -Cl60 | sed 's/;/_/g' > "\$gname"_folded.fasta
    else
        genome_filename=\$(basename "${genome}")
        if [ "\$genome_filename" == "\$gname"_folded.fasta ]; then
            echo "Genome filename matches "\$gname"_folded.fasta, please rename"
        else
            seqtk seq -Cl60 ${genome} | sed 's/;/_/g' > "\$gname"_folded.fasta
        fi
    fi

    """
}
process PERCENT_MASKED {
    label 'process_low'

    container 'plantgenomics/easel:perl'

    input:
    path(genome)
    val(bins)

    output:
    path("log_mask.txt"), emit: log_genome
    path("*.part-*"), emit: chromosomes
      
    """
    
masked_bases=\$(awk '!/^>/ { gsub("[^a-z]", ""); count += length } END { print count }' ${genome})
genome_length=\$(awk '!/^>/ { gsub("[^A-Za-z]", ""); count += length } END { print count }' ${genome})
masked_percentage=\$(awk 'BEGIN {printf "%.2f", ('\$masked_bases'/'\$genome_length')*100}')
high=\$(echo "25")
low=\$(echo "${params.percent_masked}")

if [[ "\$masked_percentage" > "\$low" && "\$masked_percentage" < "\$high" ]]
then
echo "##### Genome #####" >> log_mask.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_mask.txt
echo "WARNING: Less than 25% masked" >> log_mask.txt
elif [[ "\$masked_percentage" < "\$low" ]]
then
echo "######## Genome ########" >> log_mask.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_mask.txt
echo "ERROR: Less than 10% of ${genome} is masked" >> log_mask.txt
exit 1
else
echo "######## Genome ########" >> log_mask.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_mask.txt
fi

${projectDir}/bin/fasta-splitter.pl --n-parts ${bins} ${genome}
    """
}
process METRICS {

    publishDir "$params.outdir/metrics/busco",  mode: 'copy', pattern: "genome/*"
    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*.txt'

    label 'process_medium_cpu'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.7.0--pyhdfd78af_1' :
        'quay.io/biocontainers/busco:5.7.0--pyhdfd78af_1' }"

    input:
    path(genome)
    val(odb)
    path(genome_log)

    output:
    path("genome/*"), emit: busco
    path("log_genome.txt"), emit: log

    script:
   
    """
    busco -i ${genome} -l ${odb} -o genome -m Genome -r -c ${task.cpus}
    grep "The lineage" genome/*.txt > odb.txt
    grep "C:" genome/*.txt > busco.txt
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    
    sed -n '18,23p' genome/*.txt | awk 'BEGIN {FS="\t"}; {print \$3 ": " \$2}' | while IFS= read -r line; do
        echo "\$line" >> stats.txt
    done

    echo "BUSCO (\$odb): \$busco" > log_metrics.txt
    cat ${genome_log} stats.txt log_metrics.txt > log_genome.txt
    """  
}
process GENOME_LOG {

    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*.txt'
    label 'process_low'

    input:
    path(genome_log)

    output:
    path("log_genome.txt"), emit: log

    script:
   
    """
    cat ${genome_log} > log_genome.txt
    """  
}
