process COUNT_ATG {
    label 'process_low'
    container 'plantgenomics/easelv2.1:python'
    
    input:
    path(cds)
    path(matrix)
     
    output: 
    path("atg_counts.tracking"), emit: counts
    
    """
python ${projectDir}/bin/atg_count.py ${cds} atg_counts.txt
python ${projectDir}/bin/map_transcript.py ${matrix} atg_counts.txt ATG_Counts atg_counts.tracking

    """
}