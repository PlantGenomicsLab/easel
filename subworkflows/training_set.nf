include { START_SITE; F1; TRAINING_SET } from '../modules/create_training_set.nf'

workflow BUILD_TRAINING{

	take:
	genome
	reference
	prediction
	matrix

	main:
	START_SITE			( genome, reference, prediction )
	F1					( prediction, reference )
	TRAINING_SET 		( F1.out.F1, START_SITE.out.target, matrix, params.prefix )
}