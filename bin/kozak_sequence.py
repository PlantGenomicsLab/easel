#!/usr/bin/env python

#########################################################
#                                                       #
# kozak_sequence.py - identify kozak sequence for each  #
# transcript and report -3 and +4 position              #
#                                                       #
#                                                       #
# Developed by Cynthia Webster (2024)                   #
# Plant Computational Genomics Lab                      #
# University of Connecticut                             #
#                                                       #
# This script is under the MIT License                  #
#                                                       #
#########################################################


import sys
from Bio import SeqIO
from Bio.Seq import Seq

def base_to_num(base):
    """Convert nucleotide base to corresponding number."""
    mapping = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    return mapping.get(base, -1)  # Return -1 if base is unknown

def extract_kozak_sequences(fasta_file, gff_file, output_file):
    # Parse the FASTA file to retrieve sequences for each chromosome
    genome = SeqIO.to_dict(SeqIO.parse(fasta_file, "fasta"))

    # Open the GFF file to read start codon positions
    start_codons = {}
    with open(gff_file, "r") as gff:
        for line in gff:
            if line.startswith("#"):
                continue
            fields = line.strip().split("\t")
            if fields[2] == "start_codon":  # We're only interested in start codons
                chrom = fields[0]
                start = int(fields[3])  # Start position of the start codon (1-based)
                strand = fields[6]       # Strand info (+ or -)
                
                # Extract transcript ID from the attributes (last column)
                attributes = fields[8]
                
                # Check for Parent attribute to ensure it's associated with a transcript
                if "Parent=" in attributes:
                    parent_id = attributes.split("Parent=")[1].split(";")[0]  # Get the Parent ID
                    
                    # Store the start codon information using the parent transcript ID
                    start_codons[parent_id] = (chrom, start, strand)

    # Open the output file to write the results
    with open(output_file, "w") as output:
        output.write("Transcript\tKozak_Sequence\tNeg_Three\tPos_Four\tKozak_Numeric\tNeg_Three_Numeric\tPos_Four_Numeric\n")
        for transcript_id, (chrom, start, strand) in start_codons.items():
            # Extract the start codon sequence (ATG)
            if strand == "+":
                start_pos = start - 1  # Adjust for 0-based indexing
                end_pos = start_pos + 3  # ATG is 3 bases long
                kozak_start = max(start_pos - 6, 0)  # 3 bases before the start codon
                kozak_end = end_pos + 1  # Start codon is included
                neg_three = start_pos - 3  # -3 position (3 bases before start codon)
                pos_four = start_pos + 3  # +4 position (4 bases after start codon)
            elif strand == "-":
                start_pos = start - 1  # Adjust for the start of the start codon
                end_pos = start + 2  # Position of the start codon (last base, 1-based)
                kozak_start = start_pos - 1 # Start codon is included
                kozak_end = min(start_pos + 9, len(genome[chrom].seq))  
                neg_three = start_pos + 5  # +6 position in the reverse complement
                pos_four = start_pos - 1  # +3 position in the reverse complement
            else:
                continue
            
            # Retrieve the sequence from the genome
            if strand == "+":
                kozak_sequence = genome[chrom].seq[kozak_start:kozak_end].upper()
                neg_three_base = genome[chrom].seq[neg_three:neg_three + 1].upper()  # +1 to get one base
                pos_four_base = genome[chrom].seq[pos_four:pos_four + 1].upper()  # +1 to get one base
            else:  # strand == "-"
                kozak_sequence = genome[chrom].seq[kozak_start:kozak_end].reverse_complement().upper()
                neg_three_base = genome[chrom].seq[neg_three:neg_three + 1].reverse_complement().upper()  # +1 to get one base
                pos_four_base = genome[chrom].seq[pos_four:pos_four + 1].reverse_complement().upper()  # +1 to get one base

            # Convert sequences to numerical representations
            kozak_numeric = [base_to_num(base) for base in kozak_sequence]
            neg_three_numeric = base_to_num(neg_three_base)
            pos_four_numeric = base_to_num(pos_four_base)

            # Write to the output file, including the numeric representations
            output.write(f"{transcript_id}\t{kozak_sequence}\t{neg_three_base}\t{pos_four_base}\t" +
                         f"{' '.join(map(str, kozak_numeric))}\t{neg_three_numeric}\t{pos_four_numeric}\n")

def main():
    if len(sys.argv) != 4:
        print("Usage: python kozak_sequence.py <fasta_file> <gff_file> <output_file>")
        sys.exit(1)

    fasta_file = sys.argv[1]
    gff_file = sys.argv[2]
    output_file = sys.argv[3]

    extract_kozak_sequences(fasta_file, gff_file, output_file)

if __name__ == "__main__":
    main()

