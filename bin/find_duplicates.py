#!/usr/bin/env python

########################################################
#                                                      #
# find_duplicates.py - identify duplicate isoforms     #
# using random forest feature matrix                   #
#                                                      #
# Developed by Cynthia Webster (2024)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import sys
import re

def process_features(features_file):
    gene_transcripts = {}

    # Process each line of the file
    with open(features_file, 'r') as f:
        for line in f:
            parts = re.split(r'\s+', line.strip())  # Split by one or more whitespace characters
            if len(parts) < 8:  # Ensure there are enough columns for processing
                continue
            
            gene = parts[0]  # Gene is in the first column (index 0)
            # Combine all columns from index 7 onward into a key
            key = '\t'.join(parts[6:])  # Combine columns from index 6 onward with a tab delimiter

            combined_key = f"{gene}\t{key}"  # Combine gene and the key
            
            # Append the transcript (column 2) to the existing or new entry
            if combined_key in gene_transcripts:
                if parts[1] not in gene_transcripts[combined_key]:  # Avoid duplicate transcripts
                    gene_transcripts[combined_key].append(parts[1])
            else:
                gene_transcripts[combined_key] = [parts[1]]

    # Write results to the output file
    with open('coding_dups.tsv', 'w') as out:
        for combined_key, transcripts in gene_transcripts.items():
            if len(transcripts) > 1:  # Only output if there are multiple transcripts
                gene = combined_key.split('\t')[0]  # Extract the gene from the combined key
                out.write(f"{gene}\t{','.join(transcripts)}\n")  # Only output gene and transcripts

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python find_duplicates.py <features_file>")
        sys.exit(1)

    features_file = sys.argv[1]
    process_features(features_file)

