process UNFILTERED_GTF {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: '*.gtf' 
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(species)
     
    output: 
    path("*unfiltered.gtf"), emit: unfiltered_gtf
      
    """
   agat_convert_sp_gff2gtf.pl --gff ${gff} -o convert.gtf
   grep -v '^#' convert.gtf > ${species}_unfiltered.gtf
    """
    
}


