process FEATURE_COUNTS {
    label 'process_medium'
    tag {id}

    conda "bioconda::subread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/subread:2.0.6--he4a0461_0' :
        'quay.io/biocontainers/subread:2.0.6--he4a0461_0' }"

    input:
    tuple val(id), path(bam)
    path(unfiltered_gtf)

    output:
    path "*.counts", emit: counts

    script:
    """
    if [[ "${bam}" == sr_* ]]; then
        featureCounts -T ${task.cpus} -p --countReadPairs -t exon -g transcript_id -a ${unfiltered_gtf} -o ${id}.txt ${bam}
    elif [[ "${bam}" == lr_* ]]; then
        featureCounts -T ${task.cpus} -t exon -L -g transcript_id -a ${unfiltered_gtf} -o ${id}.txt ${bam}
    fi

awk -F ' ' '
NR > 1 && !/^#/ {
    gene_id = \$1;
    gene_length = \$6;
    counts = \$7;

    if (counts + 0 > 0 && gene_length + 0 > 0) {
        rpk = counts / (gene_length / 1000);
        total_rpk += rpk;
        data[gene_id] = rpk;
    } else {
        data[gene_id] = 0;
    }
}
END {
    if (total_rpk > 0) {
        for (gene_id in data) {
            tpm = (data[gene_id] / total_rpk) * 1000000;
            print gene_id "\t" tpm;
        }
    } else {
        for (gene_id in data) {
            print gene_id "\t" 0;  
        }
        print "Warning: Total RPK is zero. No valid data found.";
    }
}' "${id}.txt" > "${id}.counts"

    """
}
process GENE_EXPRESSION {
    label 'process_low'
    container 'plantgenomics/easelv2.1:python'

    input:
    path(counts)
    path(matrix)

    output:
    path "expression.tracking", emit: expression

    script:
    """
mkdir read_counts
mv ${counts} read_counts

awk 'NF > 1 { a[\$1] += \$2 } 
END { 
    c = 1;
    for (i in a) { 
        log_value = (a[i] > 0) ? log(a[i] + c) / log(2) : 0; 
        print i "\t" log_value 
    } 
}' read_counts/*.counts > merged.counts

sed -i \$'1 i\\\nTranscript\tHit\' merged.counts
python ${projectDir}/bin/map_transcript.py ${matrix} merged.counts Expression expression.tracking

    """
}
