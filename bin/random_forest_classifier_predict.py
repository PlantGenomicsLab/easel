#!/usr/bin/env python

########################################################
#                                                      #
# random_forest_classifier_predict.py - TIS prediction #
# with random forest classifier and feature matrix     #
#                                                      #
# Developed by Cynthia Webster (2022)                  #
# Updated - 2024                                       #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
import numpy as np
import sys
from sklearn.metrics import mean_squared_error, accuracy_score, r2_score

def clean_kosak_pos(df, column_name='Kozak_Pos_4', valid_nucleotides={'A', 'C', 'G', 'T'}):
    df[column_name] = df[column_name].apply(lambda x: x if x in valid_nucleotides else np.nan)
    return df

def clean_nan(df):
    df = df.replace('NaN', np.nan)
    return df

def get_feature_importances(cols, importances):
    feats = {}
    for feature, importance in zip(cols, importances):
        feats[feature] = importance

    importances = pd.DataFrame.from_dict(feats, orient='index').rename(columns={0: 'Gini-importance'})
    return importances.sort_values(by='Gini-importance', ascending=False)

def drop_features(df, stringency):
    if stringency == 0:
        return df
    elif stringency == 1:
        df = df.drop([
            "EggNOG_QueryStart", "EggNOG_QueryEnd", "EggNOG_SubjectStart", 
            "EggNOG_SubjectEnd", "OrthoDB_QueryStart", "OrthoDB_QueryEnd", 
            "OrthoDB_SubjectStart", "OrthoDB_SubjectEnd", "OrthoDB_Length", 
            "OrthoDB_Mismatch"
        ], axis=1)
    elif stringency == 2:
        df = df.drop([
            "EggNOG_Bitscore", "EggNOG_Evalue", "EggNOG_SimilarityScore", 
            "EggNOG_QueryCoverage", "EggNOG_SubjectCoverage", "OrthoDB_Bitscore", "OrthoDB_Evalue", 
            "OrthoDB_SimilarityScore"
        ], axis=1)
    elif stringency == 3:
        df = df.drop([
            "EggNOG_QueryStart", "EggNOG_QueryEnd", "EggNOG_SubjectStart", 
            "EggNOG_SubjectEnd", "OrthoDB_QueryStart", "OrthoDB_QueryEnd", 
            "OrthoDB_SubjectStart", "EggNOG_SubjectCoverage", "OrthoDB_SubjectEnd", "OrthoDB_Length", 
            "OrthoDB_Mismatch", "EggNOG_Bitscore", "EggNOG_Evalue", 
            "EggNOG_SimilarityScore", "EggNOG_QueryCoverage", "OrthoDB_Bitscore", 
            "OrthoDB_Evalue", "OrthoDB_SimilarityScore"
        ], axis=1)
    else:
        raise ValueError("Invalid stringency level. Please provide a value between 0 and 3.")
    return df

if len(sys.argv) != 5:
    print("Usage: python random_forest_classifier_predict.py <training_set> <testing_set> <output> <stringency>")
    sys.exit(1)

training_set = sys.argv[1]
testing_set = sys.argv[2]
output = sys.argv[3]
stringency = int(sys.argv[4])

# Read the training dataset
data = pd.read_csv(training_set, delimiter="\t", low_memory=False)
data = clean_kosak_pos(data)

# One-hot encode 'Kozak_Pos_4' column
encoded_columns = pd.get_dummies(data['Kozak_Pos_4'], prefix='Kozak_Pos_4')
data = pd.concat([data, encoded_columns], axis=1)
data = data.drop('Kozak_Pos_4', axis=1)

label = data['Target']
data = data.drop(['F1', 'Target', 'Gene', 'Transcript'], axis=1)
data = drop_features(data, stringency)
data = clean_nan(data)

X_train, X_test, y_train, y_test = train_test_split(data, label, test_size=0.2, random_state=42)
rf = RandomForestClassifier(n_estimators=200)
rf.fit(X_train, y_train)

# Training set evaluation
rfc_pred_train = rf.predict(X_train)
print('Training Set Evaluation RMSE=>', np.sqrt(mean_squared_error(y_train, rfc_pred_train)))
print('Training Set Evaluation r2=>', r2_score(y_train, rfc_pred_train))

# Test set evaluation
rfc_pred_test = rf.predict(X_test)
print('Testing Set Evaluation RMSE=>', np.sqrt(mean_squared_error(y_test, rfc_pred_test)))
print('Testing Set Evaluation r2=>', r2_score(y_test, rfc_pred_test))
print('Testing Set Evaluation accuracy =>', accuracy_score(y_test, rfc_pred_test))

# Feature importance
importances = rf.feature_importances_
importance_df = get_feature_importances(data.columns, importances)

print("\nImportant Features:")
for feature, importance in importance_df.iterrows():
    print(f"{feature}: {importance['Gini-importance']}")

# Train on entire dataset and predict on new data
rf_all = RandomForestClassifier(n_estimators=200)
rf_all.fit(data, label)

# Predict on new data
new_data = pd.read_csv(testing_set, delimiter=r"\s+", low_memory=False)

# Drop unnecessary columns from new data (same as for training)
new_order_numbers = new_data['Transcript']
new_data = new_data.drop(['Gene', 'Transcript'], axis=1)

# Clean 'Kozak_Pos_4' in the new data (same as for training)
new_data = clean_kosak_pos(new_data)
encoded_columns = pd.get_dummies(new_data['Kozak_Pos_4'], prefix='Kozak_Pos_4')
new_data = pd.concat([new_data, encoded_columns], axis=1)
new_data = new_data.drop('Kozak_Pos_4', axis=1)
new_data = drop_features(new_data, stringency)
new_data = clean_nan(new_data)

rf_pred = rf_all.predict(new_data)
# Cast predictions to integers (1 or 0)
rf_pred_int = rf_pred.astype(int)
res = pd.DataFrame({'Transcript': new_order_numbers, 'Prediction': rf_pred_int})
print("Pass (1):", res['Prediction'].sum())
print("Fail (0):", len(res) - res['Prediction'].sum())
res.to_csv(output, index=False)
