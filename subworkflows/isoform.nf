include { LONGEST_ISOFORM } from '../modules/primary_gene.nf'

workflow PRIMARY_GENE {

    take:
    filtered
    
    main:

    if(params.primary_isoform == true ){
    LONGEST_ISOFORM         ( filtered, params.prefix )
    longest_isoform = LONGEST_ISOFORM.out.filtered_gff
    }

    else if(params.primary_isoform == false ){
    longest_isoform = Channel.empty()
    }
    
    emit:
    longest_isoform
}
