process START_SITE {
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
    'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
    'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    path(genome)
    path(reference)
    path(prediction)

    output:
    path("start_site.txt"), emit: target

    """
agat_sp_add_start_and_stop.pl --gff ${reference} --fasta ${genome} --out reference.gff

awk '\$3 == "start_codon" {
    split(\$9, attributes, ";"); 
    for (i in attributes) {
        if (attributes[i] ~ /^Parent=/) {
            parent_id = substr(attributes[i], index(attributes[i], "=") + 1);
            break;
        }
    }
    OFS="\t"; 
    print \$1, \$4, \$7, parent_id
}' ${prediction} > prediction_start_sites.txt

awk '\$3 == "start_codon" {
    split(\$9, attributes, ";"); 
    for (i in attributes) {
        if (attributes[i] ~ /^Parent=/) {
            parent_id = substr(attributes[i], index(attributes[i], "=") + 1);
            break;
        }
    }
    OFS="\t"; 
    print \$1, \$4, \$7, parent_id
}' reference.gff > reference_start_sites.txt

awk 'NR==FNR{A[\$1,\$2,\$3]=\$4; next}(\$1,\$2,\$3) in A{print \$0, A[\$1,\$2,\$3]}' reference_start_sites.txt prediction_start_sites.txt | awk '{print \$4}' > target.txt
awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' target.txt > start_site.txt
sed -i \$'1 i\\\nTranscript\tHit' start_site.txt


    """
}

process F1 {
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"

    input:
    path(prediction)
    path(reference)
     
    output: 
    path("F1.txt"), emit: F1
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o reference_prediction
awk 'NR > 1 { print \$4, \$10 }' OFS="\t" reference_prediction.tmap > F1.txt
sed -i \$'1 i\\\nTranscript\tHit' F1.txt

    """
}
process TRAINING_SET {
    publishDir "$params.outdir/training_set",  mode: 'copy'
    container 'plantgenomics/easelv2.1:python'

    input:
    path(f1)
    path(start)
    path(matrix)
    val(prefix)
     
    output: 
    path("${prefix}.tracking"), emit: model
      
    """
awk '{\$1=\$1; print}' OFS='\t' ${matrix} > matrix
python ${projectDir}/bin/map_transcript.py matrix ${start} Target feature.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
    if (\$40 == "") { 
        \$40 = 0 
    } else { 
        \$40 = int(\$40) 
    } 
} 1' feature.tracking > target.tracking

python ${projectDir}/bin/map_transcript.py matrix ${f1} F1 feature.tracking
awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$40 = sprintf("%d", \$40) }; 1' OFS="\t" feature.tracking > F1.tracking

awk -F'\t' '{print \$40}' target.tracking > target.txt
paste F1.tracking target.txt > ${prefix}.tracking
    """
}
