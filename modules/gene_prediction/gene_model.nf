process PSICLASS {
    publishDir "$params.outdir/04_assembly/psiclass/gtf",  mode: 'copy', pattern: "*.gtf"
    label 'process_medium'

    //container "sagnikbanerjee15/psiclass:1.0.3"
    container "plantgenomics/psiclass:1.0.3-fork"
     
    input:
    path(bam)
    val(args)
     
    output: 
    path("*gtf"), emit: all_gtf
    path("psiclass.gtf"), emit: gtf
      
    """
    bam=\$(echo '${bam}' | sed -e "s/ /,/g") 
    psiclass -b \$bam -p ${task.cpus} ${args}
    mv psiclass_vote.gtf psiclass.gtf
    """
}

process STRINGTIE2 {
        publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy'   
        label 'process_medium' 
        tag { id }

    conda "bioconda::stringtie=2.2.3"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.3--h43eeafb_0' :
        'quay.io/biocontainers/stringtie:2.2.3--h43eeafb_0' }"

        input:
        tuple val(id), path(bam)
        val(gap)
        val(args)

        output:
        path("*.gtf"), emit: stringtie2_gtf 

        script:
        """ 
        if [[ "${bam}" == sr_* ]]; then
            stringtie ${bam} -p ${task.cpus} -g ${gap} ${args} -o ${id}.gtf
        elif [[ "${bam}" == lr_* ]]; then
            stringtie -L ${bam} -p ${task.cpus} -g ${gap} ${args} -o ${id}.gtf
        fi
        """
}

process MERGE_STRINGTIE2 {
    publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy', pattern: '*gtf'

    label 'process_medium' 

    conda "bioconda::stringtie=2.2.3"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.3--h43eeafb_0' :
        'quay.io/biocontainers/stringtie:2.2.3--h43eeafb_0' }"

        input:
    file(gtf)
    val(args)
    
        output:
    path("stringtie2.gtf"), emit: merged_gtf

        script:
        """   
    stringtie --merge -p ${task.cpus} ${args} -o stringtie2.gtf *.gtf
        """
}
process FASTA {
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy', pattern: '*fa'    
    tag { id }

        
    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"
    label 'process_low' 

        input:
    path(genome)
    tuple val(id), path(gtf)
    
    
        output:
    tuple val(id), path("${id}_transcripts.fa"), emit: fasta
    tuple val(id), path("${id}_transcripts.gff3"), emit: gff3  

        script:
        """   
    ${projectDir}/bin/gtf_genome_to_cdna_fasta.pl ${gtf} ${genome} > ${id}_transcripts.fa 
    ${projectDir}/bin/gtf_to_alignment_gff3.pl ${gtf} > ${id}_transcripts.gff3
        """
}
process ORF {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium'
    tag { id }
    cache 'lenient'

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(transcriptome) 
     
    output:
    tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/longest_orfs.pep"), emit: longOrfsPepFiles
    tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/"), emit: LongOrfsDirFiles 
    //tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/__checkpoints_longorfs/"), emit: LongOrfsCheckpointsFiles
      
    //rm -r *.transdecoder_dir/__checkpoints_TDpredict/*
    script:
    """
    TransDecoder.LongOrfs -t ${transcriptome}
    rm -r *.transdecoder_dir/__checkpoints_longorfs
    """
}

process EGGNOG {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium' 
    tag { id }
    cache 'lenient'

    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.12--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.12--pyhdfd78af_0' }"

    input:
    tuple val(id), path(protein)
    path(db)  
    
    output:
    tuple val(id), path("${id}_eggnog.blastp.emapper.hits"), emit: eggnogBlastp 
   
    script:
    """
    emapper.py -i ${protein} --dmnd_db ${db} --no_annot -o ${id}_eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process PREDICT {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium'
    tag { id }
    cache 'lenient'
    
    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(transcriptomePredict), path(eggnogBlastp), path(directory)
     
    output:
    tuple val(id), path("${id}_transcripts.fa.transdecoder.pep"), emit: transdecoderPEP
    tuple val(id), path("${id}_transcripts.fa.transdecoder.bed"), emit: transdecoderBED
    tuple val(id), path("${id}_transcripts.fa.transdecoder.cds"), emit: transdecoderCDS
    tuple val(id), path("${id}_transcripts.fa.transdecoder.gff3"), emit: transdecoderGFF3

    script:
    """
    TransDecoder.Predict -t ${transcriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}
    rm -r *.transdecoder_dir/__checkpoints_TDpredict
    """
}
process GENEMODEL {
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(orf), path(gff3), path(transcript)
     
    output:
    tuple val(id), path("${id}.gff3"), emit: genomeGFF3
    path("${id}.gff3"), emit: model
    path("psiclass.gff3"), emit: psiclass_model, optional: true
    path("stringtie2.gff3"), emit: stringtie2_model, optional: true

    script:
    """
    ${projectDir}/bin/PerlLib/*.pm .
      ${projectDir}/bin/cdna_alignment_orf_to_genome_orf.pl \\
        ${orf} \\
        ${gff3} \\
        ${transcript} > ${id}.gff3
    """
}
process CLUSTER {
    label 'process_medium'    
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy'
    tag { id }
    
    conda "bioconda::vsearch"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/vsearch:2.28.1--h6a68c12_0' :
        'quay.io/biocontainers/vsearch:2.28.1--h6a68c12_0' }"

    input:
    tuple val(id), path(cds)
    val(clust_id)

    output:
    tuple val(id), path("centroids.cds"), emit: centroids
    path "centroids.uc", emit: uc
 
    script:
    """
    vsearch --notrunclabels --cluster_fast ${cds} --centroids centroids.cds --uc centroids.uc --id ${clust_id} --threads ${task.cpus}
    """

}
 
process GET_COMPLETE {
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy', pattern: '*.cds'
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy', pattern: '*.txt'
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy', pattern: '*.gff3'
    label 'process_low'  
    tag { id }

    conda "bioconda::samtools=1.17"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.20--h50ea8bc_0' :
        'quay.io/biocontainers/samtools:1.20--h50ea8bc_0' }"


    input:
    tuple val(id), path(centroids), path(gff3)

    output:
    tuple val(id), path("${id}.completeORF.cds"), emit: completeORF
    tuple val(id), path("model.${id}.gff3"), emit: GeneModel
    path("id.txt"), emit: completeOutput
    
    script:
    """
    grep  "type:complete" ${centroids} | awk '{print \$1}' > id.txt 
sed -i -e 's/>//g' id.txt
while read line;
    do samtools faidx ${centroids} \$line >> ${id}.completeORF.cds;
done < id.txt
grep -Fwf id.txt ${gff3} | sed 's/\tmRNA\t/\ttranscript\t/g' > model.${id}.gff3
    """
}
process BUSCO {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy'

    tag {id}
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.7.0--pyhdfd78af_1' :
        'quay.io/biocontainers/busco:5.7.0--pyhdfd78af_1' }"

    input:
    tuple val(id), path(protein)
    val(odb)

    output:
    tuple val(id), path("${id}"), emit: busco
    tuple val(id), path("*/*.txt"), emit: busco_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${id} -m Protein -c ${task.cpus}
    """
}

process AGAT {
    publishDir "$params.outdir/metrics/agat",  mode: 'copy'
    label 'process_low'
    tag {id}

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(gff)
    val(species)

    output:
    tuple val(id), path("*.txt"), emit: stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${id}.txt
    """
}

process LOG {
    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*txt'
    tag {id}

    input:
    tuple val(id), path(busco), path(agat)

    output:
    path("log_psiclass.txt"), emit: psiclass, optional: true
    path("log_stringtie2.txt"), emit: stringtie2, optional: true


    """
        grep -m 1 "Number of gene" ${agat} > genes.txt
        grep -m 1 "Number of mrna" ${agat} > transcripts.txt

        grep "The lineage" ${busco} > odb.txt
        grep "C:" ${busco} > busco.txt

        odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
        busco=\$(sed -e 's/[ \t]*//' busco.txt)
        genes=\$(grep -o '[0-9]\\+' genes.txt)
        transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)

    # Check if "Number of single exon gene" exists before creating mono.txt
        if grep -q "Number of single exon gene" ${agat}; then
            grep -m 1 "Number of single exon gene" ${agat} > mono.txt
            mono=\$(grep -o '[0-9]\\+' mono.txt)
            multi=\$((\$genes - \$mono))
            mono_multi=\$(echo "scale=3 ; \$mono / \$multi" | bc)
        else
            mono_multi="0"  
            fi

        if [[ "${id}" == *stringtie2* ]]; then
            echo ""
            echo "##### StringTie2 #####" >> log_${id}.txt
        elif [[ "${id}" == *psiclass* ]]; then
	    echo ""
            echo "##### PsiCLASS #####" >> log_${id}.txt
        fi

        echo "Total number of genes: \$genes" >> log_${id}.txt
        echo "Total number of transcripts: \$transcripts" >> log_${id}.txt
        echo "Mono-exonic/multi-exonic ratio: \$mono_multi " >> log_${id}.txt
        echo "BUSCO (\$odb): \$busco" >> log_${id}.txt
    """
}
