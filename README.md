# EASEL (Efficient, Accurate, Scalable Eukaryotic modeLs), a tool for improvement of eukaryotic genome annotation

[![Nextflow](https://img.shields.io/badge/nextflow%20DSL2-%E2%89%A522.10.1-23aa62.svg)](https://www.nextflow.io/)
[![run with docker](https://img.shields.io/badge/run%20with-docker-0db7ed?labelColor=000000&logo=docker)](https://www.docker.com/)
[![run with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)

[TOC]

### Join Slack
Need help running EASEL or find a bug? Message us on [`Slack`](https://easel-annotation.slack.com)!

## What is EASEL?
Gene annotation programs struggle with predicting less common gene structures (e.g. long introns or micro-exons), finding the preferred TIS location, and distinguishing pseudogenes. EASEL (Efficient, Accurate, Scalable Eukaryotic modeLs), is an integrated genome annotation tool that leverages machine learning, RNA folding, and functional annotations to enhance gene prediction accuracy.

EASEL utilizes AUGUSTUS with parameters optimized for prediction of gene models incorporating extrinsic evidence from transcripts and protein alignments. In specific, EASEL aligns high throughput short read data (RNA-Seq) and assembles putative transcripts via Stringtie2 and PsiCLASS. Frames are subsequently predicted through TransDecoder utilizing a gene family database (EggNOG) for refinement. Transcriptome hints from frame-selected sequences and protein hints from OrthoDB alignments are independently used to train AUGUSTUS, and the resulting predictions are combined into a single gene set (AGAT). Implicated gene structures are further refined through machine learning based on a set of primary and secondary sequence features (e.g. RNA folding). Annotations are combined and filtered from a feature matrix of output and scored to generate the highest supported annotation.

EASEL aims to facilitate the complex process of genome annotation through Nextflow in a pipeline that can be easily deployed on a variety of HPC systems.  EASEL also leverages existing and well-supported open-source packages and avoids re-inventing the wheel for providing evidence to AUGUSTUS.  The integrated process includes benchmarking, integration of external resources, functional annotation, descriptive logs, and standardized outputs.

This annotation pipeline was constructed using [Nextflow](https://www.nextflow.io), a workflow tool to run tasks across multiple compute infrastructures in a very portable manner. It uses Docker/Singularity containers making installation trivial and results highly reproducible. The [Nextflow DSL2](https://www.nextflow.io/docs/latest/dsl2.html) implementation of this pipeline uses one container per process which makes it much easier to maintain and update software dependencies.

BGA 2024 Workshop: https://www.youtube.com/watch?v=01ZCy5f72F8

![easel](docs/EASEL.png)

## Installing Dependencies
The following tools are required to run EASEL:
1. [`Nextflow`](https://www.nextflow.io/docs/latest/getstarted.html#installation) (`>=22.10.1`)

    > Make sure that Java v8+ is installed: `java -version`

    > Install Nextflow: `curl -fsSL get.nextflow.io | bash`

2. [`Docker`](https://docs.docker.com/engine/installation/), [`Singularity`](https://www.sylabs.io/guides/3.0/user-guide/),[`Podman`](https://podman.io/), [`Shifter`](https://nersc.gitlab.io/development/shifter/how-to-use/) **or** [`Charliecloud`](https://hpc.github.io/charliecloud/) for full pipeline reproducibility; [`Conda`](https://conda.io/miniconda.html) is *not* supported, see [docs](https://nf-co.re/usage/configuration#basic-configuration-profiles))

    If you are using `singularity` then the pipeline will auto-detect this and attempt to download the Singularity images directly as opposed to performing a conversion from Docker images. If you are persistently observing issues downloading Singularity images directly due to timeout or network issues then you can pre-download the containers with `scripts/pull_containers.sh`. Once downloaded, you must set the `singularity.cacheDir`](https://www.nextflow.io/docs/latest/singularity.html?#singularity-docker-hub) parameter with `--singularity_cache_dir` to the `singularity` directory produced. It is highly recommended to be able to store and re-use the images from a central location for future pipeline runs. 

3. [`GNU packages`](https://www.gnu.org/software/software.html)
EASEL requires a Linux-system with `bash`, please ensure the following packages are installed:

>- sed
>- wget
>- gzip
>- gawk

## Running EASEL
#### Executor 
By default EASEL will run the `local` executor. In this mode, it is recommended that you specify the maximum memory (`--max_memory`) and cpu (`--max_cpus`). Please check ['nf-core/configs'](https://github.com/nf-core/configs#documentation) to see if a custom config file to run nf-core pipelines already exists for your Institute. If so, you can simply use `-profile <institute>` in your command. This will enable either docker or singularity and set the appropriate execution settings for your local compute environment.

### Submitting a Script

> Make sure to [test your setup](https://nf-co.re/docs/usage/introduction#how-to-run-a-pipeline) with 
> `nextflow run -hub gitlab PlantGenomicsLab/easel -profile test,<singularity/docker> --outdir <OUTDIR>` 
> before running the workflow on actual data.
> **Note:** results of your test set will look _terrible_, but it's by design. 
> The small datasets and low filtering cut-offs speed up the process!  

#### Method 1: List required input flags directly in script
   ```
   nextflow run -hub gitlab PlantGenomicsLab/easel -profile <docker/singularity/podman/shifter/charliecloud/institution> \
       --genome "/home/genome/arabidopsis.fa" \
       --sra "/home/sra/sra_list.txt" \
       --busco_lineage "embryophyta" \
       --order "Brassicales" \
       --training_set "plant"
   ```

#### Method 2: Provide desired inputs in a yaml file
   ```
   nextflow run -hub gitlab PlantGenomicsLab/easel -profile <docker/singularity/podman/shifter/charliecloud/institution> \
       -params-file params.yaml
   ```
**params.yaml:**
```
genome            : "/home/genome/arabidopsis.fa"
sra               : "/home/sra/sra_list.txt"
busco_lineage     : "embryophyta"
order             : "Brassicales"
training_set      : "plant"
 ```
> string and path should be in "quotes", while number, integer and bool should not 

### Setting Paramaters
#### Mandatory
The following parameters are **required** to run EASEL, it will fail otherwise.
* RNA libraries: `sra`, `user_reads`, `bam` **and/or** `bam_long` 
* Protein evidence: `order` **or** `user_protein`

| PARAM | DESCRIPTION | QUALIFIER | EXAMPLE | IMPORTANT NOTES |
|--|---|---|---|---|
|genome | Softmasked genome in FASTA format (zipped/unzipped) | path | /home/genome/arabidopsis.fa | If your genome is not softmasked, consider setting `--repeat_masking` to *true*. |
|training_set | Clade specific training set for random forest feature filtering algorithm (options: plant, invertebrate, vertebrate, user) | string | plant | The training set is not required when `--build_training_set` is set to *true*. When `--training_set user` is invoked, you must provide a path to `--training_set_path`.
|busco_lineage | Relevant BUSCO protein database | string | embryophyta | Copy and paste a relevant lineage from the following [`file`](bin/busco_lineage.txt), spelling matters! |
|user_reads | User paired-end RNA FASTQ files (zipped/unzipped) | path | /home/reads/{1,2}.fastq.gz | **SHORT READS ONLY**. All libraries must be in the same directory with the following format: `*{1,2}.fastq`. Make sure prefixes *match* (i.e., **athaliana**_1.fastq & **athaliana**_2.fastq). |
|sra | SRA accession text file | path | /home/sra/sra_list.txt | **SHORT READS ONLY**. SRA input should have the following format: [`sra_list.txt`](assets/sra_list.txt). |
|bam | Short read user RNA alignments (bam) | path | /home/bam/*.bam | **SHORT READS ONLY**. Please provide HISAT2 alignments: *hisat2 --dta -q -x ${index}/hisat2 -1 ${read_1} -2 ${read_2} -S ${id}.sam | samtools view -b ${id}.sam | samtools sort -o sorted.bam* |
| bam_long | Long read user RNA alignments (bam) | path | /home/bam/*.bam | **LONG READS ONLY**. |
|order | Taxonomic rank such as order/clade to filter OrthoDB proteins | string | Brassicales | Copy and paste a relevant OrthoDB order from the following [`file`](bin/orthodb.txt), spelling matters! (Recommendation: Do an advanced search on https://www.orthodb.org/ with your species or genus) |
|user_protein| User input proteins | path | /path/to/orthodb.faa | This parameter can be used **in place** of order. It is recommended that you combine the OrthoDB database with your own set of proteins if invoked! |

#### Recommended

| PARAM | DESCRIPTION | QUALIFIER | EXAMPLE | IMPORTANT NOTES |
|--|---|---|---|--|
|outdir  |  Output directory name (default: easel) | string| easel | - |
|prefix   |                 AUGUSTUS training identifier and final predictions prefix (default: easel) | string | easel | Gene IDs will be populated with this prefix, it is recommended that you pick something informative (i.e., species name). |
|taxon | Any lineage such as species/kingdom/phylum used to filter ENTAP functional annotation (default: null) | string | arabidopsis | The taxon will encourage EnTAP to pick hits related to your species, as well as increase speed. |
|reference_db | DIAMOND database(s) for EnTAP functional annotation (default: complete RefSeq)| path | /home/database/refseq.dmnd | You may provide EASEL with a different supported `reference_db` for Diamond BLAST in EnTAP to perform functional annotation. You can download them with the provided [`scripts`](scripts). If you provide more than one database, you must put them in a single directory and use a wildcard (*.dmnd). |
| entap_db | path to EnTAP database (default: null) | path | /home/database/entap/bin/entap_database.bin | The database will be downloaded automatically into `/databases/entap/bin` if no path is provided. |
| eggnog_dir | path to EggNOG database directory (default: null) | path | /home/database/entap/databases | The database will be downloaded automatically into `/databases/entap` if no path is provided. |
| eggnog_db | path to EggNOG DIAMOND database (default: null) | path | /home/database/entap/bin/eggnog_proteins.dmnd | The database will be downloaded automatically into `/databases/entap/bin` if no path is provided. |
|contam  | Comma-separated list of contaminants to be filtered out by EnTAP (default: null) | string | bacteria,fungi | Contaminants will be reported in the functional annotation output. |
| pfam | Drop transcripts from final annotation without PFAM domain (default: false) | bool | false | It is encouraged to set this to true, we expect that high-quality predictions will have a PFAM domain. |
| pfam_db | path to Pfam-A HMM database (default: null) | path | /home/database/Pfam-A.hmm | This is only necessary if `--pfam true`, if no path is provided the latest [PFAM release](https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam37.1/) will be downloaded. |
|singularity_cache_dir | Output directory of singularity images | path | /home/singularity | If containers are [pre-downloaded](scripts/pull_containers.sh), it is important to set the path! | 
|primary_isoform | Output the longest isoform of filtered annotation (default: true) | bool | true | It is **not** recommended to set this to false, many downstream analyses require a primary isoform annotation (i.e., OrthoFinder). |
|max_memory |                Maximum memory allocated | string | 500.GB | Set in **local mode** |
|max_cpus    |              Maximum cpus allocated | integer | 64 | Set in **local mode** |
|max_time     |             Maximum time allocated | string  | 250.h | Set in **local mode** |

#### Optional
You may fine-tune EASEL with the following parameters:
| PARAM | DESCRIPTION | QUALIFIER | EXAMPLE | IMPORTANT NOTES |
|--|---|---|---|--|
| resume_filtering        | Resume EASEL from filtering stage (post-AUGUSTUS) (default: false) | bool | false | **ONLY AFTER AUGUSTUS HAS FINISHED.** Nextflow's `-resume` flag often causes the pipeline to backtrack (even though it shouldn't). Use this flag in conjunction with `-resume` to prevent AUGUSTUS from starting over. |
| rna_list                  | Path to StringTie2 and PsiCLASS GFF files used to resume filtering (default: launchDir/outdir/06_predictions/rna.txt) | path | home/easel/06_predictions/rna.txt | If AUGUSTUS completes successfully, you should not have to use this flag when `--resume_filtering` is set to true |
| augustus_list                  | Path to AUGUSTUS GFF files used to resume filtering (default: launchDir/outdir/06_predictions/augustus.txt) | path | home/easel/06_predictions/augustus.txt | If AUGUSTUS completes successfully, you should not have to use this flag when `--resume_filtering` is set to true |
|regressor                | Threshold for filtering random forest F1 prediction (default: 80) | integer| 80| If you find too many genes are being filtered out, you may consider lowering the `regressor` value. Likewise, if you capture too many genes, increase the value. Also see `--stringency`. |
| stringency      | (**0**) All features are included in random forest filtering (default); (**1**) Drop the columns related to start/end positions and length/mismatch of hits in the EggNOG and OrthoDB outputs; (**2**) Drop columns related to bitscore, e-value, similarity, and coverage metrics from both EggNOG and OrthoDB; (**3**) A combination of stringency levels 1 and 2: drops both the position-based columns and the score/metric-based columns | integer | 3 | Each training set has the highest accuracy with stringency set to level 0; however, this may not translate to species with limited genomic resources. If you notice a significant drop in BUSCO scores or gene counts, consider setting `--stringency 3`. This level prioritizes binary evidence (hit/no hit) from EggNOG and OrthoDB, minimizing reliance on BLAST-derived features. |
|keep_taxa | Keep certain taxa from the `--training_set` (default: null); **plant:** a_thaliana, p_patens, p_trichocarpa; **invertebrate:** a_aegypti, d_melanogaster, c_elegans; **vertebrate:** d_rerio, m_musculus, x_laevis | string (comma separated) | a_thaliana, p_patens| Make sure you filter using the appropriate species for your `--training_set`. Spelling matters! | 
|bins                     | Number of bins genome will be split into (default: 10) |integer | 10 | A higher value will increase the parallelization of AUGUSTUS, but it is limited by the number of contigs in your genome (i.e., if you have 7 chromosomes and you set bins to 10, it will default to 7). |
|parts                    | Number of bins true start sites will be split into (default: 5) |integer  | 5 | This stage can be time consuming. Similar to `--bins`, a higher value will increase parallelization for free energy feature generation. |
|percent_masked           | Filtering percentage of repeats masked cut-off (default: 10) | integer | 10| A cut-off of 0 is not allowed, see `--repeat_masking`. | 
|rate                     | Filtering mapping rate cut-off (default: 85) |integer  | 85 | Low mapping rates are an indication of low quality reads. This cut-off is generally sufficient for same-species RNA libraries. When aligning more diverged species, consider lowering this value.  | 
|total_reads              | Filtering read length cut-off (default: 15000000) |integer  | 15000000 | Using libraries with low total read counts is generally not desired. |
|mean_length              | Filtering mean length of reads cut-off (default: 70) |integer  | 70 | - |
|fastp_args               | fastp: additional arguments (default: '') | string | --qualified_quality_phred 15 | [FASTP manual](https://github.com/OpenGene/fastp) |
|cluster_id               | VSEARCH: identify threshold (default: 0.80) |number  | 0.80 | [VSEARCH manual](https://github.com/torognes/vsearch): Clustering will prevent redundant transcripts from being used to train AUGUSTUS. |
| psiclass_args | PsiCLASS: additional arguments (default: "") | string | --sa 0.7 | [PsiCLASS manual](https://github.com/splicebox/PsiCLASS) |
| stringtie2_args | StringTie2: additional arguments for standard StringTie2 run (default: "") | string | -m 150 | [StringTie2 manual](https://github.com/skovaka/stringtie2) |
| stringtie2_merge_args | StringTie2: additional arguments for merge command (default: "") | string | -m 25 | [StringTie2 manual](https://github.com/skovaka/stringtie2) |
|gap_tolerance            | StringTie2: gap tolerance (default: 0) |integer |0| The maximum gap allowed between read mappings. Avoid changing this parameter with --stringtie2_args. |
|hisat2_min_intronlen     | HISAT2: min inton length (default: 20) |integer  |20| - |
|hisat2_max_intronlen     | HISAT2: max inton length (default: 500000) |integer  |500000| See `--max_intron`. |
|hisat2_args              | HISAT2: additional arguments (default: '') | string | --maxins 500 | [HISAT2 manual](https://daehwankimlab.github.io/hisat2/)
| miniprothint | Select high quality miniprot alignments for hints evidence in AUGUSTUS (default: false) | boolean | false | [miniprothint manual](https://github.com/tomasbruna/miniprothint); if set to true you may see a drop in gene sensitivity/BUSCO completeness. Ideally, the random forest will handle possible noise introduced by the entire protein database set by `--order` or `--user_proteins`. | 
|miniprot_args           | miniprot: additional arguments (default: '') | string | -n 10 | We run miniprot with "-I" by default, this sets max intron size based on genome size. You can override this option by setting `miniprot_args: "-G NUM"` | [miniprot manual](https://github.com/lh3/miniprot)
| augustus | AUGUSTUS: path where repository is downloaded (default: ${projectDir}/bin) | path | /home/software | If AUGUSTUS is failing, consider changing this path to your home directory. |  
|test_count               | AUGUSTUS: number of genes to test (default: 250) |integer |250|- |
|train_count              | AUGUSTUS: number of genes to train (default: 1000) |integer  |1000| - |
|kfold                    | AUGUSTUS: k-fold cross validation of optimization  (default: 8)  | integer | 8 | - |
|rounds                   | AUGUSTUS: number of rounds for optimization (default: 5) | integer | 5 |- |
|optimize                 | AUGUSTUS: optimize the meta parameters (default: true) | bool | true | By default the `optimize` parameter is set to true to increase gene prediction accuracy. Although not recommended, you may turn off this option to decrease runtime.  |
|orthodb_tcoverage                | OrthoDB feature: t-coverage cutoff (default: 70) | integer | 70 | Lowering will reduce stringency of random forest filtering. |
| orthodb_qcoverage               | OrthoDB feature: q-coverage cutoff (default: 70) | integer | orthodb_tcoverage | Lowering will reduce stringency of random forest filtering.|
|entap_tcoverage                | EnTAP: t-coverage cutoff (default: 70) |integer  |70| Lowering will reduce stringency of Similarity Search step. | 
|entap_qcoverage                | EnTAP: q-coverage cutoff (default: 70) |integer  |70| Lowering will reduce stringency of Similarity Search step. |
|window                   | RNA folding window size, upstream_downstream (default: 400_200) |string  | 400_200 | Increasing the window size will significantly increase runtime. |
|taxid | Taxonomic ID of species you want to remove from OrthoDB database (default: null) | integer | 3702 | The `taxid` can only be set to one species, which you can find on [`NCBI`]( https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi). This feature is most relevant for benchmarking against model organisms. |
|reference| Reference GTF to calculate F1 metric (output in `metrics/mikado`) | path | /path/to/reference.gtf | This will allow you to compare your prediction to the reference. Must be set if `--build_training_set true` |
|utr | Keep UTR features in final annotation (default: false) | bool | true | UTRs are not well predicted by AUGUSTUS and can complicate the concatenation of PsiCLASS and StringTie2 gene models. | 
| long_intron | Increase maximum intron length across entire pipeline (default: false) | bool | false | This parameter can be adjusted with `--max_intron`. Most relevant for large genomes with long introns (i.e., conifers) | 
| max_intron | Max length of `--long_intron` (default: 3000000) | integer | 3000000 | Increasing this parameter may lead to OOM error from Miniprot. | 
| repeat_masking | Basic RepeatMasker/RepeatModeler pipeline to generate softmasked genome for structural annotation (default: false) | bool | false | This pipeline will not curate repeats, if you are interested in a high-quality repeat annotation consider independelty soft-masking your genome. |
| repeat_modeler_args | RepeatModeler: additional arguments (default: "") | string | --engine ncbi | [RepeatModeler manual](https://github.com/Dfam-consortium/RepeatModeler/blob/master/RepeatModeler)|
| repeat_masker_args | RepeatMasker: additional arguments (default: "") | string | --species cow | [RepeatMasker manual](https://www.repeatmasker.org/)|
| build_training_set | Generate a training set for random forest filtering with model species (default: false) | bool | false | If current training sets for random forest filtering do not reflect your species of interest well, you can build your own. A **model** `--reference` is required.|
| user | Path to generated training set (default: null) | path | /home/training_set/arabidopsis.tracking | Must set `--training_set` to user. |
| remove_dups | Find and remove suspect isoforms with duplicate features from random forest (default: true) | bool | true | This will help resolve overprediction issues. |
| busco_genome | Run BUSCO on genome assembly (default: true) | bool | true | - |


### Example 
The following script gives an example with _Arabidopsis thaliana_ as the species of interest:
>- [`Shell script`](nextflow.sh)
>- [`params.yaml`](params.yaml)
### Outputs
#### Structural Annotation
Unfiltered and filtered structural annotation files are populated in the `final_predictions` directory:

```
final_predictions
├── a_thaliana_filtered.cds
├── a_thaliana_filtered.gff
├── a_thaliana_filtered.gtf
├── a_thaliana_filtered.gff
├── a_thaliana_filtered.pep
├── a_thaliana_filtered_functional.gff
├── a_thaliana_filtered_longest_isoform.cds
├── a_thaliana_filtered_longest_isoform.gff
├── a_thaliana_filtered_longest_isoform.gtf
├── a_thaliana_filtered_longest_isoform.pep
├── a_thaliana_filtered_longest_isoform_functional.gff
├── a_thaliana_unfiltered.cds
├── a_thaliana_unfiltered.gff
├── a_thaliana_unfiltered.gtf
├── a_thaliana_unfiltered.pep
└── a_thaliana_unfiltered_functional.gff
```

* **.cds** - nucleotide coding sequences
* **.pep** - protein coding sequences
* **.gff** - general feature format
* **.gtf** - general transfer format

The `unfiltered` output contains all merged annotations from AUGUSTUS, as well as StringTie2 and PsiCLASS. The `filtered` output is the post-feature filtering via random forest prediction (including the longest isoform). Each annotation with `_functional.gff` contains biological information (9th column) derived from EnTAP in NCBI format. Filtered annotations are considered as the _final_ EASEL annotation. 

#### Functional Annotation
Proteins from `final_annotations` are used to functionally annotate the genome and provide biological information for each predicted transcript. EnTAP results, which are partitioned by filtered and unfiltered predictions, can be found in `08_functional_annotation`. 

```
08_functional_annotation/<filtered>/final_results
```
_final annotation:_`annotated.tsv`

The `SeqSeqrch_Description` column in the final annotation provides a similarity search hit, which is attached to `_functional.gff` in `final_predictions`:

_Example:_
| Query_Sequence | SeqSearch_Description |
| -------------- | ----------- |
|g6142.t1 | NP_178213.1 stress response NST1-like |
|g6136.t1 | XP_020890164.1 chaperone protein dnaJ 8|

#### Feature Filtering
The feature matrix can be found in `07_filtering`, it is called `feature.tracking`:

| Gene                | Transcript           | S1-AUG_OrthoDB_PsiCLASS | S2-AUG_OrthoDB_StringTie2 | S3-AUG_TD_PsiCLASS | S4-AUG_TD_StringTie2 | S5-Transcriptomes | Support | Exons | EggNOG_Support | EggNOG_Bitscore | EggNOG_SimilarityScore | EggNOG_Evalue | EggNOG_QueryStart | EggNOG_QueryEnd | EggNOG_SubjectStart | EggNOG_SubjectEnd | EggNOG_QueryCoverage | EggNOG_SubjectCoverage | OrthoDB_Support | OrthoDB_Bitscore | OrthoDB_SimilarityScore | OrthoDB_Evalue | OrthoDB_Length | OrthoDB_Mismatch | OrthoDB_QueryStart | OrthoDB_QueryEnd | OrthoDB_SubjectStart | OrthoDB_SubjectEnd | Expression | Molecular_Weight | CDS_Repeat_Content | CDS_Length | GC_Content | Free_Energy | GC_Ratio       | Kozak_Neg_3_Purine | Kozak_Pos_4 | ATG_Counts | 
|---------------------|----------------------|-------------------------|---------------------------|-------------------|---------------------|-------|-----------|---------------|---------------|----------------|-------------------------|---------------|-------------------|----------------|--------------------|------------------|---------------------|-----------------------|----------------|----------------|-------------------------|---------------|---------------|-----------------|-------------------|------------------|---------------------|------------------|------------|-----------------|-------------------|------------|------------|-------------|----------------|--------------------|------|--------|
| a_thaliana_gene-9190 | a_thaliana_mrna-9191 | 1 | 1 | 1 | 1 | 1 | 5 | 3  | 1 | 2885 | 100  | 0         | 1  | 1733     | 1 | 1733 | 99.9 | 100  | 1 | 2739 | 93.7 | 0         | 1736 | 107 | 1  | 1733 | 1 | 1736 | 8.48055   | 1721628.63 | 0 | 5202 | 41.16 | -125.8 | 1.65       | 1 | A | 36 |
| a_thaliana_gene-9200 | a_thaliana_mrna-9201 | 0 | 0 | 1 | 0 | 0 | 1 | 8  | 1 | 598  | 98.7 | 1.86E-209 | 38 | 3.43E+02 | 1 | 306  | 86.2 | 41.7 | 1 | 465  | 78.8 | 9.11E-164 | 302  | 55  | 51 | 343  | 9 | 310  | 0         | 631975.39  | 0 | 1065 | 40.66 | -92.6  | 1.40506329 | 0 | C | 12 | 
| a_thaliana_gene-9200 | a_thaliana_mrna-9220 | 0 | 0 | 1 | 1 | 0 | 2 | 17 | 1 | 1461 | 100  | 0         | 38 | 7.71E+02 | 1 | 734  | 95.1 | 100  | 1 | 1408 | 95.7 | 0         | 739  | 27  | 38 | 771  | 1 | 739  | 0         | 1348540.23 | 0 | 2316 | 40.1  | -92.6  | 1.40506329 | 0 | C | 22 | 
| a_thaliana_gene-9200 | a_thaliana_mrna-9257 | 0 | 0 | 0 | 1 | 0 | 1 | 17 | 1 | 1461 | 100  | 0         | 56 | 7.89E+02 | 1 | 734  | 92.9 | 100  | 1 | 1408 | 95.7 | 0         | 739  | 27  | 56 | 789  | 1 | 739  | 0.0550461 | 1348540.23 | 0 | 2370 | 40.1  | -92.6  | 1.40506329 | 0 | C | 22 |
| a_thaliana_gene-9200 | a_thaliana_mrna-9294 | 0 | 0 | 0 | 1 | 0 | 1 | 7  | 1 | 597  | 98.7 | 1.21E-209 | 1  | 3.06E+02 | 1 | 306  | 96.2 | 41.7 | 1 | 465  | 78.8 | 2.18E-164 | 302  | 55  | 14 | 306  | 9 | 310  | 0         | 552279.37  | 0 | 954  | 40.78 | -135.8 | 1.37623762 | 0 | T | 11 | 



**What do the features mean?**

| Feature                   | Description                                                                                                                                                                                       |
|---------------------------|-----------------------------------------------------------------|
| S1-AUG_OrthoDB_PsiCLASS   |  Indicates if the transcript is supported by AUGUSTUS prediction using PsiCLASS training set and OrthoDB protein hints (1=yes or 0=no).                                 |
| S2-AUG_OrthoDB_StringTie2 | Indicates if the transcript is supported by   AUGUSTUS prediction using StringTie2 training set and OrthoDB protein hints (1=yes or 0=no).                                  |
| S3-AUG_TD_PsiCLASS        | Indicates if the transcript is supported by   AUGUSTUS prediction using PsiCLASS training set and TransDecoder hints (1=yes or 0=no).                                                                              |
| S4-AUG_TD_StringTie2      |  Indicates if the transcript is supported by   AUGUSTUS prediction using StringTie2 training set and TransDecoder hints (1=yes or 0=no).                                                                               |
| S5-Transcriptomes         | Indicates if the transcript is supported by StringTie2 and PsiCLASS gene models (1=yes or 0=no).                                                                                              |
| Support                   | Sum of S1, S2, S3, S4 and S5  |
| Exons                     | The number of exons present in the transcript.                                                                                                                                                      |
| EggNOG_Support            | Indicates if the transcript has a hit from the EggNOG database (1=yes or 0=no) |
| EggNOG_Bitscore           | The bitscore from EggNOG homology analysis, reflecting the alignment   strength of the gene with homologous sequences in the EggNOG database.                                                       |
| EggNOG_SimilarityScore    | The similarity score (percentage) from EggNOG homology analysis,   representing the sequence similarity between the gene and homologous   sequences.                                                |
| EggNOG_Evalue             | The E-value from EggNOG homology analysis, measuring the statistical   significance of the sequence alignment (smaller values indicate more   significant alignments).                              |
| OrthoDB_Support            | Indicates if the transcript has a hit from the OrthoDB (`--order` or `--user_protein`) database (1=yes or 0=no) |
| OrthoDB_Bits              | The bitscore from OrthoDB homology analysis, reflecting the strength of   alignment with orthologous sequences in the OrthoDB database.                                                             |
| OrthoDB_SimilarityScore   | The similarity score (percentage) from OrthoDB homology analysis,   indicating sequence similarity with orthologous sequences.                                                                      |
| OrthoDB_Evalue            | The E-value from OrthoDB homology analysis, representing the statistical   significance of the alignment (smaller values indicate more significant   alignments).                                   |
| Expression                | TPM normalized RNA expression (log2).                                                                             |
| Molecular_Weight          | The calculated molecular weight of the transcript   (in Daltons).                                                                                                            |
| CDS_Repeat_Content        | The percentage of soft-masked elements (lower case) found within the   coding sequence (CDS).                                                                                                   |
| CDS_Length                | The length of the coding sequence (CDS).                                                                 |
| GC_Content                | The percentage of guanine (G) and cytosine (C) bases in the transcript,   reflecting the GC bias of the sequence.                                                                                   |
| Free_Energy               | The calculated free energy of the RNA structure, representing its   stability (lower values indicate more stable RNA structures).                                                                   |
| GC_Ratio                  | The ratio of GC content between different regions of the transcript   (e.g., exons vs. introns).                                                                                                    |
| Kozak_Neg_3_Purine        | A binary indicator (1 or 0) showing whether a purine base (A or G) is   present at the -3 position relative to the start codon (ATG) in the Kozak   sequence, important for translation initiation. |
| Kozak_Pos_4               | The nucleotide found at the +4 position relative to the start codon (ATG)   in the Kozak sequence, which can influence translation efficiency. G is highly conserved in eukaryotes.                                                      |
| ATG_Counts                | The number of occurrences of the start codon (ATG) in the transcript.                                                                             |

**Random Forest**

The random forest algorithm (classifier and regressor) is leveraged to predict true transcripts, and remove false positives: 

![rf](docs/random_forest.png)


#### Summary
You will also see an `easel_summary.txt` file with the following statistical information:

``` 
##### Genome #####
16.20 % of arabidopsis_thaliana_chr_folded.fasta is masked
WARNING: Less than 25% masked
Number of scaffolds: 5
Number of contigs: 97
Total length: 119146348
Percent gaps: 0.156%
Scaffold N50: 23 MB
Contigs N50: 11 MB
BUSCO (embryophyta_odb10): C:99.2%[S:98.2%,D:1.0%],F:0.3%,M:0.5%,n:1614,E:3.2%     
 
##### RNA Reads #####
FASTQ files were not provided.

##### Alignment Rates #####
Passed:
library rate
sr_ERR2757910 96.52
sr_ERR4619338 93.67
sr_ERR5343646 92.88
sr_SRR2037338 98.38
sr_SRR2037346 95.84
sr_SRR2037362 97.43
sr_SRR4095607 95.53
sr_SRR4095612 97.58

Failed:
None.
All libraries have a mapping rate greater than 85%

##### StringTie2 #####
Total number of genes: 18233
Total number of transcripts: 32022
Mono-exonic/multi-exonic rate: .12 
BUSCO (embryophyta_odb10): C:95.7%[S:73.7%,D:22.0%],F:0.9%,M:3.4%,n:1614           

##### PsiCLASS #####
Total number of genes: 19052
Total number of transcripts: 38314
Mono-exonic/multi-exonic rate: .17 
BUSCO (embryophyta_odb10): C:94.8%[S:66.2%,D:28.6%],F:1.7%,M:3.5%,n:1614           
 
##### Unfiltered #####
Total number of genes: 27907
Total number of transcripts: 60674
EnTAP alignment rate: .88
Mono-exonic/multi-exonic rate: .27 
BUSCO (embryophyta_odb10): C:99.7%[S:47.0%,D:52.7%],F:0.1%,M:0.2%,n:1614

##### Filtered #####
Total number of genes: 24998
Total number of transcripts: 44226
EnTAP alignment rate: .97
Mono-exonic/multi-exonic rate: .25 
BUSCO (embryophyta_odb10): C:99.5%[S:22.6%,D:76.9%],F:0.1%,M:0.4%,n:1614           
 
##### Filtered (Longest Isoform)  #####
Total number of genes: 24998
EnTAP alignment rate: .98
Mono-exonic/multi-exonic rate: .27 
BUSCO (embryophyta_odb10): C:99.2%[S:98.2%,D:1.0%],F:0.2%,M:0.6%,n:1614        
```
More detailed `BUSCO` and `AGAT` metrics can be found in the `metrics` directory, partitioned by unfiltered and filtered. 

You can expect the following directory tree upon a successful run:
[`EASEL Output`](docs/output.txt)


## Troubleshooting
**Failed Run?**
>- Nextflow will generate an output file where you can track down the error. To further inspect the problem locate the directory called `work` and the failed job's identifier.  
```
Command error:
ERROR: read1 output (--out1) and read1 output (--out2) should be different

Work dir:
/core/labs/Wegrzyn/easel/updates/easel/work/ae/7b95edd64d32b9dddeb0d152d0f3fc

Tip: you can replicate the issue by changing to the process work dir and entering the command `bash .command.run`
```
>- You can also see the progress of your job in `outdir/pipeline_info`. Several files exist that will identify complete and failed jobs as well as cpu and memory usage. 
>- Once the problem is resolved, Nextflow will allow you to pick up where you left off with [`-resume`](https://www.nextflow.io/docs/latest/getstarted.html)
>- If past AUGUSTUS, make sure to add `--resume_filtering true` as well.


## Literature
[`Welcome to the big leaves: best practices for improving genome annotation in non-model plant genomes`](https://www.biorxiv.org/content/10.1101/2022.10.03.510643v3)
  > Vidya Sudershan Vuruputoor, Daniel Monyak, Karl Christian Fetter, Cynthia Webster, Sumaira Zaman, Akriti   Bhattarai, Jeremy Bennett, Susan McEvoy, Bikash Shrestha, Madison Caballero, Jill Wegrzyn
    bioRxiv 2022.10.03.510643; doi: https://doi.org/10.1101/2022.10.03.510643

## Credits

Development of EASEL was funded by the U.S. National Science Foundation (NSF) Award [#1943371](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1943371&HistoricalAwards=false).

## Citations

EASEL is currently unpublished. For now, please use the GitLab URL when referencing.

An extensive list of references for the tools used by the pipeline can be found in the [`CITATIONS.md`](CITATIONS.md) file.

---------
Contact: cynthia.webster@uconn.edu

