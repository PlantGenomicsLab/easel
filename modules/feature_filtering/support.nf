process SUPPORT {
   label 'process_medium'
   container 'plantgenomics/easelv2.1:python'
     
    input:
    path(unfiltered_prediction)
    path(list)
     
    output: 
    path("support.tracking"), emit: support
    path("support_binary.tracking"), emit: binary
    
    script:
    """
    cat ${list} | while read gff; do
        if [[ "\$gff" == *"orthodb_psiclass"* ]]; then
            echo "Finding support for OrthoDB_PsiCLASS..." 
            python ${projectDir}/bin/support.py ${unfiltered_prediction} "\$gff" S1-AUG_OrthoDB_PsiCLASS.tracking

        elif [[ "\$gff" == *"orthodb_stringtie2"* ]]; then
            echo "Finding support for OrthoDB_StringTie2..."
            python ${projectDir}/bin/support.py ${unfiltered_prediction} "\$gff" S2-AUG_OrthoDB_StringTie2.tracking

        elif [[ "\$gff" == *"td_psiclass"* ]]; then
            echo "Finding support for TD_PsiCLASS..."
            python ${projectDir}/bin/support.py ${unfiltered_prediction} "\$gff" S3-AUG_TD_PsiCLASS.tracking

        elif [[ "\$gff" == *"td_stringtie2"* ]]; then
            echo "Finding support for TD_StringTie2..."
            python ${projectDir}/bin/support.py ${unfiltered_prediction} "\$gff" S4-AUG_TD_StringTie2.tracking
    
        elif [[ "\$gff" == *"rna_"* ]]; then
            echo "Finding support for transcriptomes..."
            python ${projectDir}/bin/support.py ${unfiltered_prediction} "\$gff" S5-Transcriptomes.tracking
        fi

        echo "Merging..."
        {
        echo -e "Gene\tTranscript\tS1-AUG_OrthoDB_PsiCLASS\tS2-AUG_OrthoDB_StringTie2\tS3-AUG_TD_PsiCLASS\tS4-AUG_TD_StringTie2\tS5-Transcriptomes"
        paste <(awk '{print \$1, \$2, \$3}' S1-AUG_OrthoDB_PsiCLASS.tracking) \
            <(awk '{print (\$3 == 0 ? 0 : 1)}' S2-AUG_OrthoDB_StringTie2.tracking) \
            <(awk '{print (\$3 == 0 ? 0 : 1)}' S3-AUG_TD_PsiCLASS.tracking) \
            <(awk '{print (\$3 == 0 ? 0 : 1)}' S4-AUG_TD_StringTie2.tracking) \
            <(awk '{print (\$3 == 0 ? 0 : 1)}' S5-Transcriptomes.tracking) | \
        awk '{print \$1, \$2, \$3, \$4, \$5, \$6, \$7}' OFS='\t'
        } > support.tracking
        echo "Done."
    done

    awk 'NR > 1 {sum=\$3+\$4+\$5+\$6+\$7; print \$1, \$2, sum}' OFS='\t' support.tracking | sed \$'1 i\\\nTranscript\tHit\tSupport' > support_binary.tracking

    """
}
