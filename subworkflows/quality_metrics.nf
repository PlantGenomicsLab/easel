include { MIKADO; BUSCO; AGAT; ENTAP; ENTAP_HGT; ADD_FUNCTION; LOG } from '../modules/metrics.nf'

workflow SUMMARY_STATS{
  
    take:
    unfiltered_gff
    filtered_gff
    longest_gff
    filtered_protein
    longest_protein
    unfiltered_protein
    odb
    reference
    reference_db
    entap_db
    eggnog_db
    eggnog_dir

    main:

    merge = Channel.empty()

    // Merge the final annotations
    merge
        .concat( filtered_gff, unfiltered_gff, longest_gff )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { annotation }

    // Merge the final protein files
    merge
            .concat( filtered_protein, unfiltered_protein, longest_protein )
            .flatten()
            .map { file -> tuple(file.baseName, file) }
        //  .view()
            .set { pep }

    if(params.reference != null){
	annotation
	.combine(reference)
	.set { annot_ref }
        MIKADO ( annot_ref, params.prefix )
    } 

    dmnd_ch = reference_db.collect()

    // horizontal gene transfer mode
    if(params.hgt_donor != null && params.hgt_recipient != null){
        hgt_donor = Channel.fromPath(params.hgt_donor, checkIfExists: true)
        hgt_recipient = Channel.fromPath(params.hgt_recipient, checkIfExists: true)

        ch_longest_prot = longest_protein.flatten().map { file -> tuple(file.baseName, file) }.view()

        merge
            .concat( filtered_protein, unfiltered_protein )
            .flatten()
            .map { file -> tuple(file.baseName, file) }
        //  .view()
            .set { isoform_pep }


        isoform_pep
            .combine( entap_db ).combine( eggnog_db).combine( eggnog_dir )
            .view()
            .set { entap_tuple }

        ENTAP_HGT                 ( ch_longest_prot, longest_gff, entap_db, eggnog_db, eggnog_dir, hgt_donor, hgt_recipient, dmnd_ch, params.taxon ?: '', params.entap_tcoverage, params.entap_qcoverage, params.contam ?: '')
        hgt_log = ENTAP_HGT.out.entap_log_hgt
        hgt_results = ENTAP_HGT.out.entap_results_hgt

        ENTAP                   ( entap_tuple, dmnd_ch, params.taxon ?: '', params.entap_tcoverage, params.entap_qcoverage, params.contam ?: '')
        isoform_log_f = ENTAP.out.entap_log_filtered
        isoform_results_f = ENTAP.out.entap_results_filtered
        isoform_log_uf = ENTAP.out.entap_log_unfiltered
        isoform_results_uf = ENTAP.out.entap_results_unfiltered

        merge
            .concat( isoform_results_f).concat(isoform_results_uf).concat(hgt_results )
            .flatten()
            .map { file -> tuple(file.parent, file) }
            .set { entap_results }

        merge
            .concat( isoform_log_f).concat(isoform_log_uf).concat(hgt_log )
            .flatten()
            .map { file -> tuple(file.parent, file) }
            .view()
            .set { entap_log }

        annotation
        .join( entap_results, by:0 )
        .view()
        .set { entap_annotation }
    }
    else if(params.hgt_donor == null && params.hgt_recipient == null ){
        
        pep
        .combine( entap_db ).combine( eggnog_db).combine( eggnog_dir )
        .view()
        .set { entap_tuple }

        ENTAP                   ( entap_tuple, dmnd_ch, params.taxon ?: '', params.entap_tcoverage, params.entap_qcoverage, params.contam ?: '')
        entap_log = ENTAP.out.entap_log
        entap_results = ENTAP.out.entap_results

        annotation
        .join(entap_results, by:0)
        .view()
        .set { entap_annotation }
    }

    BUSCO                   ( pep, odb )
    AGAT                    ( annotation, params.prefix )
    ADD_FUNCTION            ( entap_annotation )

    busco_txt = BUSCO.out.busco_txt
    agat_stats = AGAT.out.stats


    busco_txt
        .join(agat_stats, by:0).join(entap_log)
        .view()
        .set { metrics }

    LOG                     ( metrics )

    unfiltered = LOG.out.unfiltered
    
    if (params.build_training_set == false){
    filtered = LOG.out.filtered
        if (params.primary_isoform == true){
            longest = LOG.out.longest    
        }
        else if (params.primary_isoform == false){
            longest = Channel.empty()
        }
    }
    else if (params.build_training_set == true){
    filtered = Channel.empty()
    longest = Channel.empty()
    }

    emit:
    unfiltered
    filtered
    longest
    
}
