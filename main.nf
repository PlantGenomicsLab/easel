def helpMessage() {
	log.info"""
========================================================================================
             EASEL - Efficient, Accurate Scalable Eukaryotic modeLs
========================================================================================
 	
Usage:
nextflow run -hub gitlab PlantGenomicsLab/easel --genome /path/to/genome.fa --busco_lineage <busco_lineage> --order <orthodb_order> --sra /path/to/sralist.txt --training_set <plant/invertebrate> [...] 
or
nextflow run -hub gitlab PlantGenomicsLab/easel -profile <singularity/docker> -params-file params.yaml
	

Mandatory Parameters:
  --genome               Softmasked genome in FASTA format (zipped/unzipped)
                         Type: path
                         Example: --genome /home/genome/arabidopsis.fa

  --training_set         Clade specific training set for random forest feature filtering algorithm
                         Options: plant, invertebrate, vertebrate, user
                         Type: string
                         Example: --training_set plant

  --busco_lineage        Relevant BUSCO protein database
                         Type: string
                         Example: --busco_lineage embryophyta

  --user_reads           User paired-end RNA FASTQ files (zipped/unzipped)
                         Type: path
                         Example: --user_reads /home/reads/{1,2}.fastq.gz

  --sra                  SRA accession text file
                         Type: path
                         Example: --sra /home/sra/sra_list.txt

  --bam                  Short read user RNA alignments (BAM)
                         Type: path
                         Example: --bam /home/bam/*.bam

  --bam_long             Long read user RNA alignments (BAM)
                         Type: path
                         Example: --bam_long /home/bam/*.bam

  --order                Taxonomic rank such as order/clade to filter OrthoDB proteins
                         Type: string
                         Example: --order Brassicales

  --user_protein         User input proteins (default: null)
                         Type: path
                         Example: --user_protein /path/to/orthodb.faa

Recommended Parameters:
  --outdir                 Output directory name (default: easel)
                           Type: string
                           Example: --outdir easel
  
  --prefix                 AUGUSTUS training identifier and final predictions prefix (default: easel)
                           Type: string
                           Example: --prefix easel
  
  --taxon                  Any lineage such as species/kingdom/phylum used to filter ENTAP functional annotation (default: null)
                           Type: string
                           Example: --taxon arabidopsis

  --reference_db           DIAMOND database(s) for EnTAP functional annotation (default: complete RefSeq)
                           Type: path
                           Example: --reference_db /home/database/refseq.dmnd
  
  --entap_db               Path to EnTAP database (default: null)
                           Type: path
                           Example: --entap_db /home/database/entap/bin/entap_database.bin
  
  --eggnog_dir             Path to EggNOG database directory (default: null)
                           Type: path
                           Example: --eggnog_dir /home/database/entap/databases
  
  --eggnog_db              Path to EggNOG DIAMOND database (default: null)
                           Type: path
                           Example: --eggnog_db /home/database/entap/bin/eggnog_proteins.dmnd
  
  --contam                 Comma-separated list of contaminants to be filtered out by EnTAP (default: null)
                           Type: string
                           Example: --contam bacteria,fungi
  
  --pfam                   Drop transcripts from final annotation without PFAM domain (default: false)
                           Type: boolean
                           Example: --pfam false
  
  --pfam_db                Path to Pfam-A HMM database (default: null)
                           Type: path
                           Example: --pfam_db /home/database/Pfam-A.hmm
  
  --singularity_cache_dir   Output directory of singularity images
                           Type: path
                           Example: --singularity_cache_dir /home/singularity
  
  --primary_isoform        Output the longest isoform of filtered annotation (default: true)
                           Type: bool
                           Example: --primary_isoform true
  
  --max_memory             Maximum memory allocated (default: 500.GB)
                           Type: string
                           Example: --max_memory 500.GB
  
  --max_cpus               Maximum CPUs allocated (default: 64)
                           Type: integer
                           Example: --max_cpus 64
  
  --max_time               Maximum time allocated (default: 250.h)
                           Type: string
                           Example: --max_time 250.h

Optional Parameters:
  --resume_filtering       Resume EASEL from filtering stage (post-AUGUSTUS) (default: false)
                           Type: bool
                           Example: --resume_filtering false
  
  --rna_list               Path to StringTie2 and PsiCLASS GFF files used for resume_filtering (default: launchDir/outdir/06_predictions/rna.txt)
                           Type: path
                           Example: --rna_list /home/easel/06_predictions/rna.txt
  
  --augustus_list          Path to AUGUSTUS GFF files used for resume_filtering (default: launchDir/outdir/06_predictions/augustus.txt)
                           Type: path
                           Example: --augustus_list /home/easel/06_predictions/augustus.txt
  
  --bins                   Number of bins genome will be split into (default: 10)
                           Type: integer
                           Example: --bins 10
  
  --parts                  Number of bins true start sites will be split into (default: 5)
                           Type: integer
                           Example: --parts 5
  
  --percent_masked         Filtering percentage of repeats masked cut-off (default: 10)
                           Type: integer
                           Example: --percent_masked 10
  
  --rate                   Filtering mapping rate cut-off (default: 85)
                           Type: integer
                           Example: --rate 85
  
  --total_reads            Filtering read length cut-off (default: 15000000)
                           Type: integer
                           Example: --total_reads 15000000
  
  --mean_length            Filtering mean length of reads cut-off (default: 70)
                           Type: integer
                           Example: --mean_length 70
  
  --fastp_args             fastp: additional arguments (default: '')
                           Type: string
                           Example: --fastp_args --qualified_quality_phred 15
  
  --cluster_id             VSEARCH: identify threshold (default: 0.80)
                           Type: number
                           Example: --cluster_id 0.8
  
  --gap_tolerance          StringTie2: gap tolerance (default: 0)
                           Type: integer
                           Example: --gap_tolerance 0
  
  --hisat2_min_intronlen   HISAT2: min intron length (default: 20)
                           Type: integer
                           Example: --hisat2_min_intronlen 20
  
  --hisat2_max_intronlen   HISAT2: max intron length (default: 500000)
                           Type: integer
                           Example: --hisat2_max_intronlen 500000
  
  --hisat2_args            HISAT2: additional arguments (default: '')
                           Type: string
                           Example: --hisat2_args --maxins 500
  
  --miniprot_args          miniprot: additional arguments (default: '')
                           Type: string
                           Example: --miniprot_args -n 10
  
  --augustus               AUGUSTUS: path where repository is downloaded (default: ${projectDir}/bin)
                           Type: path
                           Example: --augustus /home/software
  
  --test_count             AUGUSTUS: number of genes to test (default: 250)
                           Type: integer
                           Example: --test_count 250
  
  --train_count            AUGUSTUS: number of genes to train (default: 1000)
                           Type: integer
                           Example: --train_count 1000
  
  --kfold                 AUGUSTUS: k-fold cross validation of optimization (default: 8)
                           Type: integer
                           Example: --kfold 8
  
  --rounds                AUGUSTUS: number of rounds for optimization (default: 5)
                           Type: integer
                           Example: --rounds 5
  
  --optimize              AUGUSTUS: optimize the meta parameters (default: true)
                           Type: bool
                           Example: --optimize true
  
  --orthodb_tcoverage      OrthoDB feature: t-coverage cutoff (default: 70)
                           Type: integer
                           Example: --orthodb_tcoverage 70
  
  --orthodb_qcoverage      OrthoDB feature: q-coverage cutoff (default: 70)
                           Type: integer
                           Example: --orthodb_qcoverage 70
  
  --entap_tcoverage        EnTAP: t-coverage cutoff (default: 70)
                           Type: integer
                           Example: --entap_tcoverage 70
  
  --entap_qcoverage        EnTAP: q-coverage cutoff (default: 70)
                           Type: integer
                           Example: --entap_qcoverage 70
  
  --window                 RNA folding window size, upstream_downstream (default: 400_200)
                           Type: string
                           Example: --window 400_200
  
  --regressor              Threshold for filtering random forest F1 prediction (default: 80)
                           Type: integer
                           Example: --regressor 80
  
  --taxid                  Taxonomic ID of species you want to remove from OrthoDB database (default: null)
                           Type: integer
                           Example: --taxid 3702
  
  --reference              Reference GTF to calculate F1 metric (output in `metrics/mikado`)
                           Type: path
                           Example: --reference /path/to/reference.gtf
  
  --utr                    Remove UTR features from final annotation (default: true)
                           Type: bool
                           Example: --utr true
  
  --long_intron            Increase maximum intron length across entire pipeline (default: false)
                           Type: boolean
                           Example: --long_intron false
  
  --max_intron             Max length of `--long_intron` (default: 3000000)
                           Type: integer
                           Example: --max_intron 3000000
  
  --repeat_masking         Basic RepeatMasker/RepeatModeler pipeline to generate softmasked genome for structural annotation (default: false)
                           Type: boolean
                           Example: --repeat_masking false
  
  --repeat_modeler_args    RepeatModeler: additional arguments (default: "")
                           Type: string
                           Example: --repeat_modeler_args "--engine ncbi"
  
  --repeat_masker_args      RepeatMasker: additional arguments (default: "")
                           Type: string
                           Example: --repeat_masker_args "--species cow"
  
  --build_training_set      Generate a training set for random forest filtering with model species (default: false)
                           Type: boolean
                           Example: --build_training_set false
  
  --user                   Path to generated training set (default: null)
                           Type: path
                           Example: --user /home/training_set/arabidopsis.tracking
  
  --psiclass_args          PsiCLASS: additional arguments (default: "")
                           Type: string
                           Example: --psiclass_args "--no-branch-cut"

  --stringtie2_args        StringTie2: additional arguments (default: "")
                           Type: string
                           Example: --stringtie2_args "-m 150"

  --stringtie2_merge_args  StringTie2: additional merge arguments (default: "")
                           Type: string
                           Example: --stringtie2_merge_args "-m 25"

  --remove_dups          Find and remove suspect isoforms with duplicate features from random forest
                         (default: true)
                         Type: boolean
                         Example: --remove_dups true

  --busco_genome         Run BUSCO on genome assembly
                         (default: true)
                         Type: boolean
                         Example: --busco_genome false

  --keep_taxa		 Keep certain taxa from the --training_set plant: a_thaliana, p_patens, p_trichocarpa; invertebrate: a_aegypti, d_melanogaster, c_elegans; vertebrate: d_rerio, m_musculus, x_laevis
                         (default: null)
                         Type: string
                         Example: --training_set plant --keep_taxa a_thaliana, p_patens

  --miniprothint         Select high quality miniprot alignments for hints evidence in AUGUSTUS
			 (default: false)
			 Type: boolean
			 Example: --miniprothint false

  --stringency           (0) All features are included in random forest filtering (default); (1) Drop the columns related to start/end positions and length/mismatch of hits in the EggNOG and OrthoDB outputs; (2) Drop columns related to bitscore, e-value, similarity, and coverage metrics from both EggNOG and OrthoDB; (3) A combination of stringency levels 1 and 2: drops both the position-based columns and the score/metric-based columns
			(default: 0)
			Type: integer
			Example: --stringency 3

Resuming nextflow:
  -resume					Resume script from last step 
}
	
	Documentation:
	https://gitlab.com/PlantGenomicsLab/easel

 	Contact:
	Cynthia Webster <cynthia.webster@uconn.edu>
	Jill Wegrzyn <jill.wegrzyn@uconn.edu>
	========================================================================================

    """.stripIndent()
}

params.help = false
if (params.help){
    helpMessage()
    exit 0
}

nextflow.enable.dsl=2

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE & PRINT PARAMETER SUMMARY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

WorkflowMain.initialise(workflow, params, log)


/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN ALL WORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { main_workflow as EASEL } from './workflows/main_workflow.nf'

workflow {

    EASEL()

    }

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
