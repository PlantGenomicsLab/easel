/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE INPUTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

def valid_params = [
    training       : ['plant', 'invertebrate', 'vertebrate', 'user', 'null'],
    orthodb        : ['Actinopterygii', 'Cyprinodontiformes', 'Cichliformes', 'Alveolata', 'Apicomplexa', 'Ciliophora', 'Coccidia', 'Aconoidasida', 'Cryptosporidium', 'Plasmodium', 'Sarcocystidae', 'Eimeria', 'Piroplasmida', 'Plasmodium', 'Babesia', 'Plasmodium', 'Theileriidae', 'Plasmodium', 'Amoebozoa', 'Eumycetozoa', 'Arthropoda', 'Crustacea', 'Hexapoda', 'Arachnida', 'Insecta', 'Acari', 'Araneae', 'Hemiptera', 'Holometabola', 'Lepidoptera', 'Hymenoptera', 'Diptera', 'Coleoptera', 'Brachycera', 'Polyphaga', 'Papilionoidea', 'Aculeata', 'Nematocera', 'Formicidae', 'Apoidea', 'Glossina', 'Culicidae', 'Drosophilidae', 'Drosophila', 'Anopheles', 'Cnidaria', 'Anthozoa', 'Euglenozoa', 'Trypanosoma', 'Leishmaniinae', 'Leishmania', 'Lophotrochozoa', 'Mollusca', 'Mammalia', 'Eutheria', 'Metatheria', 'Laurasiatheria', 'Euarchontoglires', 'Glires', 'Eulipotyphla', 'Artiodactyla', 'Carnivora', 'Primates', 'Rodentia', 'Cetacea', 'Cercopithecoidea', 'Hominoidea', 'Hominidae', 'Nematoda', 'Chromadorea', 'Rhodophyta', 'Sauropsida', 'Squamata', 'Aves', 'Gruiformes', 'Passeriformes', 'Falconiformes', 'Pelecaniformes', 'Galloanserae', 'Stramenopiles', 'Oomycota', 'Bacillariophyta', 'Saprolegniaceae', 'Phytophthora', 'Viridiplantae', 'Embryophyta', 'Chlorophyta', 'eudicotyledons', 'Liliopsida', 'Chlorophyceae', 'Trebouxiophyceae', 'Mamiellales', 'Malvales', 'Brassicales', 'Poales', 'Fabales', 'Lamiales', 'Malpighiales', 'Solanales', 'Rosales', 'Rosaceae', 'Nicotiana','Tetrapoda'],
    busco          : ['acidobacteria', 'aconoidasida', 'actinobacteria', 'actinobacteria', 'actinopterygii', 'agaricales', 'agaricomycetes', 'alphabaculovirus', 'alphaherpesvirinae', 'alphaproteobacteria', 'alteromonadales', 'alveolata', 'apicomplexa', 'aquificae', 'arachnida', 'archaea', 'arthropoda', 'ascomycota', 'aves', 'aviadenovirus', 'bacillales', 'bacilli', 'bacteria', 'bacteroidales', 'bacteroidetes-chlorobi', 'bacteroidetes', 'bacteroidia', 'baculoviridae', 'basidiomycota', 'bclasvirinae', 'betabaculovirus', 'betaherpesvirinae', 'betaproteobacteria', 'boletales', 'brassicales', 'burkholderiales', 'campylobacterales', 'capnodiales', 'carnivora', 'cellvibrionales', 'cetartiodactyla', 'chaetothyriales', 'cheoctovirus', 'chlamydiae', 'chlorobi', 'chloroflexi', 'chlorophyta', 'chordopoxvirinae', 'chromatiales', 'chroococcales', 'clostridia', 'clostridiales', 'coccidia', 'coriobacteriales', 'coriobacteriia', 'corynebacteriales', 'cyanobacteria', 'cyprinodontiformes', 'cytophagales', 'cytophagia', 'delta-epsilon-subdivisions', 'deltaproteobacteria', 'desulfobacterales', 'desulfovibrionales', 'desulfurococcales', 'desulfuromonadales', 'diptera', 'dothideomycetes', 'embryophyta', 'endopterygota', 'enquatrovirus', 'enterobacterales', 'entomoplasmatales', 'epsilonproteobacteria', 'euarchontoglires', 'eudicots', 'euglenozoa', 'eukaryota', 'eurotiales', 'eurotiomycetes', 'euryarchaeota', 'eutheria', 'fabales', 'firmicutes', 'flavobacteriales', 'flavobacteriia', 'fromanvirus', 'fungi', 'fusobacteria', 'fusobacteriales', 'gammaherpesvirinae', 'gammaproteobacteria', 'glires', 'glomerellales', 'guernseyvirinae', 'halobacteria', 'halobacteriales', 'haloferacales', 'helotiales', 'hemiptera', 'herpesviridae', 'hymenoptera', 'hypocreales', 'insecta', 'iridoviridae', 'lactobacillales', 'laurasiatheria', 'legionellales', 'leotiomycetes', 'lepidoptera', 'liliopsida', 'mammalia', 'metazoa', 'methanobacteria', 'methanococcales', 'methanomicrobia', 'methanomicrobiales', 'micrococcales', 'microsporidia', 'mollicutes', 'mollusca', 'mucorales', 'mucoromycota', 'mycoplasmatales', 'natrialbales', 'neisseriales', 'nematoda', 'nitrosomonadales', 'nostocales', 'oceanospirillales', 'onygenales', 'oscillatoriales', 'pahexavirus', 'passeriformes', 'pasteurellales', 'peduovirus', 'planctomycetes', 'plasmodium', 'pleosporales', 'poales', 'polyporales', 'poxviridae', 'primates', 'propionibacteriales', 'proteobacteria', 'pseudomonadales', 'rhizobiales', 'rhizobium-agrobacterium', 'rhodobacterales', 'rhodospirillales', 'rickettsiales', 'rudiviridae', 'saccharomycetes', 'sauropsida', 'selenomonadales', 'simplexvirus', 'skunavirus', 'solanales', 'sordariomycetes', 'sphingobacteriia', 'sphingomonadales', 'spirochaetales', 'spirochaetes', 'spirochaetia', 'spounavirinae', 'stramenopiles', 'streptomycetales', 'streptosporangiales', 'sulfolobales', 'synechococcales', 'synergistetes', 'tenericutes', 'tequatrovirus', 'teseptimavirus', 'tetrapoda', 'tevenvirinae', 'thaumarchaeota', 'thermoanaerobacterales', 'thermoplasmata', 'thermoproteales', 'thermoprotei', 'thermotogae', 'thiotrichales', 'tissierellales', 'tissierellia', 'tremellomycetes', 'tunavirinae', 'varicellovirus', 'verrucomicrobia', 'vertebrata', 'vibrionales', 'viridiplantae', 'xanthomonadales']
]

def summary_params = NfcoreSchema.paramsSummaryMap(workflow, params)

// Validate input parameters
WorkflowEasel.initialise(params, log, valid_params)

// Check input path parameters to see if they exist
checkPathParamList = [
    params.genome, params.busco_lineage, params.order, params.outdir, params.prefix, params.config_gene_model, params.config_protein, params.training_set, params.bins, params.total_reads, params.mean_length, params.rate, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.gap_tolerance, params.cluster_id, params.test_count, params.train_count, params.kfold, params.rounds, params.optimize, params.entap_tcoverage, params.entap_qcoverage, params.orthodb_tcoverage, params.orthodb_qcoverage, params.reference_db, params.window, params.parts, params.regressor, params.primary_isoform, params.user_protein, params.augustus ]

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { DATA_INPUT } from '../subworkflows/data_preparation.nf'
include { ANNOTATION } from '../subworkflows/gene_prediction.nf'
include { FILTERING } from '../subworkflows/filter.nf'
include { FINAL_PREDICTIONS } from '../subworkflows/format.nf'
include { PRIMARY_GENE } from '../subworkflows/isoform.nf'
include { SUMMARY_STATS } from '../subworkflows/quality_metrics.nf'
include { OUTPUT } from '../subworkflows/log_easel.nf'
include { BUILD_TRAINING } from '../subworkflows/training_set.nf'

workflow main_workflow {
	
	DATA_INPUT 				( params.max_intron, params.hisat2_max_intronlen ) 
    ANNOTATION              ( DATA_INPUT.out[1], DATA_INPUT.out[0], DATA_INPUT.out[10], DATA_INPUT.out[2], params.config_gene_model, params.config_protein, DATA_INPUT.out[6], DATA_INPUT.out[9] )
    FILTERING               ( DATA_INPUT.out[10], DATA_INPUT.out[6], DATA_INPUT.out[0], ANNOTATION.out[0], ANNOTATION.out[1], DATA_INPUT.out[13], DATA_INPUT.out[14])
    if (params.build_training_set == true){
    BUILD_TRAINING          ( DATA_INPUT.out[10], DATA_INPUT.out[11], ANNOTATION.out[0], FILTERING.out[3])
    }
    PRIMARY_GENE            ( FILTERING.out[0] )
    FINAL_PREDICTIONS       ( FILTERING.out[0], PRIMARY_GENE.out[0], DATA_INPUT.out[10] )
    SUMMARY_STATS           ( ANNOTATION.out[0], FINAL_PREDICTIONS.out[0], PRIMARY_GENE.out[0], FINAL_PREDICTIONS.out[1], FINAL_PREDICTIONS.out[2], FILTERING.out[2], params.busco_lineage, DATA_INPUT.out[11], DATA_INPUT.out[12], DATA_INPUT.out[7], DATA_INPUT.out[6], DATA_INPUT.out[8] )
    OUTPUT                  ( DATA_INPUT.out[3], DATA_INPUT.out[4], DATA_INPUT.out[5], ANNOTATION.out[2], ANNOTATION.out[3], SUMMARY_STATS.out[0], SUMMARY_STATS.out[1], SUMMARY_STATS.out[2] )
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPLETION EMAIL AND SUMMARY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

workflow.onComplete {
    if (params.email || params.email_on_fail) {
        NfcoreTemplate.email(workflow, params, summary_params, projectDir, log )
    }
    NfcoreTemplate.summary(workflow, params, log)
    if (params.hook_url) {
        NfcoreTemplate.adaptivecard(workflow, params, summary_params, projectDir, log)
    }
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
