process TRUE_START_SITE {
    label 'process_single'
    
    input:
    path(unfiltered_prediction)

    output:
    path("true_start_sites.txt"), emit: text

    """
    awk '\$3 == "start_codon" {
    match(\$9, /Parent=([^;]+)/, arr); OFS="\t";
    print \$1, \$4, \$7, arr[1]
    }' ${unfiltered_prediction} > true_start_sites.txt
    """
}
process PAD {     
    label 'process_medium'

    container 'plantgenomics/easelv2.1:python'
    
    input:
    val(windows)
    path(true_start_sites)
    path(genome)
     
    output: 
    path("*true_start_sites.fa"), emit: fasta
      
    """
var1=\$(echo ${windows} | cut -f1 -d"_")
var2=\$(echo ${windows} | cut -f2 -d"_" | cut -f1 -d"/")
python ${projectDir}/bin/pad.py --start_sites ${true_start_sites} --fasta ${genome} --out ${windows}_true_start_sites.fa --up \$var1 --down \$var2   

    """
}
process SPLIT_FASTA {
    label 'process_low'
    container 'plantgenomics/easel:perl'

    input:
    path(start_fasta)
    val(parts)
    
    output:
    path("*.fa"), emit: split

    """
    ${projectDir}/bin/fasta-splitter.pl --n-parts ${parts} ${start_fasta}

    """

}
process RNA_FOLD {
    label 'process_medium'
    tag {id}
 
    conda "bioconda::viennarna"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/viennarna:2.6.4--py310pl5321h6cc9453_1' :
        'quay.io/biocontainers/viennarna:2.6.4--py310pl5321h6cc9453_1' }"
    

    input:
    tuple val(id), path(start_fasta)
     
    output: 
    path("*rna.out"), emit: rna_fold
      
    """
awk '/^>/{if(NR==1){print}else{printf("\\n%s\\n",\$0)}next} {printf("%s",\$0)} END{printf("\\n")}' ${start_fasta} > unwrapped.fasta 
cat unwrapped.fasta | while read line; do                                                                                                                                                                 
if [[ "\$line" =~ ^\\>.* ]]
    then
    echo \$line >> ${id}.rna.out
    else
    echo -e \$line | RNAfold --verbose --noPS >> ${id}.rna.out
    fi
done      
    """
}
process MERGE {
    label 'process_single'

    input:
    path(rna)
    
    output: 
    path("merged_rna.out"), emit: merge_rna
      
    """
    cat ${rna} > merged_rna.out                                                                                         
    """
}
process MATRIX {
    label 'process_medium'
    container 'plantgenomics/easelv2.1:python'

    input:
    val(windows)
    path(matrix)
    path(folding)
     
    output: 
    path("free_energy.tracking"), emit: free_energy
    path("gc_ratio.tracking"), emit: gc_ratio
      
    """
    var1=\$(echo ${windows} | cut -f1 -d"_")
    var2=\$(echo ${windows} | cut -f2 -d"_" | cut -f1 -d"/")
    python ${projectDir}/bin/folding_features.py ${folding} \$var1 \$var2 folding_features.txt

    sed -i 's/"//g' folding_features.txt
    awk 'BEGIN { OFS="\t" } { print \$1, \$2 }' folding_features.txt > free_energy.txt
    sed -i \$'1 i\\\nTranscript\tHit\' free_energy.txt
    awk 'BEGIN { OFS="\t" } { print \$1, \$3 }' folding_features.txt > GC_ratio.txt
    sed -i \$'1 i\\\nTranscript\tHit\' GC_ratio.txt

    python ${projectDir}/bin/map_transcript.py ${matrix} free_energy.txt Free_Energy feature1.tracking
    awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
    if (\$8 == "") { 
        \$8 = "NaN" 
        } 
    } 1' feature1.tracking > free_energy.tracking
    python ${projectDir}/bin/map_transcript.py ${matrix} GC_ratio.txt GC_Ratio feature2.tracking
    awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
    if (\$8 == "") { 
        \$8 = "NaN" 
        } 
    }1' feature2.tracking > gc_ratio.tracking

    """
}
