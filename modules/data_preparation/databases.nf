process REFERENCE {
    label 'process_medium'
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"

    input:
    path(reference)
    val(prefix)
     
    output: 
    path("*.gtf"), emit: gtf
      
    """
    agat_convert_sp_gff2gtf.pl --gff ${reference} --gtf_version 1 -o ${prefix}_reference.gtf
    """   
}
process REFSEQ {
    label 'process_medium'
    publishDir "$params.outdir/databases",  mode: 'copy'

    container "plantgenomics/entap:1.4.0"

    output:
    path("refseq_complete.dmnd"), emit: entap_reference

    script:

    """
    wget -r -A '*.protein.faa.gz' ftp://ftp.ncbi.nlm.nih.gov/refseq/release/complete/
        gunzip ./ftp.ncbi.nlm.nih.gov/refseq/release/complete/*.gz
        cat ./ftp.ncbi.nlm.nih.gov/refseq/release/complete/*.faa > refseq_complete.protein.faa
        rm -rf ftp.ncbi.nlm.nih.gov
    diamond makedb --threads ${task.cpus} --in refseq_complete.protein.faa -d refseq_complete.dmnd
        rm refseq_complete.protein.faa

    """
}
process ENTAP {
    label 'process_medium'
    publishDir "$params.outdir/databases",  mode: 'copy'

    container "plantgenomics/entap:2.0.0"

    input:
    path(entap_db)
    path(eggnog_dir)
    path(eggnog_db)

    output:
    path('entap/bin/entap_database.bin'), emit: entap_db, optional: true
    path('entap/databases'), emit: eggnog_dir, optional: true
    path('entap/bin/eggnog_proteins.dmnd'), emit: eggnog_db, optional: true

    script:

    """
    cat <<EOF > entap_run.params
    fpkm=0.5
    align=
    single-end=false
    transdecoder-m=100
    transdecoder-no-refine-starts=false
    out-dir=entap
    overwrite=false
    resume=false
    input=
    database=
    no-trim=false
    threads=${task.cpus}
    output-format=1,3,4,7,
    hgt-donor=
    hgt-recipient=
    hgt-gff=
    ontology_source=0,
    eggnog-contaminant=true
    eggnog-dbmem=false
    eggnog-sensitivity=more-sensitive
    diamond-sensitivity=very-sensitive
    interproscan-db=
    taxon=
    qcoverage=50
    tcoverage=50
    contam=
    e-value=1e-05
    uninformative=conserved,predicted,unknown,unnamed,hypothetical,putative,unidentified,uncharacterized,uncultured,uninformative,
    EOF

    cat <<'EOF' > entap_config.ini
    data-generate=false
    data-type=0,
    entap-db-bin=${entap_db}
    entap-db-sql=
    entap-graph=${projectDir}/bin/entap_graphing.py
    rsem-calculate-expression=rsem-calculate-expression
    rsem-sam-validator=rsem-sam-validator
    rsem-prepare-reference=rsem-prepare-reference
    convert-sam-for-rsem=convert-sam-for-rsem
    transdecoder-long-exe=TransDecoder.LongOrfs
    transdecoder-predict-exe=TransDecoder.Predict
    eggnog-map-exe=emapper.py
    eggnog-map-data=${eggnog_dir}
    eggnog-map-dmnd=${eggnog_db}
    interproscan-exe=interproscan.sh
    diamond-exe=diamond
    EOF

    EnTAP --config --entap-ini entap_config.ini --run-ini entap_run.params 
    """
}
process ORTHODB {
    label 'process_low'
    publishDir "$params.outdir/databases/orthodb",  mode: 'copy', pattern: "*_orthodb.fasta"

    input:
    val(order)
    val(taxid)
    path(user_protein)

    output:
    path "*orthodb.fasta", emit: protein

    script:

    if (params.user_protein == null)
    """
    wget https://data.orthodb.org/v11/download/odb11v0_levels.tab.gz --no-check-certificate
    wget https://data.orthodb.org/v11/download/odb11v0_level2species.tab.gz --no-check-certificate
    wget https://data.orthodb.org/v11/download/odb11v0_species.tab.gz --no-check-certificate
    gzip -d odb11v0_species.tab.gz

    awk -F'\\t' '\$1 == "${order}"' ${projectDir}/bin/orthodb.txt 
    protein=\$(awk -F'\\t' '\$1 == "${order}" { print \$2 }' ${projectDir}/bin/orthodb.txt)
    wget https://treegenesdb.org/FTP/OrthoDB/v11/"\$protein".fasta.gz --no-check-certificate
    gzip -d "\$protein".fasta.gz

    GROUP="${order}"
    level=`zcat odb11v0_levels.tab.gz | awk -v grp=\$GROUP '\$2 == grp'`
    taxid=`echo \$level | cut -d ' ' -f 1`
    species=`zcat odb11v0_level2species.tab.gz \\
    | awk -v taxid=\$taxid '\$4 ~ "{"\$taxid"}" || 
                    \$4 ~ "{"taxid"," || 
                    \$4 ~ ","taxid"," || 
                    \$4 ~ ","taxid"}" {print \$2}'`
    echo \$species | wc -w
    echo \$level
    regex=`echo \$species | tr ' ' '|'`
    grep --no-group-separator -A 1 -P -w "\$regex" "\$protein".fasta > "\$GROUP"_orthodb.fasta

    if [ -z "${taxid}" ]; then
        echo "--taxid not provided, keeping species in orthodb database"
    elif awk -F'\\t' '\$1 == "${taxid}"' odb11v0_species.tab
    then
        id=\$(awk -F'\\t' '\$1 == "${taxid}" { print \$2 }' odb11v0_species.tab)
        tax_species=\$(awk -F'\\t' '\$1 == "${taxid}" { print \$3 }' odb11v0_species.tab)
        echo \$id
        echo \$tax_species
        grep \$id "\$GROUP"_orthodb.fasta > header.txt
        if [ ! -s "header.txt" ]; then 
            echo "taxid does not exist in database"
            exit 1
        else
            sed -i 's/^.//' header.txt
            awk 'FNR==NR { headers[\$0]; next } /^>/ { header=\$0; sub(/^>/, "", header); if (!(header in headers)) print; print_seq = !(header in headers); next } print_seq { print }' header.txt "\$GROUP"_orthodb.fasta > no_${taxid}_"\$GROUP"_orthodb.fasta
            rm "\$GROUP"_orthodb.fasta
        fi
    fi
    """
    else if (params.user_protein != null)
    """
    mv ${user_protein} orthodb.fasta
    """
}
process PFAM {
    label 'process_medium'
    publishDir "$params.outdir/databases/pfam",  mode: 'copy'

    output:
    path("Pfam-A.hmm"), emit: db

    script:
    """
    wget https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam37.0/Pfam-A.hmm.gz
    gunzip Pfam-A.hmm.gz
    """
}
