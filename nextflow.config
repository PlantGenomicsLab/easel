/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    EASEL Nextflow config file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Default config options for all compute environments
----------------------------------------------------------------------------------------
*/
params {
    // required input 
    genome                   = null
    sra                      = null 
    user_reads               = null
    busco_lineage            = null
    bam                      = null
    bam_long                 = null

    // recommended input
    outdir                   = "easel"
    order                    = null
    prefix                   = "easel"	
    primary_isoform          = true
    long_intron              = false
    max_intron               = 3000000
    taxon                    = null

    // protein filtering
    taxid                    = null
    
    // repeat masking
    repeat_masking           = false
    repeat_modeler_args      = ''
    repeat_masker_args       = ''

    // extrinsic evidence
    user_protein	         = null
    
    // config files
    config_gene_model        = "${projectDir}/bin/gene_model.cfg"
    config_protein           = "${projectDir}/bin/protein.cfg"
    
    // training set
    plant		             = "${projectDir}/bin/plant.tracking"
    invertebrate	         = "${projectDir}/bin/invertebrate.tracking"
    vertebrate               = "${projectDir}/bin/vertebrate.tracking"
    training_set_path        = null
    user                     = "${params.training_set_path}"
    training_set             = null
    build_training_set       = false
    keep_taxa                = null

    // genome  
    bins                     = 10
    reference                = null
    busco_genome	     = true
    
    // quality control
    total_reads              = 15000000
    mean_length              = 70
    rate                     = 85
    percent_masked	         = 10

    // fastp
    fastp_args               = ''

    //hisat2
    hisat2_min_intronlen     = 20
    hisat2_max_intronlen     = 500000
    hisat2_args              = ''

    //stringtie2
    gap_tolerance            = 0
    stringtie2_args          = ''
    stringtie2_merge_args    = ''
    
    //psiclass
    psiclass_args            = ''

    // clustering
    cluster_id               = 0.80

    // miniprot
    miniprot_args            = '' 
    miniprothint             = false
    
    //pfam
    pfam                     = false
    pfam_db                  = null 
    
    // augustus options
    test_count               = 250
    train_count              = 1000
    kfold                    = 0
    rounds		             = 5
    optimize                 = true
    augustus                 = "${projectDir}/bin"

    // orthodb
    orthodb_tcoverage		 = 70
    orthodb_qcoverage		 = 70

    // entap
    entap_tcoverage          = 70
    entap_qcoverage          = 70
    reference_db             = null
    entap_db                 = null
    eggnog_dir               = null
    eggnog_db                = null

    contam		             = null
    hgt_donor                = null
    hgt_recipient            = null
   
    // rna folding
    window                   = '400_200'
    parts                    = 8

    // filtering
    regressor                = 80
    resume_filtering         = false
    rna_list                 = null
    augustus_list            = null
    utr                      = false
    remove_dups              = true
    stringency               = 0
    
    // Max resource options
    // Defaults only, expecting to be overwritten
    max_memory                 = '1.TB'
    max_cpus                   = 64
    max_time                   = '14.d'

    // container
    singularity_cache_dir      = "${projectDir}/singularity"

    // Boilerplate options
    tracedir                   = "${params.outdir}/pipeline_info"
    email                      = null
    email_on_fail              = null
    plaintext_email            = false
    monochrome_logs            = false
    hook_url                   = null
    help                       = false
    version                    = false
    validate_params            = true
    show_hidden_params         = false
    schema_ignore_params       = null
    
    // Config options
    custom_config_version      = 'master'
    custom_config_base         = "https://raw.githubusercontent.com/nf-core/configs/${params.custom_config_version}"
    config_profile_description = null
    config_profile_contact     = null
    config_profile_url         = null
    config_profile_name        = null
}

// Load base.config by default for all pipelines
includeConfig 'conf/base.config'
// Load nf-core custom profiles from different Institutions
try {
    includeConfig "${params.custom_config_base}/nfcore_custom.config"
} catch (Exception e) {
    System.err.println("WARNING: Could not load nf-core/config profiles: ${params.custom_config_base}/nfcore_custom.config")
}


profiles {
    debug { process.beforeScript = 'echo $HOSTNAME' }
    conda {
        conda.enabled          = false
        docker.enabled         = false
        conda.cacheDir         = "${projectDir}/conda"
        singularity.enabled    = false
        podman.enabled         = false
        shifter.enabled        = false
        charliecloud.enabled   = false
    }
    mamba {
        conda.enabled          = false
        conda.useMamba         = true
        docker.enabled         = false
        singularity.enabled    = false
        podman.enabled         = false
        shifter.enabled        = false
        charliecloud.enabled   = false
    }
    docker {
        docker.enabled         = true
        docker.userEmulation   = true
        singularity.enabled    = false
        podman.enabled         = false
        shifter.enabled        = false
        charliecloud.enabled   = false
    }
    arm {
        docker.runOptions = '-u $(id -u):$(id -g) --platform=linux/amd64'
    }
    singularity {
        singularity.enabled    = true
        singularity.cacheDir   = "${params.singularity_cache_dir}"
        singularity.autoMounts = true
        docker.enabled         = false
        podman.enabled         = false
        shifter.enabled        = false
        charliecloud.enabled   = false
	singularity.pullTimeout = "1h"
    }
    podman {
        podman.enabled         = true
        docker.enabled         = false
        singularity.enabled    = false
        shifter.enabled        = false
        charliecloud.enabled   = false
    }
    shifter {
        shifter.enabled        = true
        docker.enabled         = false
        singularity.enabled    = false
        podman.enabled         = false
        charliecloud.enabled   = false
    }
    charliecloud {
        charliecloud.enabled   = true
        docker.enabled         = false
        singularity.enabled    = false
        podman.enabled         = false
        shifter.enabled        = false
    }
    gitpod {
        executor.name          = 'local'
        executor.cpus          = 16
        executor.memory        = 60.GB
    }
    test      { includeConfig 'conf/test.config'      }
}

// Export these variables to prevent local Python/R libraries from conflicting with those in the container
// The JULIA depot path has been adjusted to a fixed path `/usr/local/share/julia` that needs to be used for packages in the container.
// See https://apeltzer.github.io/post/03-julia-lang-nextflow/ for details on that. Once we have a common agreement on where to keep Julia packages, this is adjustable.

env {
    PYTHONNOUSERSITE = 1
    R_PROFILE_USER   = "/.Rprofile"
    R_ENVIRON_USER   = "/.Renviron"
    JULIA_DEPOT_PATH = "/usr/local/share/julia"
    AUGUSTUS_CONFIG_PATH = "${params.augustus}/.easel/augustus/config"
}

def trace_timestamp = new java.util.Date().format( 'yyyy-MM-dd_HH-mm-ss')
timeline {
    enabled = true
    file    = "${params.tracedir}/execution_timeline_${trace_timestamp}.html"
}
report {
    enabled = true
    file    = "${params.tracedir}/execution_report_${trace_timestamp}.html"
}
trace {
    enabled = true
    file    = "${params.tracedir}/execution_trace_${trace_timestamp}.txt"
}
dag {
    enabled = true
    file    = "${params.tracedir}/pipeline_dag_${trace_timestamp}.html"
}

manifest {
    name            = 'EASEL'
    author          = 'Cynthia Webster <cynthia.webster@uconn.edu>'
    homePage        = 'https://gitlab.com/PlantGenomicsLab/easel'
    description     = 'A Tool for Improvement of Eukaryotic Genome Annotation'
    mainScript      = 'main.nf'
    nextflowVersion = '!>=22.10.1'
    version         = '2.1.0-beta'
    defaultBranch   = 'main'
}

// Function to ensure that resource requirements don't go beyond
// a maximum limit
def check_max(obj, type) {
    if (type == 'memory') {
        try {
            if (obj.compareTo(params.max_memory as nextflow.util.MemoryUnit) == 1)
                return params.max_memory as nextflow.util.MemoryUnit
            else
                return obj
        } catch (all) {
            println "   ### ERROR ###   Max memory '${params.max_memory}' is not valid! Using default value: $obj"
            return obj
        }
    } else if (type == 'time') {
        try {
            if (obj.compareTo(params.max_time as nextflow.util.Duration) == 1)
                return params.max_time as nextflow.util.Duration
            else
                return obj
        } catch (all) {
            println "   ### ERROR ###   Max time '${params.max_time}' is not valid! Using default value: $obj"
            return obj
        }
    } else if (type == 'cpus') {
        try {
            return Math.min( obj, params.max_cpus as int )
        } catch (all) {
            println "   ### ERROR ###   Max cpus '${params.max_cpus}' is not valid! Using default value: $obj"
            return obj
        }
    }
}
