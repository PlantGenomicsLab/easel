process HMMSCAN {
    label 'process_medium'
    publishDir "$params.outdir/08_functional_annotation/pfam",  mode: 'copy'

    conda "bioconda::hmmer"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/hmmer:3.4--hdbdd923_1' :
        'quay.io/biocontainers/hmmer:3.4--hdbdd923_1' }"

    input:
    path(protein)
    path(pfam)
     
    output:
    path("pfam_transcripts.txt"), emit: transcripts
    path("pfam.domtblout"), emit: domtblout
    
    script:
    """
    hmmpress ${pfam}
    hmmscan --cpu ${task.cpus} --domtblout pfam.domtblout ${pfam} ${protein}
    awk '{print \$4}' pfam.domtblout | sort | uniq > pfam_transcripts.txt
    """
}
