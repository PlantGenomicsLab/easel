//
// This file holds several functions specific to the main.nf workflow in the PlantGenomicLab/easel pipeline
//
import nextflow.Nextflow

class WorkflowMain {

    //
    // Citation string for pipeline
    //
    public static String citation(workflow) {
        return "If you use ${workflow.manifest.name} for your analysis please cite:\n\n" +
            "* EASEL \n" +
            "  https://gitlab.com/PlantGenomicsLab/easel/\n\n" +
            "* Software dependencies\n" +
            "  https://gitlab.com/PlantGenomicsLab/easel/CITATIONS.md"
    }

    //
    // Print help to screen if required
    //
    public static String help(workflow, params) {
        def command = "nextflow run -hub gitlab PlantGenomicsLab/easel --genome /path/to/genome.fa --busco_lineage <busco_lineage> --order <orthodb_order> --sra /path/to/sralist.txt [...] "
        def help_string = ''
        help_string += NfcoreTemplate.logo(workflow, params.monochrome_logs)
        help_string += NfcoreSchema.paramsHelp(workflow, params, command)
        help_string += '\n' + citation(workflow) + '\n'
        help_string += NfcoreTemplate.dashedLine(params.monochrome_logs)
        return help_string
    }
    //
    // Generate parameter summary log string
    //
    public static String paramsSummaryLog(workflow, params) {
        def summary_log = ''
        summary_log += NfcoreTemplate.logo(workflow, params.monochrome_logs)
        summary_log += NfcoreSchema.paramsSummaryLog(workflow, params)
        summary_log += '\n' + citation(workflow) + '\n'
        summary_log += NfcoreTemplate.dashedLine(params.monochrome_logs)
        return summary_log
    }
    //
    // Validate parameters and print summary to screen
    //
    public static void initialise(workflow, params, log) {
        // Print help to screen if required
        if (params.help) {
            log.info help(workflow, params)
            System.exit(0)
        }

        // Print workflow version and exit on --version
        if (params.version) {
            String workflow_version = NfcoreTemplate.version(workflow)
            log.info "${workflow.manifest.name} ${workflow_version}"
            System.exit(0)
        }

        // Print parameter summary log to screen
        log.info paramsSummaryLog(workflow, params)

        // Warn about using custom configs to provide pipeline parameters
        NfcoreTemplate.warnParamsProvidedInConfig(workflow, log)

        // Validate workflow parameters via the JSON schema
        if (params.validate_params) {
            NfcoreSchema.validateParameters(workflow, params, log)
        }

        // Check that a -profile or Nextflow config has been provided to run the pipeline
        NfcoreTemplate.checkConfigProvided(workflow, log)
    }
}
