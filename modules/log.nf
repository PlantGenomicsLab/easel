process EASEL {
    label 'process_single'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(genome)
    path(rna_reads)
    path(alignments)
    path(psiclass)
    path(stringtie2)
    path(unfiltered)
    path(filtered)
    path(longest)

    output:
    path("easel_summary.txt"), emit: log
    
    script:
    """
    cat ${genome} <(echo "") ${rna_reads} <(echo "") ${alignments} <(echo "") ${psiclass} <(echo "") ${stringtie2} <(echo "") ${unfiltered} <(echo "") ${filtered} <(echo "") ${longest} > easel_summary.txt
    """
}
