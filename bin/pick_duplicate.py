#!/usr/bin/env python

########################################################
#                                                      #
# pick_duplicate.py - keep one isoform if identical    #
#                                                      #
# Developed by Cynthia Webster (2024)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

import sys
import random
from collections import defaultdict

def parse_transcripts_file(transcripts_file):
    """Parse the input file to get a list of transcript groups from the second column."""
    transcript_groups = []
    with open(transcripts_file, 'r') as file:
        for line in file:
            transcripts = line.strip().split("\t")[1]
            transcript_groups.append(transcripts.split(","))
    return transcript_groups

def parse_gtf_file(gtf_file):
    """Parse the GTF file once to get UTR lengths for each transcript."""
    utr_lengths = defaultdict(lambda: {'five_prime_UTR': 0, 'three_prime_UTR': 0})

    with open(gtf_file, 'r') as file:
        for line in file:
            if "five_prime_UTR" in line or "three_prime_UTR" in line:
                fields = line.strip().split("\t")
                feature = fields[2]
                start = int(fields[3])
                end = int(fields[4])
                length = end - start + 1

                # Extract transcript ID from the attributes (Parent field)
                attributes = fields[8]
                transcript_id = None
                for attr in attributes.split(";"):
                    if attr.strip().startswith("Parent"):
                        transcript_id = attr.split("=")[1].strip()
                        break

                # Add UTR length to the respective transcript
                if transcript_id:
                    utr_lengths[transcript_id][feature] += length
    return utr_lengths

def pick_best_transcript(utr_lengths, transcripts):
    """Pick the best transcript based on UTR lengths."""
    best_transcript = None
    best_utr_lengths = {'five_prime_UTR': 0, 'three_prime_UTR': 0}

    for transcript in transcripts:
        current_utr_lengths = utr_lengths[transcript]
        if (current_utr_lengths['five_prime_UTR'] > best_utr_lengths['five_prime_UTR'] or
            current_utr_lengths['three_prime_UTR'] > best_utr_lengths['three_prime_UTR']):
            best_transcript = transcript
            best_utr_lengths = current_utr_lengths

    # If no transcript is better, randomly pick one
    if best_transcript is None:
        best_transcript = random.choice(transcripts)

    return best_transcript

def process_file(transcripts_file, gtf_file, output_file):
    """Process the transcript selection for each group of transcripts."""
    transcript_groups = parse_transcripts_file(transcripts_file)
    utr_lengths = parse_gtf_file(gtf_file)

    with open(output_file, 'w') as out_file:
        for transcripts in transcript_groups:
            if len(transcripts) > 1:
                best_transcript = pick_best_transcript(utr_lengths, transcripts)
            else:
                best_transcript = transcripts[0]
            out_file.write(f"{best_transcript}\n")

transcripts_file = sys.argv[1]  # Your input file with gene and transcript pairs
gtf_file = sys.argv[2]  # Your GTF file
output_file = sys.argv[3]  # Output file with best transcripts

process_file(transcripts_file, gtf_file, output_file)

