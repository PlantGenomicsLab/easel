process PROTEIN {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.pep"
    label 'process_medium'
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"
    
    input:
    path(genome)
    path(unfiltered_prediction)
    val(prefix)
     
    output: 
    path("*unfiltered.pep"), emit: protein
      
    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o ${prefix}_unfiltered.pep
    """
}
process NUCLEOTIDE {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.cds"
    label 'process_medium'
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.4.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.4.0--pl5321hdfd78af_0' }"
    
    input:
    path(genome)
    path(unfiltered_prediction)
    val(prefix)
     
    output: 
    path("*unfiltered.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} -t cds -o ${prefix}_unfiltered.cds

    """
}
process EGGNOG {
    label 'process_medium'
    
    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.12--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.12--pyhdfd78af_0' }"
    
    input:
    path(protein)
    path(db)

    output: 
    path("eggnog.emapper.seed_orthologs"), emit: hits
      
    """
    emapper.py -i ${protein} -o eggnog --dmnd_db ${db} --override --no_annot -m diamond --cpu $task.cpus
    mkdir -p tmp 
    """
}
process EGGNOG_FEATURE {
    label 'process_low'
    container 'plantgenomics/easelv2.1:python'
    
    input:
    path(matrix)
    path(eggnog)

    output: 
    path("eggnog_bitscore.tracking"), emit: bitscore
    path("eggnog_similarity.tracking"), emit: similarity
    path("eggnog_evalue.tracking"), emit: evalue
    path("eggnog_qstart.tracking"), emit: qstart
    path("eggnog_qend.tracking"), emit: qend
    path("eggnog_sstart.tracking"), emit: sstart
    path("eggnog_send.tracking"), emit: send
    path("eggnog_qcoverage.tracking"), emit: qcoverage
    path("eggnog_scoverage.tracking"), emit: scoverage
    path("eggnog_support.tracking"), emit: support
      
    """
awk '!/^#/ {print \$1 "\t" \$4}' ${eggnog} > bitscore.txt
sed -i \$'1 i\\\nTranscript\tHit' bitscore.txt
python ${projectDir}/bin/map_transcript.py ${matrix} bitscore.txt EggNOG_Bitscore feature1.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN"
    } 
}   1' feature1.tracking > eggnog_bitscore.tracking

awk '!/^#/ {print \$1 "\t" \$9}' ${eggnog} > similarity.txt
sed -i \$'1 i\\\nTranscript\tHit' similarity.txt
python ${projectDir}/bin/map_transcript.py ${matrix} similarity.txt EggNOG_SimilarityScore feature2.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature2.tracking > eggnog_similarity.tracking

awk '!/^#/ {print \$1 "\t" \$3}' ${eggnog} > evalue.txt
sed -i \$'1 i\\\nTranscript\tHit' evalue.txt
python ${projectDir}/bin/map_transcript.py ${matrix} evalue.txt EggNOG_Evalue feature3.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature3.tracking > eggnog_evalue.tracking

awk '!/^#/ {print \$1 "\t" \$5}' ${eggnog} > qstart.txt
sed -i \$'1 i\\\nTranscript\tHit' qstart.txt
python ${projectDir}/bin/map_transcript.py ${matrix} qstart.txt EggNOG_QueryStart feature4.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature4.tracking > eggnog_qstart.tracking

awk '!/^#/ {print \$1 "\t" \$6}' ${eggnog} > qend.txt
sed -i \$'1 i\\\nTranscript\tHit' qend.txt
python ${projectDir}/bin/map_transcript.py ${matrix} qend.txt EggNOG_QueryEnd feature5.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature5.tracking > eggnog_qend.tracking

awk '!/^#/ {print \$1 "\t" \$7}' ${eggnog} > sstart.txt
sed -i \$'1 i\\\nTranscript\tHit' sstart.txt
python ${projectDir}/bin/map_transcript.py ${matrix} sstart.txt EggNOG_SubjectStart feature6.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature6.tracking > eggnog_sstart.tracking

awk '!/^#/ {print \$1 "\t" \$8}' ${eggnog} > send.txt
sed -i \$'1 i\\\nTranscript\tHit' send.txt
python ${projectDir}/bin/map_transcript.py ${matrix} send.txt EggNOG_SubjectEnd feature7.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature7.tracking > eggnog_send.tracking

awk '!/^#/ {print \$1 "\t" \$10}' ${eggnog} > qcov.txt
sed -i \$'1 i\\\nTranscript\tHit' qcov.txt
python ${projectDir}/bin/map_transcript.py ${matrix} qcov.txt EggNOG_QueryCoverage feature8.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature8.tracking > eggnog_qcoverage.tracking

awk '!/^#/ {print \$1 "\t" \$11}' ${eggnog} > scov.txt
sed -i \$'1 i\\\nTranscript\tHit' scov.txt
python ${projectDir}/bin/map_transcript.py ${matrix} scov.txt EggNOG_SubjectCoverage feature9.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = "NaN" 
    } 
}   1' feature9.tracking > eggnog_scoverage.tracking

awk '!/^#/ {print \$1}' ${eggnog} > ids.txt
awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' ids.txt > support.txt
sed -i \$'1 i\\\nTranscript\tHit' support.txt
python ${projectDir}/bin/map_transcript.py ${matrix} support.txt EggNOG_Support feature10.tracking
awk 'BEGIN { FS = OFS = "\t" } NR > 1 { 
if (\$8 == "") { 
    \$8 = 0 
    }
else if (\$8 == 1.0){
    \$8 = 1
    } 
}   1' feature10.tracking > eggnog_support.tracking
    """
}
